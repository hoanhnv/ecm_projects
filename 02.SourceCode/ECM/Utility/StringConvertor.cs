﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace VietLaw.Utils
{
    public static class StringConvertor
    {
        // Fields
        public static string format = "&#{0};";
        private const string HTML_PREFIX = "http://";

        private static char[] PHRASE_SEPARATORS = new char[]
                                                      {
                                                          '.', ',', ';', ':', '?', '!', '"', '\'', '\t', '\r', '\n', '|'
                                                          , '(', ')', '{', '}',
                                                          '-', '+'
                                                      };
        public const char SEPARATOR = '-';
        private const char SPACE = ' ';
        private const string SPACE_CHARS = " \t\r\n";
        private static char[] SPECIAL_CHARS = new char[] {'\x00ba'};

        private static char[][] VnVowelTbl = new char[][]
                                                 {
                                                     new char[]
                                                         {
                                                             'a', 'ă', '\x00e2', 'e', '\x00ea', 'i', 'o', '\x00f4', 'ơ',
                                                             'u', 'ư', 'y', '\x00e0', 'ằ', 'ầ', '\x00e8',
                                                             'ề', '\x00ec', '\x00f2', 'ồ', 'ờ', '\x00f9', 'ừ', 'ỳ',
                                                             '\x00e1', 'ắ', 'ấ', '\x00e9', 'ế', '\x00ed', '\x00f3', 'ố',
                                                             'ớ', '\x00fa', 'ứ', '\x00fd', 'ả', 'ẳ', 'ẩ', 'ẻ', 'ể', 'ỉ',
                                                             'ỏ', 'ổ', 'ở', 'ủ', 'ử', 'ỷ',
                                                             '\x00e3', 'ẵ', 'ẫ', 'ẽ', 'ễ', 'ĩ', '\x00f5', 'ỗ', 'ỡ', 'ũ',
                                                             'ữ', 'ỹ', 'ạ', 'ặ', 'ậ', 'ẹ',
                                                             'ệ', 'ị', 'ọ', 'ộ', 'ợ', 'ụ', 'ự', 'ỵ', 'đ', '\x00f0'
                                                         }, new char[]
                                                                {
                                                                    'a', 'a', 'a', 'e', 'e', 'i', 'o', 'o', 'o', 'u',
                                                                    'u', 'y', 'a', 'a', 'a', 'e',
                                                                    'e', 'i', 'o', 'o', 'o', 'u', 'u', 'y', 'a', 'a',
                                                                    'a', 'e', 'e', 'i', 'o', 'o',
                                                                    'o', 'u', 'u', 'y', 'a', 'a', 'a', 'e', 'e', 'i',
                                                                    'o', 'o', 'o', 'u', 'u', 'y',
                                                                    'a', 'a', 'a', 'e', 'e', 'i', 'o', 'o', 'o', 'u',
                                                                    'u', 'y', 'a', 'a', 'a', 'e',
                                                                    'e', 'i', 'o', 'o', 'o', 'u', 'u', 'y', 'd', 'd'
                                                                }
                                                 };

        private static char[] WORD_SEPARATORS = new char[]
                                                    {
                                                        ' ', '.', ',', ';', ':', '?', '!', '"', '\'', '\t', '\r', '\n',
                                                        '|', '(', ')', '{',
                                                        '}', '-', '+', '>', '<', '='
                                                    };

        // Methods
        public static string EmptyOrTrim(this object Val)
        {
            return (((Val == null) || string.IsNullOrEmpty(Val.ToString())) ? "" : Val.ToString().Trim());
        }

        public static string EmptyOrTrim(this string Val)
        {
            return (string.IsNullOrEmpty(Val) ? "" : Val.Trim());
        }


        public static string Floor(this string txt)
        {
            int num = txt.LastIndexOfAny(PHRASE_SEPARATORS);
            return ((num > 0) ? txt.Substring(0, num + 1) : txt);
        }

        public static bool IsLetterOrDigit(this char c)
        {
            return ((Enumerable.All<char>(Path.GetInvalidPathChars(), (Func<char, bool>) (i => (i != c))) &&
                     Enumerable.All<char>(SPECIAL_CHARS, (Func<char, bool>) (i => (i != c)))) && char.IsLetterOrDigit(c));
        }

        public static string NullOrTrim(this string Val)
        {
            return ((string.IsNullOrEmpty(Val) || (Val.Trim().Length == 0)) ? null : Val.Trim());
        }

        public static string RemoveSpaces(this string txt)
        {
            if (string.IsNullOrEmpty(txt))
            {
                return "";
            }
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < txt.Length; i++)
            {
                if (" \t\r\n".IndexOf(txt[i]) < 0)
                {
                    builder.Append(txt[i]);
                }
            }
            return builder.ToString();
        }

        public static string ToHTMLCodes(this string inputString)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(inputString);
            StringBuilder builder = new StringBuilder();
            foreach (byte num in bytes)
            {
                builder.Append(string.Format(format, num));
            }
            return builder.ToString();
        }

   
        public static string ToUrl(this string u)
        {
            u = u.RemoveSpaces();
            return (u = ((u.Length > 0) && !u.ToLower().StartsWith("http://")) ? ("http://" + u) : u);
        }

        public static string Transform(this string text, params KeyValuePair<string, string>[] patterns)
        {
            if ((string.IsNullOrEmpty(text) || (patterns == null)) || (patterns.Length == 0))
            {
                return text;
            }
            string str = text;
            foreach (KeyValuePair<string, string> pair in patterns)
            {
                str = str.Replace(pair.Key, pair.Value);
            }
            return str;
        }

        public static string UnMarkText(string txt)
        {
            if (string.IsNullOrEmpty(txt))
            {
                return "";
            }
            StringBuilder builder = new StringBuilder();
            foreach (char ch in txt)
            {
                try
                {
                    int index = 0;
                    while (index < VnVowelTbl[0].Length)
                    {
                        if (ch == VnVowelTbl[0][index])
                        {
                            break;
                        }
                        index++;
                    }
                    builder.Append((index == VnVowelTbl[0].Length) ? ch : VnVowelTbl[1][index]);
                }
                catch
                {
                    builder.Append(ch);
                }
            }
            return builder.ToString();
        }

        public static string Concat<T>(IEnumerable<T> l, string Separator)
        {
            if (l.Count() <= 0)
            {
                return null;
            }
            StringBuilder builder = new StringBuilder();
            Separator = Separator ?? "";
            foreach (T local in l)
            {
                builder.Append(local.ToString()).Append(Separator);
            }
            builder.Length -= Separator.Length;
            return builder.ToString();
        }

        public static uint ToUInt32(this object str, uint defaultvalue)
        {
            if (str == null || str == DBNull.Value)
                return defaultvalue;

            uint retval = defaultvalue;

            try
            {
                retval = Convert.ToUInt32(str);
            }
            catch (Exception)
            {
                //
                retval = defaultvalue;
            }

            return retval;
        }

        public static int ToInt32(this object str, int defaultvalue)
        {
            if (str == null || str == DBNull.Value)
                return defaultvalue;

            int retval = defaultvalue;

            try
            {
                retval = Convert.ToInt32(str);
            }
            catch (Exception)
            {
                //
                retval = defaultvalue;
            }

            return retval;
        }

        public static int? ToInt32(this object str, int? defaultvalue)
        {

            if (str == null || str == DBNull.Value)
                return defaultvalue;

            int? retval = defaultvalue;

            try
            {
                retval = Convert.ToInt32(str);
            }
            catch (Exception)
            {
                //
                retval = defaultvalue;
            }

            return retval;
        }

        public static short ToShort(this object str, short defaultvalue)
        {
            if (str == null || str == DBNull.Value)
                return defaultvalue;

            short retval = defaultvalue;

            try
            {
                if (short.TryParse(str.ToString(), out retval) == false)
                {
                    retval = defaultvalue;
                }
            }
            catch (Exception)
            {
                //
                retval = defaultvalue;
            }

            return retval;
        }

        public static byte ToByte(this object str, byte defaultvalue)
        {
            if (str == null || str == DBNull.Value)
                return defaultvalue;

            byte retval = defaultvalue;

            try
            {
                if (byte.TryParse(str.ToString(), out retval) == false)
                {
                    retval = defaultvalue;
                }
            }
            catch (Exception)
            {
                //
                retval = defaultvalue;
            }

            return retval;
        }

        public static byte? ToByte(this object str, byte? defaultvalue)
        {
            if (str == null || str == DBNull.Value)
                return defaultvalue;

            byte? retval = defaultvalue;

            try
            {
                byte _retval;
                if (byte.TryParse(str.ToString(), out _retval))
                {
                    retval = _retval;
                }
                else
                {
                    retval = defaultvalue;
                }
            }
            catch (Exception)
            {
                //
                retval = defaultvalue;
            }

            return retval;
        }

        public static double ToDouble(this object str, double defaultvalue)
        {
            if (str == null || str == DBNull.Value)
                return defaultvalue;

            double retval = defaultvalue;

            try
            {
                retval = Convert.ToDouble(str);
            }
            catch (Exception)
            {
                //
                retval = defaultvalue;
            }

            return retval;
        }

        public static long ToLong(this object str, long defaultvalue)
        {
            if (str == null || str == DBNull.Value)
                return defaultvalue;

            long retval = defaultvalue;

            try
            {
                retval = Convert.ToInt64(str);
            }
            catch (Exception)
            {
                //
                retval = defaultvalue;
            }

            return retval;
        }

        public static long? ToLong(this object str, long? defaultvalue)
        {
            if (str == null || str == DBNull.Value)
                return defaultvalue;

            long? retval = defaultvalue;

            try
            {
                retval = Convert.ToInt64(str);
            }
            catch (Exception)
            {
                //
                retval = defaultvalue;
            }

            return retval;
        }

        public static float ToFloat(this object str, float defaultvalue)
        {
            if (str == null || str == DBNull.Value)
                return defaultvalue;

            float retval = defaultvalue;

            try
            {
                retval = Convert.ToSingle(str);
            }
            catch (Exception)
            {
                //
                retval = defaultvalue;
            }

            return retval;
        }

        public static float? ToFloat(this object str, float? defaultvalue)
        {
            if (str == null || str == DBNull.Value)
                return defaultvalue;

            float? retval = defaultvalue;

            try
            {
                retval = Convert.ToSingle(str);
            }
            catch (Exception)
            {
                //
                retval = defaultvalue;
            }

            return retval;
        }

        public static bool ToBoolean(this object str, bool defaultvalue)
        {
            if (str == null || str == DBNull.Value)
                return defaultvalue;

            bool retval = defaultvalue;

            try
            {
                retval = Convert.ToBoolean(str);
            }
            catch (Exception)
            {
                //
                retval = defaultvalue;
            }

            return retval;
        }

        //public static DateTime ToDateTime(this object str, DateTime defaultvalue)
        //{
        //    DateTime retval = defaultvalue;
        //    if (str != null && str != DBNull.Value)
        //    {
        //        try
        //        {
        //            if (DateTime.TryParse(str.ToString(), out retval) == false)
        //                retval = defaultvalue;
        //        }
        //        catch (Exception)
        //        {
        //            retval = defaultvalue;
        //        }
        //    }
        //    return retval;
        //}

        public static string ToVNString(this DateTime dateTime)
        {
            return String.Format("{0:dd-MM-yyyy}", dateTime);
        }

        /// <summary>
        /// Convert string format (dd-mm-yyyy) to datetime type
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static DateTime ToVNDateTime(this string datetime)
        {
            var day = datetime.Substring(0, 2).ToInt32(0);
            var month = datetime.Substring(3, 2).ToInt32(0);
            var year = datetime.Substring(6, 4).ToInt32(0);
            return new DateTime(year, month, day);
        }

        public static string ToDate4Path(this DateTime datetime)
        {
            return datetime.ToString("yyMMdd");
        }

        public static string ToString(this List<string> lst, char separator)
        {
            string retval = string.Empty;
            if (lst != null && lst.Count > 0)
            {
                Array.ForEach(lst.ToArray(), obj => retval += obj + separator);
                retval = retval.Trim(separator);
            }
            return retval;
        }

        public static string EncodeToBase64String(this string input)
        {
            string retVal = string.Empty;
            if (!string.IsNullOrEmpty(input))
            {
                byte[] bytes = Encoding.Unicode.GetBytes(input);
                retVal = Convert.ToBase64String(bytes);
            }
            return retVal;
        }

        public static string DecodeBase64ToString(this string input)
        {
            string retVal = string.Empty;
            if (!string.IsNullOrEmpty(input))
            {
                byte[] bytes = Convert.FromBase64String(input);
                retVal = Encoding.Unicode.GetString(bytes);
            }
            return retVal;
        }

        /// <summary>
        /// Remove or normalize string
        /// </summary>
        /// <param name="input">the string need to normalize</param>
        /// <param name="separatedChar">the char need to remove </param>
        /// <returns></returns>
        public static string RemoveSeparatedChar(this string input, char separatedChar)
        {
            string retVal = string.Empty;
            if (!string.IsNullOrEmpty(input))
            {
                var strArr = input.Split(new[] {separatedChar}, StringSplitOptions.RemoveEmptyEntries);
                retVal = strArr.Aggregate(retVal, (current, s) => current + (s + separatedChar));
                retVal = retVal.TrimEnd(separatedChar);
            }
            return retVal;
        }
    }
}
