﻿using VietLaw.Utils;

namespace VietLaw.Services
{
    using Microsoft.VisualBasic;
    
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using Microsoft.VisualBasic.CompilerServices;

    public class Null
    {
        public static object GetNull(object objField, object objDBNull)
        {
            object objectValue = RuntimeHelpers.GetObjectValue(objField);
            if (objField == null)
            {
                return RuntimeHelpers.GetObjectValue(objDBNull);
            }
            if (objField is byte)
            {
                if (Convert.ToByte(RuntimeHelpers.GetObjectValue(objField)) == NullByte)
                {
                    objectValue = RuntimeHelpers.GetObjectValue(objDBNull);
                }
                return objectValue;
            }
            if (objField is short)
            {
                if (Convert.ToInt16(RuntimeHelpers.GetObjectValue(objField)) == NullShort)
                {
                    objectValue = RuntimeHelpers.GetObjectValue(objDBNull);
                }
                return objectValue;
            }
            if (objField is int)
            {
                if (RuntimeHelpers.GetObjectValue(objField).ToInt32(NullInteger) == NullInteger)
                {
                    objectValue = RuntimeHelpers.GetObjectValue(objDBNull);
                }
                return objectValue;
            }
            if (objField is float)
            {
                if (Convert.ToSingle(RuntimeHelpers.GetObjectValue(objField)) == NullSingle)
                {
                    objectValue = RuntimeHelpers.GetObjectValue(objDBNull);
                }
                return objectValue;
            }
            if (objField is double)
            {
                if (Convert.ToDouble(RuntimeHelpers.GetObjectValue(objField)) == NullDouble)
                {
                    objectValue = RuntimeHelpers.GetObjectValue(objDBNull);
                }
                return objectValue;
            }
            if (objField is decimal)
            {
                if (decimal.Compare(Convert.ToDecimal(RuntimeHelpers.GetObjectValue(objField)), NullDecimal) == 0)
                {
                    objectValue = RuntimeHelpers.GetObjectValue(objDBNull);
                }
                return objectValue;
            }
            if (objField is DateTime)
            {
                if (DateTime.Compare(Convert.ToDateTime(RuntimeHelpers.GetObjectValue(objField)).Date, NullDate.Date) == 0)
                {
                    objectValue = RuntimeHelpers.GetObjectValue(objDBNull);
                }
                return objectValue;
            }
            if (objField is string)
            {
                if (objField == null)
                {
                    return RuntimeHelpers.GetObjectValue(objDBNull);
                }
                if (objField.ToString() == NullString)
                {
                    objectValue = RuntimeHelpers.GetObjectValue(objDBNull);
                }
                return objectValue;
            }
            if (objField is bool)
            {
                if (Convert.ToBoolean(RuntimeHelpers.GetObjectValue(objField)) == NullBoolean)
                {
                    objectValue = RuntimeHelpers.GetObjectValue(objDBNull);
                }
                return objectValue;
            }
            if (objField is Guid)
            {
                Guid guid2 = (Guid)objField;
                if (guid2.Equals(NullGuid))
                {
                    objectValue = RuntimeHelpers.GetObjectValue(objDBNull);
                }
            }
            return objectValue;
        }

        public static bool IsNull(object objField)
        {
            if (objField != null)
            {
                if (objField is int)
                {
                    return objField.Equals(NullInteger);
                }
                if (objField is short)
                {
                    return objField.Equals(NullShort);
                }
                if (objField is byte)
                {
                    return objField.Equals(NullByte);
                }
                if (objField is float)
                {
                    return objField.Equals(NullSingle);
                }
                if (objField is double)
                {
                    return objField.Equals(NullDouble);
                }
                if (objField is decimal)
                {
                    return objField.Equals(NullDecimal);
                }
                if (objField is DateTime)
                {
                    return Conversions.ToDate(objField).Date.Equals(NullDate.Date);
                }
                if (objField is string)
                {
                    return objField.Equals(NullString);
                }
                if (objField is bool)
                {
                    return objField.Equals(NullBoolean);
                }
                return ((objField is Guid) && objField.Equals(NullGuid));
            }
            return true;
        }

        public static object SetNull(PropertyInfo objPropertyInfo)
        {
            string str = objPropertyInfo.PropertyType.ToString();
            if (str == "System.Int16")
            {
                return NullShort;
            }
            if ((((str == "System.Int32") || (str == "System.Int64")) ? 1 : 0) != 0)
            {
                return NullInteger;
            }
            if (str == "system.Byte")
            {
                return NullByte;
            }
            if (str == "System.Single")
            {
                return NullSingle;
            }
            if (str == "System.Double")
            {
                return NullDouble;
            }
            if (str == "System.Decimal")
            {
                return NullDecimal;
            }
            if (str == "System.DateTime")
            {
                return NullDate;
            }
            if ((((str == "System.String") || (str == "System.Char")) ? 1 : 0) != 0)
            {
                return NullString;
            }
            if (str == "System.Boolean")
            {
                return NullBoolean;
            }
            if (str == "System.Guid")
            {
                return NullGuid;
            }
            Type propertyType = objPropertyInfo.PropertyType;
            if (propertyType.BaseType.Equals(typeof(Enum)))
            {
                Array values = Enum.GetValues(propertyType);
                Array.Sort(values);
                return RuntimeHelpers.GetObjectValue(Enum.ToObject(propertyType, RuntimeHelpers.GetObjectValue(values.GetValue(0))));
            }
            return null;
        }

        public static object SetNull(object objValue, object objField)
        {
            if (Information.IsDBNull(RuntimeHelpers.GetObjectValue(objValue)))
            {
                if (objField is short)
                {
                    return NullShort;
                }
                if (objField is byte)
                {
                    return NullByte;
                }
                if (objField is int)
                {
                    return NullInteger;
                }
                if (objField is float)
                {
                    return NullSingle;
                }
                if (objField is double)
                {
                    return NullDouble;
                }
                if (objField is decimal)
                {
                    return NullDecimal;
                }
                if (objField is DateTime)
                {
                    return NullDate;
                }
                if (objField is string)
                {
                    return NullString;
                }
                if (objField is bool)
                {
                    return NullBoolean;
                }
                if (objField is Guid)
                {
                    return NullGuid;
                }
                return null;
            }
            return RuntimeHelpers.GetObjectValue(objValue);
        }

        public static bool NullBoolean
        {
            get
            {
                return false;
            }
        }

        public static byte NullByte
        {
            get
            {
                return 0xff;
            }
        }

        public static DateTime NullDate
        {
            get
            {
                return DateTime.MinValue;
            }
        }

        public static decimal NullDecimal
        {
            get
            {
                return -79228162514264337593543950335M;
            }
        }

        public static double NullDouble
        {
            get
            {
                return double.MinValue;
            }
        }

        public static Guid NullGuid
        {
            get
            {
                return Guid.Empty;
            }
        }

        public static int NullInteger
        {
            get
            {
                return -1;
            }
        }

        public static short NullShort
        {
            get
            {
                return -1;
            }
        }

        public static float NullSingle
        {
            get
            {
                return float.MinValue;
            }
        }

        public static string NullString
        {
            get
            {
                return "";
            }
        }
    }
}

