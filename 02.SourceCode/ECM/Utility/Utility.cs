﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Utility
{
    public class Utility
    {
        /// <summary>
        /// Convert date from dd/mm/yyyy to mm/dd/yyyy
        /// </summary>
        /// <param name="date"></param>
        /// <returns>DateTime</returns>
        public static string ConvertDateTime(string date)
        {
            string[] str = date.Split('/');

            return str[1] + "/" + str[0] + "/" + str[2];
        }

        /// <summary>
        /// Convert date from mm/dd/yyyy to yyyy/mm/dd
        /// </summary>
        /// <param name="date"></param>
        /// <returns>DateTime</returns>
        public static DateTime ConvertDateTimeyyyymmdd(string date)
        {
            string[] str = date.Split('/');
            var convertDate = new DateTime(Int32.Parse(str[2]), Int32.Parse(str[0]), Int32.Parse(str[1]));
            return convertDate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d1"></param>
        /// <param name="d2"></param>
        /// <returns></returns>
        public static int TotalDay(string d1, string d2)
        {
            DateTime date1 = ConvertDateTimeyyyymmdd(d1);
            DateTime date2 = DateTime.Parse(d2);
            TimeSpan timespan = date2.Subtract(date1);
            return timespan.Days;
        }

        public static string GetQueryStringFromUrl(string url, string queryStringName)
        {
            //http://stackoverflow.com/questions/35212714/url-rewrite-with-asp-net-query-string
            string value = "";
            try
            {
                if (url.Length > 0)
                {
                    string[] listString = url.Split('/');
                    if (listString.Length > 0)
                    {
                        for (int i = 0; i < listString.Length; i++)
                        {
                            if (queryStringName.Equals(listString[i])) // nếu querstring tồn tại trong url -> lấy giá trị ở mảng kế tiếp
                            {
                                value = listString[i + 1].Trim();
                            }
                        }
                    }
                }
            }
            catch (Exception )
            {

            }
            return value;
        }

        public static string CreateFrendlyText(string str)
        {
            string frendlyText = "";
            try
            {
                frendlyText = ConvertUnicodeToNoneUnicode(str).Trim().Replace(" ", "-").ToLower();
            }
            catch (Exception )
            {
            }

            return frendlyText;
        }

        public static string ConvertUnicodeToNoneUnicode(string text)
        {
            string result = "";
            try
            {
                for (int i = 33; i < 48; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                for (int i = 58; i < 65; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                for (int i = 91; i < 97; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                for (int i = 123; i < 127; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                //text = text.Replace(" ", "-"); //Comment l?i d? không dua kho?ng tr?ng thành kư t? -
                Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
                string strFormD = text.Normalize(System.Text.NormalizationForm.FormD);
                result = regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
            }
            catch (Exception)
            {

            }
            return result;
        }

        public static bool CheckContainsSpecialChars(string value)
        {
            var list = new[] { "~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "=", "-", "{", "}", " ", "?", ".", "_", "\"" };
            return list.Any(value.Contains);
        }
    }
}
