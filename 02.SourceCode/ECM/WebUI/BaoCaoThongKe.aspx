﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BaoCaoThongKe.aspx.cs" Inherits="WebUI.BaoCaoThongKe" %>

<%@ Register Src="~/UserControl/Chart/PieChart.ascx" TagPrefix="uc1" TagName="PieChart" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BreadCrumb" runat="server">
    Báo cáo thống kê
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:PieChart runat="server" ID="PieChart" />
</asp:Content>
