﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.Models;
using WebUI.Service;

namespace WebUI
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserName"] != null)
                {
                    lblUser.Text = Session["UserName"].ToString();
                    LoadListMenuByRole();
                }
                else
                {
                    Response.Redirect("Account/Login.aspx", false);
                }
            }
        }

        public void btnDangXuat_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("Account/Login.aspx", false);
        }

        public void LoadListMenuByRole()
        {
            try
            {
                int roleId = Session["RoleId"] != null ? int.Parse(Session["RoleId"].ToString()) : 0;
                var strMenu = RoleFunctionService.GetMenuByRole(roleId);
                ltrMenuAdmin.Text = strMenu;
            }
            catch (Exception)
            {
            }
        }
    }

}