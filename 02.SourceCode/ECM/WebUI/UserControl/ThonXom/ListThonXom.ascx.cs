﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.Service;
using WebUI.Models;
using Utility;

namespace WebUI.UserControl.ThonXom
{
    public partial class ListThonXom : System.Web.UI.UserControl
    {
        protected void ShowMessage(string Message, EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadListThonXom();
            }
        }
        private void LoadListThonXom(string tenThonXom = "")
        {
            try
            {
                long curentUserId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = long.Parse(Session["User_Id"].ToString());
                }

                if (tenThonXom.Length > 0) // serach
                {
                    var listThon = ThonService.GetListThonXomByName(tenThonXom);
                    if (listThon.Count > 0)
                    {
                        grvList.Visible = true;
                        grvList.DataSource = listThon;
                        grvList.DataBind();
                    }
                    else
                    {
                        grvList.Visible = false;
                        ShowMessage("Không có bản ghi nào", EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    var listThon = ThonService.GetListThonXom();
                    if (listThon.Count > 0)
                    {
                        grvList.Visible = true;
                        grvList.DataSource = listThon;
                        grvList.DataBind();
                    }
                    else
                    {
                        grvList.Visible = false;
                        ShowMessage("Không có bản ghi nào", EnumSite.MessageType.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load danh sách tỉnh: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadHuyen()
        {
            try
            {
                List<HUYEN> listHuyen = HuyenService.GetListHuyen();
                if (listHuyen.Count > 0)
                {
                    ddlHuyen.DataSource = listHuyen;
                    ddlHuyen.DataTextField = "TENHUYEN";
                    ddlHuyen.DataValueField = "HUYEN_ID";
                    ddlHuyen.DataBind();
                    ddlHuyen.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách huyện: " + ex.Message, Utility.EnumSite.MessageType.Error);
            }
        }
        private void LoadXaByHuyen(int huyenId)
        {
            try
            {
                List<XA> listHuyen = XaService.GetListXaByHuyen_ID(huyenId);
                if (listHuyen.Count > 0)
                {
                    ddlXa.DataSource = listHuyen;
                    ddlXa.DataTextField = "TENXA";
                    ddlXa.DataValueField = "XA_ID";
                    ddlXa.DataBind();
                    ddlXa.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách huyện: " + ex.Message, Utility.EnumSite.MessageType.Error);
            }
        }
        protected void ddlHuyen_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadXaByHuyen(int.Parse(ddlHuyen.SelectedValue));
            }
            catch (Exception)
            {
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim().Length == 0)
            {
                ShowMessage("Bạn chưa nhập tên xã cần tìm", EnumSite.MessageType.Success);
                return;
            }
            else
            {
                LoadListThonXom(txtSearch.Text.Trim());
            }
        }
        protected void grvList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string comanName = e.CommandName;
                var thonId = int.Parse(e.CommandArgument.ToString());
                switch (comanName)
                {
                    case "sua":
                        lblTitle.Text = "Cập nhật";
                        btnAdd.Text = "Cập nhật";
                        THON_XOM thon = ThonService.GetThonXomByID(thonId);
                        if (thon != null)
                        {
                            hdfId.Value = thon.THON_ID.ToString();
                            txtMaThon.Text = thon.MA_THON;
                            txtTenThon.Text = thon.TEN_THON;
                            LoadHuyen();
                            if (ddlHuyen.Items.Count > 0)
                            {
                                ddlHuyen.SelectedValue = thon.HUYEN_ID.ToString();
                            }

                            LoadXaByHuyen(int.Parse(ddlHuyen.SelectedValue));
                            if (ddlXa.Items.Count > 0)
                            {
                                ddlXa.SelectedValue = thon.XA_ID.ToString();
                            }

                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
                        }
                        break;
                    case "xoa":
                        try
                        {
                            var message = ThonService.DeleteThonXom(thonId);
                            if (message.Length == 0)
                            {
                                LoadListThonXom();
                                ShowMessage("Xóa xã thành công!", EnumSite.MessageType.Success);
                            }
                            else
                            {
                                ShowMessage("Lỗi xóa tỉnh:" + message, EnumSite.MessageType.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi xóa:" + ex.Message, EnumSite.MessageType.Error);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi xảy ra:" + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void grvList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grvList.PageIndex = e.NewPageIndex;
                grvList.DataBind();
                LoadListThonXom();
            }
            catch (Exception)
            {
            }
        }
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            lblTitle.Text = "Thêm mới";
            btnAdd.Text = "Thêm mới";
            hdfId.Value = "";
            txtMaThon.Text = "";
            txtTenThon.Text = "";
            LoadHuyen();
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                var mesage = "";
                if (btnAdd.Text == "Thêm mới")
                {
                    THON_XOM thon = new THON_XOM();
                    thon.MA_THON = txtMaThon.Text.Trim();
                    thon.TEN_THON = txtTenThon.Text.Trim();
                    thon.HUYEN_ID = int.Parse(ddlHuyen.SelectedValue);
                    thon.XA_ID = int.Parse(ddlXa.SelectedValue.ToString());
                    mesage = ThonService.InsertThonXom(thon);
                    if (mesage.Length > 0)
                    {
                        ShowMessage("Lỗi thêm mới: " + mesage, EnumSite.MessageType.Error);
                        return;
                    }
                    else
                    {
                        ShowMessage("Them mới thành công", EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    var thonId = int.Parse(hdfId.Value);
                    var maThon = txtMaThon.Text.Trim();
                    var tenThon = txtTenThon.Text.Trim();
                    var tinhId = int.Parse(ddlHuyen.SelectedValue);
                    var xaId = int.Parse(ddlXa.SelectedValue);

                    mesage = ThonService.UpdatetThonXom(thonId, maThon, tenThon, tinhId, xaId);
                    if (mesage.Length > 0)
                    {
                        ShowMessage("Lỗi cập nhật: " + mesage, EnumSite.MessageType.Error);
                        return;
                    }
                    else
                    {
                        ShowMessage("Cập nhật thành công ", EnumSite.MessageType.Success);
                    }
                }

                LoadListThonXom();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi thao tác: " + ex.Message, EnumSite.MessageType.Success);
            }
        }
    }
}