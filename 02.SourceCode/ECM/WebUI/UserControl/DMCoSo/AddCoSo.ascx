﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddCoSo.ascx.cs" Inherits="WebUI.UserControl.DMCoSo.AddCoSo" %>

<div class="page-header">
    <h3>
        <asp:Label Text="" ID="lblTitle" runat="server" /></h3>
</div>
<br />
<div id="alert_container" class="messagealert">
</div>
<div class="col-xs-12">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label" for="inputEmail3">
                Mã cơ sở:</label>
            <div class="col-sm-9">
                <asp:HiddenField runat="server" ID="hdfID" />
                <asp:TextBox ID="txtMaCoSo" ClientIDMode="Static" runat="server" class="validate[required] form-control"
                    placeholder="nhập mã cơ sở" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="inputPassword3">
                Tên cơ sở:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtTenCoSo" ClientIDMode="Static" runat="server" class="validate[required] form-control"
                    placeholder="nhập tên cơ sở" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Tên tiếng anh:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtTenTiengAnh" ClientIDMode="Static" runat="server" class="form-control" placeholder="nhập tên tiếng anh" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Tên viết tắt:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtTenVietTat" ClientIDMode="Static" runat="server" class="form-control" placeholder="nhập tên viết tắt" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Địa chỉ:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtDiaChi" ClientIDMode="Static" TextMode="MultiLine" Height="50px" runat="server" class="form-control" placeholder="nhập địa chỉ" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Điện thoại:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtDienThoai" ClientIDMode="Static" runat="server" class="form-control" placeholder="nhập số điện thoại" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Fax:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtFax" runat="server" ClientIDMode="Static" class="form-control" placeholder="nhập số fax" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Email:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtEmail" runat="server" ClientIDMode="Static" class="form-control" placeholder="nhập địa chỉ email" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Website:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtWebsite" runat="server" ClientIDMode="Static" class="form-control" placeholder="nhập website" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Số quyết định thành lập:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtSoQuyetDinhThanhLap" ClientIDMode="Static" runat="server" class="form-control" placeholder="nhập số quyết định thành lập" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Số giấy ĐKKD:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtSoGiayDangKyKinhDoanh" ClientIDMode="Static" runat="server" class="form-control" placeholder="nhập số giấy ĐKKD" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Ngày cấp ĐKKD:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtNgayCapDKKD" ClientIDMode="Static" runat="server" class="datepicker form-control" placeholder="nhập ngày sinh" Width="150px" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Ngày cấp QD thành lập:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtNgayQDThanhLap" ClientIDMode="Static" runat="server" class="datepicker form-control" placeholder="nhập quyết định thành lập" Width="150px" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Loại giấy tờ:</label>
            <div class="col-sm-9">
                <%--<asp:TextBox ID="txtLoaiGiayTo" runat="server" ClientIDMode="Static" class="form-control" placeholder="nhập loại giấy tờ" />--%>
                <asp:DropDownList runat="server" ID="ddlLoaiGiayTo">
                    <asp:ListItem Text="Bản cứng" Value="0" />
                    <asp:ListItem Text="Bản mềm" Value="1" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Đơn vị cấp:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtDonViCap" runat="server" ClientIDMode="Static" class="form-control" placeholder="nhập đơn vị cấp" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Nội dung cấp:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtNoiDungCap" runat="server" ClientIDMode="Static" class="form-control" placeholder="nhập nội dung cấp" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Đại diện pháp nhân:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtDaiDienPhapNhan" ClientIDMode="Static" runat="server" class="form-control" placeholder="nhập đại diện pháp nhân" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Các lĩnh vực hoạt động chính:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtLinhVucHoatDongChinh" ClientIDMode="Static" runat="server" class="form-control" placeholder="nhập các lĩnh vực hoạt động chính" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Các thông tin khác:</label>
            <div class="col-sm-9">
                <asp:TextBox ID="txtCacThongTinKhac" ClientIDMode="Static" runat="server" class="form-control" placeholder="nhập các thông tin khác" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="inputPassword3">
                Tỉnh:</label>
            <div class="col-sm-9">
                <asp:DropDownList ID="ddlTinh" ClientIDMode="Static" runat="server" class="form-control" Width="280px"></asp:DropDownList>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="inputPassword3">
                Huyện:</label>
            <div class="col-sm-9">
                <asp:DropDownList ID="ddlHuyen" ClientIDMode="Static" AutoPostBack="true" runat="server" class="form-control" Width="280px" OnSelectedIndexChanged="ddlHuyen_SelectedIndexChanged1">
                </asp:DropDownList>

            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="inputPassword3">
                Xã:</label>
            <div class="col-sm-9">
                <asp:UpdatePanel runat="server" ID="updatepanel1">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddXa" ClientIDMode="Static" AutoPostBack="true" runat="server" class="form-control" Width="280px" OnSelectedIndexChanged="ddXa_SelectedIndexChanged">
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlHuyen" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="inputPassword3">
                Thôn:</label>
            <div class="col-sm-9">
                <asp:UpdatePanel runat="server" ID="updatepanel2">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlThon" runat="server" ClientIDMode="Static" class="form-control" Width="280px">
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddXa" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Kích hoạt:</label>
            <div class="col-sm-9">
                <asp:CheckBox Text="&nbsp; kích hoạt" ID="cbKichHoat" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">
                Phê duyệt:</label>
            <div class="col-sm-9">
                <asp:CheckBox Text="&nbsp; Phê duyệt" ID="cbPheDuyet" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <center><asp:Label Text="" ID="lblMessage" ClientIDMode="Static" ForeColor="Red" runat="server" /></center>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-9">
                <asp:Button ID="btnAdd" ClientIDMode="Static" runat="server" class="btn btn-primary" OnClick="btnAdd_Click"
                    Text="Thêm mới" />
                &nbsp; <a href="javascript:window.history.back();" class="btn btn-primary">Trở lại</a>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#btnAdd').click(function () {
            debugger
            $('#lblMessage').text('');
            if (!validateEmail($('#txtEmail').val())) {
                $('#lblMessage').text('Email sai định dạng');
                return false;
            }
            if (!validatephonenumber($('#txtDienThoai').val())) {
                $('#lblMessage').text('Số điện thoại sai định dạng');
                return false;
            }

            //if ($('#txtTenCoSo').val() == '') {
            //    $('#lblMessage').text('Tên cơ sở không được để trống');
            //    return false;
            //}
            //if ($('#txtDiaChi').val() == '') {
            //    $('#lblMessage').text('Địa chỉ cơ sở không được để trống');
            //    return false;
            //}
            //if ($('#txtDienThoai').val() == '') {
            //    $('#lblMessage').text('Điện thoại không được để trống');
            //    return false;
            //}
            //if ($('#txtSoQuyetDinhThanhLap').val() == '') {
            //    $('#lblMessage').text('Số quyết định thành lập cơ sở không được để trống');
            //    return false;
            //}
            //if ($('#txtSoGiayDangKyKinhDoanh').val() == '') {
            //    $('#lblMessage').text('Số giấy đăng ký kinh doanh không được để trống');
            //    return false;
            //}
            //if ($('#txtNgayCapDKKD').val() == '') {
            //    $('#lblMessage').text('Ngày cấp giấy phép đăng ký kinh doanh không được để trống');
            //    return false;
            //}
            //if ($('#txtNgayQDThanhLap').val() == '') {
            //    $('#lblMessage').text('Ngày cấp quyết định thành lập cơ sở không được để trống');
            //    return false;
            //}
            //if ($('#txtDaiDienPhapNhan').val() == '') {
            //    $('#lblMessage').text('Đại diện pháp nhân không được để trống');
            //    return false;
            //}
        });
    });
</script>
