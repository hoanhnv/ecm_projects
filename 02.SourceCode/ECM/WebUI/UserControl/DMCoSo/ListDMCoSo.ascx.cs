﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.Service;
using Utility;
using WebUI.Models;

namespace WebUI.UserControl.DMCoSo
{
    public partial class ListDMCoSo : System.Web.UI.UserControl
    {
        ECM_CSDLEntities context = new ECM_CSDLEntities();
        protected void ShowMessage(string Message, EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadListCoSo();
            }
        }
        private void LoadListCoSo(string tenCoso = "")
        {
            try
            {
                long curentUserId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = long.Parse(Session["User_Id"].ToString());
                }
                List<COSODAOTAO> listCoSo;
                if (tenCoso.Length > 0)
                {
                    listCoSo = context.COSODAOTAO.Where(o => o.TENCOSO.Contains(tenCoso)).ToList();
                    if (listCoSo != null)
                    {
                        grvCoSo.Visible = true;
                        grvCoSo.DataSource = listCoSo;
                        grvCoSo.DataBind();
                    }
                    else
                    {
                        grvCoSo.Visible = false;
                        ShowMessage("Không có cơ sở nào", EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    listCoSo = context.COSODAOTAO.Where(o => o.TENCOSO.Contains(tenCoso)).ToList();
                    if (listCoSo != null)
                    {
                        grvCoSo.Visible = true;
                        grvCoSo.DataSource = listCoSo;
                        grvCoSo.DataBind();
                    }
                    else
                    {
                        grvCoSo.Visible = false;
                        ShowMessage("Không có cơ sở nào", EnumSite.MessageType.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load danh cơ sở: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim().Length == 0)
            {
                ShowMessage("Bạn chưa nhập tài khoản cần tìm", EnumSite.MessageType.Success);
                return;
            }
            else
            {
                LoadListCoSo(txtSearch.Text.Trim());
            }
        }
        protected void grvCoSo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string comanName = e.CommandName;
                var id = long.Parse(e.CommandArgument.ToString());
                switch (comanName)
                {
                    case "sua":
                        Response.Redirect("ThemMoiCoSo.aspx?Id=" + id, false);
                        break;
                    case "xoa":
                        try
                        {
                            var message = DM_CoSoService.DeleteCoSoDaoTao(id);
                            if (message.Length > 0)
                            {
                                LoadListCoSo();
                                ShowMessage("Xóa cơ sở thành công!", EnumSite.MessageType.Success);
                            }
                            else
                            {
                                ShowMessage("Lỗi xóa cơ sở:" + message, EnumSite.MessageType.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi xóa:" + ex.Message, EnumSite.MessageType.Error);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi xảy ra:" + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void grvCoSo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grvCoSo.PageIndex = e.NewPageIndex;
                grvCoSo.DataBind();
                LoadListCoSo();
            }
            catch (Exception)
            {
            }
        }
    }
}