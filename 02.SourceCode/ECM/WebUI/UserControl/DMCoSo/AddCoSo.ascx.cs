﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.Service;
using Utility;
using WebUI.Models;
using System.Globalization;

namespace WebUI.UserControl.DMCoSo
{
    public partial class AddCoSo : System.Web.UI.UserControl
    {
        ECM_CSDLEntities context = new ECM_CSDLEntities();
        int curentUserId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                long coSoId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = int.Parse(Session["User_Id"].ToString());
                }

                if (Request.QueryString["Id"] != null)  // eidt
                {
                    try
                    {
                        coSoId = long.Parse(Request.QueryString["Id"]);
                        btnAdd.Text = "Cập nhật";
                        hdfID.Value = coSoId.ToString();
                        lblTitle.Text = "Cập nhật thông tin cơ sở";

                        LoadThongTinCoSoInfo(coSoId);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi load thông tin cập nhật: " + ex.Message, EnumSite.MessageType.Error);
                    }
                }
                else // insert
                {
                    btnAdd.Text = "Thêm mới";
                    lblTitle.Text = "Thêm mới cơ sở";
                }
                LoadListTinh();
                LoadListHuyen();
            }
        }
        public void LoadThongTinCoSoInfo(long coSoId)
        {
            try
            {
                COSODAOTAO coSo = DM_CoSoService.GetCoSoDaoTaoByID(coSoId);
                if (coSo != null)
                {
                    txtMaCoSo.Text = coSo.MACOSO;
                    txtTenCoSo.Text = coSo.TENCOSO;
                  
                    txtDienThoai.Text = coSo.DIENTHOAI;
                    txtFax.Text = coSo.FAX;
                    txtEmail.Text = coSo.EMAIL;
                    txtWebsite.Text = coSo.WEBSITE;
                                 
                    if (coSo.IS_APPROVE == true)
                    {
                        cbPheDuyet.Checked = true;
                    }
                    else
                    {
                        cbPheDuyet.Checked = false;
                    }

                    if (coSo.IS_ACTIVE == true)
                    {
                        cbKichHoat.Checked = true;
                    }
                    else
                    {
                        cbKichHoat.Checked = false;
                    }

                    // load list huyện
                    LoadListHuyen();
                    if (ddlHuyen.Items.Count > 0)
                    {
                        ddlHuyen.SelectedValue = coSo.HUYEN_ID.ToString();
                    }

                    // load list xa by huyện Id
                    LoadListXaByHuyenID(int.Parse(ddlHuyen.SelectedValue));
                    if (ddXa.Items.Count > 0)
                    {
                        ddXa.SelectedValue = coSo.XA_ID.ToString();
                    }

                    // load list thôn by xa id
                    LoadListThonByXaId(int.Parse(ddXa.SelectedValue));
                    if (ddlThon.Items.Count > 0)
                    {
                        ddlThon.SelectedValue = coSo.THON_ID.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load thông tin cơ sở: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void ShowMessage(string Message, EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        private void LoadListTinh()
        {
            try
            {
                List<TINH> listTinh = TinhService.GetListTinh();
                if (listTinh != null)
                {
                    ddlTinh.DataSource = listTinh;
                    ddlTinh.DataTextField = "TENTINH";
                    ddlTinh.DataValueField = "TINH_ID";
                    ddlTinh.DataBind();
                    // ddlHuyen.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách tỉnh: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadListHuyen()
        {
            try
            {
                List<HUYEN> listHuyen = HuyenService.GetListHuyen();
                if (listHuyen != null)
                {
                    ddlHuyen.DataSource = listHuyen;
                    ddlHuyen.DataTextField = "TENHUYEN";
                    ddlHuyen.DataValueField = "HUYEN_ID";
                    ddlHuyen.DataBind();
                    ddlHuyen.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách huyện: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadListXaByHuyenID(int huyenId)
        {
            try
            {
                List<XA> listXa = XaService.GetListXaByHuyen_ID(huyenId);
                if (listXa != null)
                {
                    ddXa.DataSource = listXa;
                    ddXa.DataTextField = "TENXA";
                    ddXa.DataValueField = "XA_ID";
                    ddXa.DataBind();
                    ddXa.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách xã: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadListThonByXaId(int xaId)
        {
            try
            {
                List<THON_XOM> listHuyen = ThonService.GetListThonXomByXaID(xaId);
                if (listHuyen != null)
                {
                    ddlThon.DataSource = listHuyen;
                    ddlThon.DataTextField = "TEN_THON";
                    ddlThon.DataValueField = "THON_ID";
                    ddlThon.DataBind();
                    ddlThon.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách thôn: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                COSODAOTAO coSoDaoTao = new COSODAOTAO(); ;
                if (btnAdd.Text == "Thêm mới")
                {
                    try
                    {
                        coSoDaoTao.MACOSO = txtMaCoSo.Text.Trim();
                        coSoDaoTao.TENCOSO = txtTenCoSo.Text.Trim();
                       
                        coSoDaoTao.DIENTHOAI = txtDienThoai.Text.Trim();
                        coSoDaoTao.FAX = txtFax.Text.Trim();
                        coSoDaoTao.EMAIL = txtEmail.Text.Trim();
                        coSoDaoTao.WEBSITE = txtWebsite.Text.Trim();
                      
                        coSoDaoTao.NGAYTAO = DateTime.Now;
                        coSoDaoTao.NGUOITAO = curentUserId;
                        coSoDaoTao.NGAYSUA = null;
                        coSoDaoTao.NGUOISUA = null;
                        coSoDaoTao.IS_APPROVE = cbPheDuyet.Checked;
                        coSoDaoTao.NGUOIDUYET = null;
                        coSoDaoTao.NGAYDUYET = null;
                        coSoDaoTao.IS_ACTIVE = cbKichHoat.Checked;
                        coSoDaoTao.NGUOIXUATBAN = null;
                        coSoDaoTao.NGAYXUATBAN = null;
                        coSoDaoTao.THON_ID = int.Parse(ddlThon.SelectedValue);
                        coSoDaoTao.XA_ID = int.Parse(ddXa.SelectedValue);
                        coSoDaoTao.HUYEN_ID = int.Parse(ddlHuyen.SelectedValue);
                        var mesage = DM_CoSoService.InsertCoSoDaoTao(coSoDaoTao);
                        if (mesage.Length > 0)
                        {
                            ShowMessage("Lỗi thêm mới:" + mesage, EnumSite.MessageType.Error);
                            return;
                        }

                        ShowMessage("Thêm mới thành công!", EnumSite.MessageType.Success);
                        Response.Redirect("CoSo.aspx");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi thêm mới cơ sở: " + ex.Message, EnumSite.MessageType.Error);
                    }
                }
                else // cập nhật
                {
                    try
                    {
                        var mesage = "";
                        int coso_Id = int.Parse(hdfID.Value);
                        coSoDaoTao.ID = coso_Id;
                        coSoDaoTao.MACOSO = txtMaCoSo.Text.Trim();
                        coSoDaoTao.TENCOSO = txtTenCoSo.Text.Trim();
                      
                        coSoDaoTao.DIENTHOAI = txtDienThoai.Text.Trim();
                        coSoDaoTao.FAX = txtFax.Text.Trim();
                        coSoDaoTao.EMAIL = txtEmail.Text.Trim();
                        coSoDaoTao.WEBSITE = txtWebsite.Text.Trim();
                        coSoDaoTao.NGAYSUA = DateTime.Now;
                        coSoDaoTao.NGUOISUA = curentUserId;
                        coSoDaoTao.IS_APPROVE = cbPheDuyet.Checked;
                        coSoDaoTao.NGUOIDUYET = curentUserId;
                        coSoDaoTao.NGAYDUYET = DateTime.Now;
                        coSoDaoTao.IS_ACTIVE = cbKichHoat.Checked;
                        coSoDaoTao.NGUOIXUATBAN = curentUserId;
                        coSoDaoTao.NGAYXUATBAN = DateTime.Now;
                        coSoDaoTao.THON_ID = int.Parse(ddlThon.SelectedValue);
                        coSoDaoTao.XA_ID = int.Parse(ddXa.SelectedValue);
                        coSoDaoTao.HUYEN_ID = int.Parse(ddlHuyen.SelectedValue);

                        mesage = DM_CoSoService.UpdatetCoSoDaoTao(coSoDaoTao);
                        if (mesage.Length > 0)
                        {
                            ShowMessage("Lỗi cập nhật thông tin cơ sở:" + mesage, EnumSite.MessageType.Error);
                            return;
                        }

                        ShowMessage("Cập nhật thành công!", EnumSite.MessageType.Success);
                        Response.Redirect("CoSo.aspx");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi cập nhật thông tin cơ sở: " + ex.Message, EnumSite.MessageType.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void ddlHuyen_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {
                LoadListXaByHuyenID(int.Parse(ddlHuyen.SelectedValue));
            }
            catch (Exception)
            {
            }
        }
        protected void ddXa_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadListThonByXaId(int.Parse(ddXa.SelectedValue));
            }
            catch (Exception)
            {
            }
        }
    }
}