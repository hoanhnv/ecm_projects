﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.Service;
using WebUI.Models;
using Utility;

namespace WebUI.UserControl.Roles
{
    public partial class ListRoles : System.Web.UI.UserControl
    {
        protected void ShowMessage(string Message, EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadListRoles();
            }
        }
        private void LoadListRoles()
        {
            try
            {
                long curentUserId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = long.Parse(Session["User_Id"].ToString());
                }

                var listTinh = RoleService.GetListRole();
                if (listTinh != null)
                {
                    grvList.Visible = true;
                    grvList.DataSource = listTinh;
                    grvList.DataBind();
                }
                else
                {
                    grvList.Visible = false;
                    ShowMessage("Không có bản ghi nào", EnumSite.MessageType.Success);
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load danh sách tỉnh: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void grvList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string comanName = e.CommandName;
                var roleId = int.Parse(e.CommandArgument.ToString());
                switch (comanName)
                {
                    case "sua":
                        lblTitle.Text = "Cập nhật";
                        btnAdd.Text = "Cập nhật";
                        ROLES tinh = RoleService.GetRoleByID(roleId);
                        if (tinh != null)
                        {
                            hdfId.Value = tinh.ROLE_ID.ToString();
                            txtRoleName.Text = tinh.ROLENAME;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
                        }
                        break;
                    case "xoa":
                        try
                        {
                            List<ROLE_FUNCTION> roleFunction = RoleFunctionService.GetListFunctionByRoleId(roleId);
                            List<USER_ROLE> userRole = User_RoleService.GetListUserRoleByRole(roleId, 1);
                            if (roleFunction.Count >0 || userRole.Count > 0)
                            {
                                ShowMessage("Vai trò này đang được dùng bạn không thể xóa", EnumSite.MessageType.Error);
                                return;
                            }
                            var message = RoleService.DeleteRole(roleId);
                            if (message.Length == 0)
                            {
                                LoadListRoles();
                                ShowMessage("Xóa tỉnh thành công!", EnumSite.MessageType.Success);
                            }
                            else
                            {
                                ShowMessage("Lỗi xóa tỉnh:" + message, EnumSite.MessageType.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi xóa:" + ex.Message, EnumSite.MessageType.Error);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi xảy ra:" + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void grvList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grvList.PageIndex = e.NewPageIndex;
                grvList.DataBind();
                LoadListRoles();
            }
            catch (Exception)
            {
            }
        }
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            lblTitle.Text = "Thêm mới";
            btnAdd.Text = "Thêm mới";
            hdfId.Value = "";
            txtRoleName.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                var mesage = "";
                if (btnAdd.Text == "Thêm mới")
                {
                    ROLES role = new ROLES();
                    role.ROLENAME = txtRoleName.Text.Trim();
                    mesage = RoleService.InsertRole(role);
                    if (mesage.Length > 0)
                    {
                        ShowMessage("Lỗi thêm mới: " + mesage, EnumSite.MessageType.Error);
                        return;
                    }
                    else
                    {
                        ShowMessage("Them mới thành công", EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    var roleId = int.Parse(hdfId.Value);
                    var roleName = txtRoleName.Text.Trim();
                    mesage = RoleService.UpdatetRole(roleId, roleName);
                    if (mesage.Length > 0)
                    {
                        ShowMessage("Lỗi cập nhật: " + mesage, EnumSite.MessageType.Error);
                        return;
                    }
                    else
                    {
                        ShowMessage("Cập nhật thành công ", EnumSite.MessageType.Success);
                    }
                }

                LoadListRoles();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi thao tác: " + ex.Message, EnumSite.MessageType.Success);
            }
        }
    }
}