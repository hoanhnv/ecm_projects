﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListDMChucNang.ascx.cs" Inherits="WebUI.UserControl.DMChucNang.ListDMChucNang1" %>
<div class="page-header">
    <h3>Quản lý danh mục chức năng</h3>
</div>
<br />
<div class="messagealert" id="alert_container">
</div>
<div class="panel-body">
    <div class="form-group">
        <div class="col-sm-10">
            <div style="float: left; width: 280px">
                <asp:TextBox ID="txtSearch" Width="250px" runat="server" class="form-control" placeholder="Từ khóa" />
            </div>
            <div style="float: left; width: 30%">
                <asp:Button ID="btnSearch" Text="Tìm" CssClass="btn btn-info" runat="server" OnClick="btnSearch_Click" />
            </div>
        </div>
    </div>
    <%-- poup--%>
    <div id="popupadd" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title">
                        <asp:Label Text="" ID="lblTitle" runat="server" /></h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body">

                        <table>
                            <tr>
                                <td>Mã chức năng:</td>
                                <td>
                                    <asp:HiddenField ID="hdfId" runat="server" />
                                    <asp:TextBox ID="txtMaChucNang" ClientIDMode="Static" runat="server" class="form-control"
                                        placeholder="nhập mã chức năng" /></td>
                            </tr>
                            <tr>
                                <td>Tên chức năng:</td>
                                <td>
                                    <asp:TextBox ID="txtTenChucNang" ClientIDMode="Static" runat="server" class="form-control"
                                        placeholder="nhập tên chức năng" /></td>
                            </tr>
                            <tr>
                                <td>Link:</td>
                                <td>
                                    <asp:TextBox ID="txtUrl" ClientIDMode="Static" runat="server" class="form-control"
                                        placeholder="nhập đường dẫn" /></td>
                            </tr>
                            <tr>
                                <td>Thứ tự hiển thị:</td>
                                <td>
                                    <asp:TextBox ID="txtThuTuHienThi" ClientIDMode="Static" runat="server" class="form-control"
                                        placeholder="nhập thứ tự hiển thị(số)" /></td>
                            </tr>
                            <tr>
                                <td>Chức năng cha:</td>
                                <td>
                                    <asp:DropDownList ID="ddlChucNang" ClientIDMode="Static" runat="server" class="form-control" Width="280px">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td>Trạng thái:</td>
                                <td>
                                    <asp:CheckBox Text="Active" ID="cbActive" runat="server" Checked="true" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:RadioButton GroupName="grShowHide" Checked="true" Text="Ẩn" ID="rdHide" runat="server" />
                                    &nbsp;
                                    <asp:RadioButton GroupName="grShowHide" Text="Hiện" ID="rdShow" runat="server" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:Label Text="" ID="lblMessage" ClientIDMode="Static" ForeColor="Red" runat="server" /></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">hủy</button>
                    <asp:Button ID="btnAdd" ClientIDMode="Static" runat="server" class="btn btn-primary" OnClick="btnAdd_Click"
                        Text="Thêm mới" />
                </div>
            </div>
        </div>
    </div>
</div>
<div style="float: right; margin: 0px 0px 5px 0px">
    <asp:Button Text="Thêm mới" runat="server" ID="btnAddNew" CssClass="btn btn-info" OnClick="btnAddNew_Click" />
</div>
<asp:GridView runat="server" ID="grvList" ClientIDMode="Static" Width="100%" CssClass="table table-striped table-bordered table-hover dataTable no-footer DTTT_selectable"
    AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="grvList_PageIndexChanging" PageSize="30"
    OnRowCommand="grvList_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="Thứ tự">
            <ItemTemplate>
                <%#(Convert.ToInt32(DataBinder.Eval(Container, "RowIndex")) + 1)%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="7%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Mã chức năng">
            <ItemTemplate>
                <%# Eval("MA_CHUCNANG")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="20%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tên chức năng">
            <ItemTemplate>
                <%# Eval("TEN_CHUCNANG")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="30%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="TTự hiển thị">
            <ItemTemplate>
                <%# Eval("TT_HIENTHI")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="10%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Trạng thái">
            <ItemTemplate>
                <%# Eval("TRANGTHAI")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="20%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="">
            <ItemTemplate>
                <asp:LinkButton ID="btnEdit" Text="Sửa" ToolTip="Sửa" CommandArgument='<%# Eval("DM_CHUCNANG_ID") %>'
                    CommandName="sua" runat="server"><span class="ace-icon fa fa-pencil bigger-130"></span></asp:LinkButton>
                &nbsp;
                                        <asp:LinkButton ID="btnDelete" Text="Xóa" ToolTip="Xóa" OnClientClick="return confirm('Bạn có chắc muốn xóa?')"
                                            CommandName="xoa" CommandArgument='<%# Eval("DM_CHUCNANG_ID") %>' runat="server"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="12%" HorizontalAlign="Center" />
        </asp:TemplateField>
    </Columns>
    <PagerSettings FirstPageText="Đầu" LastPageText="Cuối" Mode="NumericFirstLast" NextPageText="Tiếp"
        PreviousPageText="Trước" />
    <PagerStyle BackColor="White" BorderColor="#3366CC" BorderStyle="Outset" BorderWidth="1px"
        CssClass="paging" ForeColor="Blue" Height="40px" HorizontalAlign="Right" Width="200px" />
</asp:GridView>
<script>
    $(document).ready(function () {
        $('#btnAdd').click(function () {
            if ($('#txtChucNang').val() == '') {
                $('#lblMessage').text('Mã chức năng không được để trống');
                return false;
            }
            if ($('#txtTenChucNang').val() == '') {
                $('#lblMessage').text('Tên chức năng không được để trống');
                return false;
            }
            if ($('#txtUrl').val() == '') {
                $('#lblMessage').text('Đường dẫn không được để trống');
                return false;
            }

            if ($('#txtThuTuHienThi').val() == '') {
                $('#lblMessage').text('Thứ tự hiển thị không được để trống');
                return false;
            }
        });
    });
</script>
