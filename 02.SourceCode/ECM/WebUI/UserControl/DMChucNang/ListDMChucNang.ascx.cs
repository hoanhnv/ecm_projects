﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility;
using WebUI.Service;
using WebUI.Models;

namespace WebUI.UserControl.DMChucNang
{
    public partial class ListDMChucNang1 : System.Web.UI.UserControl
    {
        protected void ShowMessage(string Message, EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadListChucNang();
            }
        }
        private void LoadListChucNang(string tenChucNang = "")
        {
            try
            {
                ECM_CSDLEntities context = new ECM_CSDLEntities();
                long curentUserId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = long.Parse(Session["User_Id"].ToString());
                }

                if (tenChucNang.Length > 0) // serach
                {
                    var listChucNang = (from cn in context.DM_CHUCNANG
                                        where (cn.TEN_CHUCNANG.Contains(tenChucNang))
                                        select new
                                        {
                                            DM_CHUCNANG_ID = cn.DM_CHUCNANG_ID,
                                            MA_CHUCNANG = cn.MA_CHUCNANG,
                                            TEN_CHUCNANG = cn.TEN_CHUCNANG,
                                            TT_HIENTHI = cn.TT_HIENTHI,
                                            TRANGTHAI = cn.TRANGTHAI > 0 ? "Kích hoạt" : "Không kích hoạt"
                                        }).OrderBy(o => o.TEN_CHUCNANG).ToList();
                    if (listChucNang != null)
                    {
                        grvList.Visible = true;
                        grvList.DataSource = listChucNang;
                        grvList.DataBind();
                    }
                    else
                    {
                        grvList.Visible = false;
                        ShowMessage("Không có bản ghi nào", EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    var listChucNang = (from cn in context.DM_CHUCNANG
                                        select new
                                        {
                                            DM_CHUCNANG_ID = cn.DM_CHUCNANG_ID,
                                            MA_CHUCNANG = cn.MA_CHUCNANG,
                                            TEN_CHUCNANG = cn.TEN_CHUCNANG,
                                            TT_HIENTHI = cn.TT_HIENTHI,
                                            TRANGTHAI = cn.TRANGTHAI > 0 ? "Kích hoạt" : "Không kích hoạt"
                                        }).OrderBy(o => o.TEN_CHUCNANG).ToList();
                    if (listChucNang != null)
                    {
                        grvList.Visible = true;
                        grvList.DataSource = listChucNang;
                        grvList.DataBind();
                    }
                    else
                    {
                        grvList.Visible = false;
                        ShowMessage("Không có bản ghi nào", EnumSite.MessageType.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load danh sách tỉnh: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadChucNang()
        {
            try
            {
                List<DM_CHUCNANG> listDanhMucChucNang = DM_ChucNangService.GetListDanhMucChucNang();
                if (listDanhMucChucNang != null)
                {
                    ddlChucNang.DataSource = listDanhMucChucNang;
                    ddlChucNang.DataTextField = "TEN_CHUCNANG";
                    ddlChucNang.DataValueField = "DM_CHUCNANG_ID";
                    ddlChucNang.DataBind();
                    ddlChucNang.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách tỉnh: " + ex.Message, Utility.EnumSite.MessageType.Error);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim().Length == 0)
            {
                ShowMessage("Bạn chưa nhập tên chức năng cần tìm", EnumSite.MessageType.Success);
                return;
            }
            else
            {
                LoadListChucNang(txtSearch.Text.Trim());
            }
        }
        protected void grvList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string comanName = e.CommandName;
                var chuNangId = int.Parse(e.CommandArgument.ToString());
                switch (comanName)
                {
                    case "sua":
                        lblTitle.Text = "Cập nhật";
                        btnAdd.Text = "Cập nhật";
                        DM_CHUCNANG chucNang = DM_ChucNangService.GetDanhMucChucNangById(chuNangId);
                        if (chucNang != null)
                        {
                            hdfId.Value = chucNang.DM_CHUCNANG_ID.ToString();
                            txtMaChucNang.Text = chucNang.MA_CHUCNANG;
                            txtTenChucNang.Text = chucNang.TEN_CHUCNANG;
                            txtUrl.Text = chucNang.URL;
                            txtThuTuHienThi.Text = chucNang.TT_HIENTHI.ToString();
                            if (chucNang.TRANGTHAI > 0)
                            {
                                cbActive.Checked = true;
                            }
                            if (chucNang.IS_HIDDEN > 0)
                            {
                                rdShow.Checked = true;
                            }
                            else
                            {
                                rdHide.Checked = true;
                            }

                            LoadChucNang();
                            if (ddlChucNang.Items.Count > 0)
                            {
                                ddlChucNang.SelectedValue = chucNang.CHUCNANG_CHA.ToString();
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
                        }
                        break;
                    case "xoa":
                        try
                        {
                            var message = DM_ChucNangService.DeleteChucNangById(chuNangId);
                            if (message.Length == 0)
                            {
                                LoadListChucNang();
                                ShowMessage("Xóa chức năng thành công!", EnumSite.MessageType.Success);
                            }
                            else
                            {
                                ShowMessage("Lỗi xóa chức năng:" + message, EnumSite.MessageType.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi xóa:" + ex.Message, EnumSite.MessageType.Error);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi xảy ra:" + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void grvList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grvList.PageIndex = e.NewPageIndex;
                grvList.DataBind();
                LoadListChucNang();
            }
            catch (Exception)
            {
            }
        }
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            lblTitle.Text = "Thêm mới";
            btnAdd.Text = "Thêm mới";
            hdfId.Value = "";
            txtMaChucNang.Text = "";
            txtTenChucNang.Text = "";
            txtUrl.Text = "";
            txtThuTuHienThi.Text = "";
            LoadChucNang();
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                var mesage = "";
                DM_CHUCNANG chucNang = new DM_CHUCNANG();
                if (btnAdd.Text == "Thêm mới")
                {
                    chucNang.MA_CHUCNANG = txtMaChucNang.Text.Trim();
                    chucNang.TEN_CHUCNANG = txtTenChucNang.Text.Trim();
                    chucNang.TRANGTHAI = cbActive.Checked ? 1 : 0;
                    chucNang.NGAYTAO = DateTime.Now;
                    if (ddlChucNang.Items.Count > 0)
                    {
                        chucNang.CHUCNANG_CHA = int.Parse(ddlChucNang.SelectedValue);
                    }
                    else
                    {
                        chucNang.CHUCNANG_CHA = 0;
                    }

                    chucNang.URL = txtUrl.Text.Trim();
                    chucNang.TT_HIENTHI = int.Parse(txtThuTuHienThi.Text.Trim());
                    chucNang.NGUOITAO = Session["UserName"].ToString();
                    if (rdHide.Checked)
                    {
                        chucNang.IS_HIDDEN = 0;
                    }
                    else
                    {
                        chucNang.IS_HIDDEN = 1;
                    }
                    mesage = DM_ChucNangService.InsertChucNang(chucNang);
                    if (mesage.Length > 0)
                    {
                        ShowMessage("Lỗi thêm mới: " + mesage, EnumSite.MessageType.Error);
                        return;
                    }
                    else
                    {
                        ShowMessage("Them mới thành công", EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    var chucNangId = int.Parse(hdfId.Value);

                    chucNang.DM_CHUCNANG_ID = chucNangId;
                    chucNang.MA_CHUCNANG = txtMaChucNang.Text.Trim();
                    chucNang.TEN_CHUCNANG = txtTenChucNang.Text.Trim();
                    chucNang.TRANGTHAI = cbActive.Checked ? 1 : 0;
                    chucNang.NGAYSUA = DateTime.Now;
                    chucNang.CHUCNANG_CHA = int.Parse(ddlChucNang.SelectedValue);
                    chucNang.URL = txtUrl.Text.Trim();
                    chucNang.TT_HIENTHI = int.Parse(txtThuTuHienThi.Text.Trim());
                    chucNang.NGUOISUA = Session["UserName"].ToString();
                    if (rdHide.Checked)
                    {
                        chucNang.IS_HIDDEN = 0;
                    }
                    else
                    {
                        chucNang.IS_HIDDEN = 1;
                    }

                    mesage = DM_ChucNangService.UpdatetChucNang(chucNang);
                    if (mesage.Length > 0)
                    {
                        ShowMessage("Lỗi cập nhật: " + mesage, EnumSite.MessageType.Error);
                        return;
                    }
                    else
                    {
                        ShowMessage("Cập nhật thành công ", EnumSite.MessageType.Success);
                    }
                }

                LoadListChucNang();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi thao tác: " + ex.Message, EnumSite.MessageType.Error);
            }
        }

        //private void LoadListGroupArticle()
        //{
        //    try
        //    {
        //        List<DropDowItem> listITem = new List<DropDowItem>();
        //        List<GroupArticle> listGroupArticle;
        //        List<GroupArticle> listGroupArticleChild;
        //        DropDowItem dopDowItem;
        //        listGroupArticle = context.GroupArticles.Where(o => o.ParentId == 0 && o.GroupArticleId != (int)EnumWebsite.GroupArticle.TrangChu && o.GroupArticleId != (int)EnumWebsite.GroupArticle.TinNoiBat).OrderBy(o => o.GroupArticleId).ToList();

        //        foreach (GroupArticle item in listGroupArticle)
        //        {
        //            dopDowItem = new DropDowItem();
        //            listGroupArticleChild = context.GroupArticles.Where(o => o.ParentId == item.GroupArticleId).ToList();
        //            if (listGroupArticleChild.Count > 0)
        //            {
        //                dopDowItem.TextValue = item.GroupName;
        //                dopDowItem.DataValue = item.GroupArticleId.ToString();
        //                listITem.Add(dopDowItem);
        //                foreach (GroupArticle itemCchild in listGroupArticleChild)
        //                {
        //                    dopDowItem = new DropDowItem();
        //                    dopDowItem.TextValue = "---- " + itemCchild.GroupName;
        //                    dopDowItem.DataValue = itemCchild.GroupArticleId.ToString();
        //                    listITem.Add(dopDowItem);
        //                }
        //            }
        //            else
        //            {
        //                dopDowItem.TextValue = item.GroupName;
        //                dopDowItem.DataValue = item.GroupArticleId.ToString();
        //                listITem.Add(dopDowItem);
        //            }
        //        }

        //        if (listITem.Count > 0)
        //        {
        //            ddlGroupArticle.DataSource = listITem;
        //            ddlGroupArticle.DataTextField = "TextValue";
        //            ddlGroupArticle.DataValueField = "DataValue";
        //            ddlGroupArticle.DataBind();
        //            ddlGroupArticle.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ShowMessage("Lỗi tải nhóm tin: " + ex.Message, MessageType.Error);
        //    }
        //}
    }
}

public class DropDowItem
{
    public string TextValue { get; set; }
    public string DataValue { get; set; }
}