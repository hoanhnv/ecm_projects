﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.Service;
using WebUI.Models;
using Utility;

namespace WebUI.UserControl.Huyen
{
    public partial class ListHuyen : System.Web.UI.UserControl
    {
        protected void ShowMessage(string Message, EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadListHuyen();
            }
        }
        private void LoadListHuyen(string tenHuyen = "")
        {
            try
            {
                ECM_CSDLEntities context = new ECM_CSDLEntities();
                long curentUserId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = long.Parse(Session["User_Id"].ToString());
                }

                if (tenHuyen.Length > 0) // serach
                {
                    //var listHuyen = HuyenService.GetListHuyenByName(tenHuyen);
                    var listHuyen = (from h in context.HUYEN
                                     join t in context.TINH on h.TINH_ID equals t.TINH_ID
                                     where (h.TENHUYEN.Contains(tenHuyen))
                                     select new
                                     {
                                         HUYEN_ID = h.HUYEN_ID,
                                         MAHUYEN = h.MAHUYEN,
                                         TENHUYEN = h.TENHUYEN,
                                         TENTINH = t.TENTINH
                                     }).OrderBy(o => o.TENHUYEN).ToList();
                    if (listHuyen != null)
                    {
                        grvList.Visible = true;
                        grvList.DataSource = listHuyen;
                        grvList.DataBind();
                    }
                    else
                    {
                        grvList.Visible = false;
                        ShowMessage("Không có bản ghi nào", EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    //var listHuyenh = HuyenService.GetListHuyen();
                    var listHuyenh = (from h in context.HUYEN
                                      join t in context.TINH on h.TINH_ID equals t.TINH_ID
                                      select new
                                      {
                                          HUYEN_ID = h.HUYEN_ID,
                                          MAHUYEN = h.MAHUYEN,
                                          TENHUYEN = h.TENHUYEN,
                                          TENTINH = t.TENTINH
                                      }).OrderBy(o => o.TENHUYEN).ToList();

                    if (listHuyenh != null)
                    {
                        grvList.Visible = true;
                        grvList.DataSource = listHuyenh;
                        grvList.DataBind();
                    }
                    else
                    {
                        grvList.Visible = false;
                        ShowMessage("Không có bản ghi nào", EnumSite.MessageType.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load danh sách tỉnh: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadTinh()
        {
            try
            {
                List<TINH> listTinh = TinhService.GetListTinh();
                if (listTinh != null)
                {
                    ddlTinh.DataSource = listTinh;
                    ddlTinh.DataTextField = "TENTINH";
                    ddlTinh.DataValueField = "TINH_ID";
                    ddlTinh.DataBind();
                    ddlTinh.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách tỉnh: " + ex.Message, Utility.EnumSite.MessageType.Error);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim().Length == 0)
            {
                ShowMessage("Bạn chưa nhập tên huyện cần tìm", EnumSite.MessageType.Success);
                return;
            }
            else
            {
                LoadListHuyen(txtSearch.Text.Trim());
            }
        }
        protected void grvList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string comanName = e.CommandName;
                var huyenId = int.Parse(e.CommandArgument.ToString());
                switch (comanName)
                {
                    case "sua":
                        lblTitle.Text = "Cập nhật";
                        btnAdd.Text = "Cập nhật";
                        HUYEN huyen = HuyenService.GetHuyenByID(huyenId);
                        if (huyen != null)
                        {
                            hdfId.Value = huyen.HUYEN_ID.ToString();
                            txtMaHuyen.Text = huyen.MAHUYEN;
                            txtTenHuyen.Text = huyen.TENHUYEN;
                            LoadTinh();
                            if (ddlTinh.Items.Count > 0)
                            {
                                ddlTinh.SelectedValue = huyen.TINH_ID.ToString();
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
                        }
                        break;
                    case "xoa":
                        try
                        {
                            var message = HuyenService.DeletetHuyen(huyenId);
                            if (message.Length == 0)
                            {
                                LoadListHuyen();
                                ShowMessage("Xóa huyện thành công!", EnumSite.MessageType.Success);
                            }
                            else
                            {
                                ShowMessage("Lỗi xóa tỉnh:" + message, EnumSite.MessageType.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi xóa:" + ex.Message, EnumSite.MessageType.Error);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi xảy ra:" + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void grvList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grvList.PageIndex = e.NewPageIndex;
                grvList.DataBind();
                LoadListHuyen();
            }
            catch (Exception)
            {
            }
        }
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            lblTitle.Text = "Thêm mới";
            btnAdd.Text = "Thêm mới";
            hdfId.Value = "";
            txtMaHuyen.Text = "";
            txtTenHuyen.Text = "";
            LoadTinh();
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                var mesage = "";
                if (btnAdd.Text == "Thêm mới")
                {
                    HUYEN huyen = new HUYEN();
                    huyen.MAHUYEN = txtMaHuyen.Text.Trim();
                    huyen.TENHUYEN = txtTenHuyen.Text.Trim();
                    huyen.TINH_ID = int.Parse(ddlTinh.SelectedValue);
                    mesage = HuyenService.InsertHuyen(huyen);
                    if (mesage.Length > 0)
                    {
                        ShowMessage("Lỗi thêm mới: " + mesage, EnumSite.MessageType.Error);
                        return;
                    }
                    else
                    {
                        ShowMessage("Them mới thành công", EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    var huyenId = int.Parse(hdfId.Value);
                    var maHuyen = txtMaHuyen.Text.Trim();
                    var tenHuyen = txtTenHuyen.Text.Trim();
                    var tinhId = int.Parse(ddlTinh.SelectedValue);

                    mesage = HuyenService.UpdatetHuyen(huyenId, maHuyen, tenHuyen, tinhId);
                    if (mesage.Length > 0)
                    {
                        ShowMessage("Lỗi cập nhật: " + mesage, EnumSite.MessageType.Error);
                        return;
                    }
                    else
                    {
                        ShowMessage("Cập nhật thành công ", EnumSite.MessageType.Success);
                    }
                }

                LoadListHuyen();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi thao tác: " + ex.Message, EnumSite.MessageType.Success);
            }
        }
    }
}