﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListHuyen.ascx.cs" Inherits="WebUI.UserControl.Huyen.ListHuyen" %>
<div class="col-md-12">
    <div class="page-header">
        <%--<div class="panel-title">--%>
        <h3>Quản lý huyện</h3>
        <%--</div>--%>
    </div>
    <br />
    <div class="messagealert" id="alert_container">
    </div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-sm-10">
                <div style="float: left; width: 280px">
                    <asp:TextBox ID="txtSearch" Width="250px" runat="server" class="form-control" placeholder="Từ khóa" />
                </div>
                <div style="float: left; width: 30%">
                    <asp:Button ID="btnSearch" Text="Tìm" CssClass="btn btn-info" runat="server" OnClick="btnSearch_Click" />
                </div>
            </div>
        </div>
        <%-- poup--%>
        <div id="popupadd" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;</button>
                        <h4 class="modal-title">
                            <asp:Label Text="" ID="lblTitle" runat="server" /></h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputEmail3">
                                    Mã huyện:</label>
                                <asp:HiddenField ID="hdfId" runat="server" />
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtMaHuyen" ClientIDMode="Static" runat="server" class="form-control"
                                        placeholder="nhập mã huyện" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputPassword3">
                                    Tên huyện:</label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtTenHuyen" ClientIDMode="Static" runat="server" class="form-control"
                                        placeholder="nhập tên huyện" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputPassword3">
                                    Tỉnh:</label>
                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlTinh" ClientIDMode="Static" runat="server" class="form-control" Width="280px">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label Text="" ID="lblMessage" ClientIDMode="Static" ForeColor="Red" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">hủy</button>
                        <asp:Button ID="btnAdd" ClientIDMode="Static" runat="server" class="btn btn-primary" OnClick="btnAdd_Click"
                            Text="Thêm mới" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--  popup--%>
<%--       <div class="form-group">
            <div class="col-sm-offset-0 col-sm-12">
                <div id="first-container">--%>
<div style="float: right; margin: 0px 0px 5px 0px">
    <asp:Button Text="Thêm mới" runat="server" ID="btnAddNew" CssClass="btn btn-info" OnClick="btnAddNew_Click" />
</div>
<asp:GridView runat="server" ID="grvList" ClientIDMode="Static" Width="100%" CssClass="table table-striped table-bordered table-hover dataTable no-footer DTTT_selectable"
    AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="grvList_PageIndexChanging"
    OnRowCommand="grvList_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="Thứ tự">
            <ItemTemplate>
                <%#(Convert.ToInt32(DataBinder.Eval(Container, "RowIndex")) + 1)%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="7%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Mã huyện">
            <ItemTemplate>
                <%# Eval("MAHUYEN")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="20%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tên huyện">
            <ItemTemplate>
                <%# Eval("TENHUYEN")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="40%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tỉnh">
            <ItemTemplate>
                <%# Eval("TENTINH")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="20%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="">
            <ItemTemplate>
                <asp:LinkButton ID="btnEdit" Text="Sửa" ToolTip="Sửa" CommandArgument='<%# Eval("HUYEN_ID") %>'
                    CommandName="sua" runat="server"><span class="ace-icon fa fa-pencil bigger-130"></span></asp:LinkButton>
                &nbsp;
                                        <asp:LinkButton ID="btnDelete" Text="Xóa" ToolTip="Xóa" OnClientClick="return confirm('Bạn có chắc muốn xóa?')"
                                            CommandName="xoa" CommandArgument='<%# Eval("HUYEN_ID") %>' runat="server"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="12%" HorizontalAlign="Center" />
        </asp:TemplateField>
    </Columns>
    <PagerSettings FirstPageText="Đầu" LastPageText="Cuối" Mode="NumericFirstLast" NextPageText="Tiếp"
        PreviousPageText="Trước" />
    <PagerStyle BackColor="White" BorderColor="#3366CC" BorderStyle="Outset" BorderWidth="1px"
        CssClass="paging" ForeColor="Blue" Height="40px" HorizontalAlign="Right" Width="200px" />
</asp:GridView>
<%--     </div>
                <div class="clear">
                </div>
            </div>
        </div>--%>

<script>
    $(document).ready(function () {
        $('#btnAdd').click(function () {
            if ($('#txtMaHuyen').val() == '') {
                $('#lblMessage').text('Mã huyện không được để trống');
                return false;
            }
            if ($('#txtTenHuyen').val() == '') {
                $('#lblMessage').text('Tên huyện không được để trống');
                return false;
            }
            if ($('#ddlTinh').val() == '0') {
                $('#lblMessage').text('Chưa chọn tỉnh');
                return false;
            }
        });
    });
</script>
