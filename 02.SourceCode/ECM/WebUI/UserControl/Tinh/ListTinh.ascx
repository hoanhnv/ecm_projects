﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListTinh.ascx.cs" Inherits="WebUI.UserControl.Tinh.ListTinh1" %>
<div class="col-md-12">
    <div class="panel-heading">
        <div class="panel-title">
            <h3>Quản lý tỉnh</h3>
        </div>
    </div>
    <br />
    <div class="messagealert" id="alert_container">
    </div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-sm-10">
                <div style="float: left; width: 280px">
                    <asp:TextBox ID="txtSearch" Width="250px" runat="server" class="form-control" placeholder="nhập tên tỉnh" />
                </div>
                <div style="float: left; width: 30%">
                    <asp:Button ID="btnSearch" Text="Tìm" CssClass="btn btn-info" runat="server" OnClick="btnSearch_Click" />
                </div>
            </div>
        </div>
        <%-- poup--%>
        <div id="popupadd" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;</button>
                        <h4 class="modal-title">
                            <asp:Label Text="" ID="lblTitle" runat="server" /></h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputEmail3">
                                    Mã tỉnh:</label>
                                <asp:HiddenField ID="hdfId" runat="server" />
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtMaTinh" ClientIDMode="Static" runat="server" class="form-control"
                                        placeholder="nhập mã tỉnh" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputPassword3">
                                    Tên tỉnh:</label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtTenTinh" ClientIDMode="Static" runat="server" class="form-control"
                                        placeholder="nhập tên tỉnh" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label Text="" ID="lblMessage" ClientIDMode="Static" ForeColor="Red" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">hủy</button>
                        <asp:Button ID="btnAdd" ClientIDMode="Static" runat="server" class="btn btn-primary" OnClick="btnAdd_Click"
                            Text="Thêm mới" />
                    </div>
                </div>
            </div>
        </div>
        <%--  popup--%>
        <div class="form-group">
            <div class="col-sm-offset-0 col-sm-12">
                <div id="first-container">
                    <div style="float: right; margin: 0px 0px 5px 0px">
                        <asp:Button Text="Thêm mới" runat="server" ID="btnAddNew" CssClass="btn btn-info" OnClick="btnAddNew_Click" />
                    </div>
                    <asp:GridView runat="server" ID="grvList" ClientIDMode="Static" Width="100%" CssClass="table table-striped table-bordered table-hover dataTable no-footer DTTT_selectable"
                        AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="grvList_PageIndexChanging"
                        OnRowCommand="grvList_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Thứ tự">
                                <ItemTemplate>
                                    <%#(Convert.ToInt32(DataBinder.Eval(Container, "RowIndex")) + 1)%>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle Width="7%" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mã tỉnh">
                                <ItemTemplate>
                                    <%# Eval("MATINH")%>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle Width="30%" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tên tỉnh">
                                <ItemTemplate>
                                    <%# Eval("TENTINH")%>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle Width="50%" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEdit" Text="Sửa" ToolTip="Sửa" CommandArgument='<%# Eval("TINH_ID") %>'
                                        CommandName="sua" runat="server"><span class="ace-icon fa fa-pencil bigger-130"></span></asp:LinkButton>
                                    &nbsp;
                                        <asp:LinkButton ID="btnDelete" Text="Xóa" ToolTip="Xóa" OnClientClick="return confirm('Bạn có chắc muốn xóa?')"
                                            CommandName="xoa" CommandArgument='<%# Eval("TINH_ID") %>' runat="server"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle Width="12%" HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings FirstPageText="Đầu" LastPageText="Cuối" Mode="NumericFirstLast" NextPageText="Tiếp"
                            PreviousPageText="Trước" />
                        <PagerStyle BackColor="White" BorderColor="#3366CC" BorderStyle="Outset" BorderWidth="1px"
                            CssClass="paging" ForeColor="Blue" Height="40px" HorizontalAlign="Right" Width="200px" />
                    </asp:GridView>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#btnAdd').click(function () {
            if ($('#txtMaTinh').val() == '') {
                $('#lblMessage').text('Mã tỉnh không được để trống');
                return false;
            }
            if ($('#txtTenTinh').val() == '') {
                $('#lblMessage').text('Tên tỉnh không được để trống');
                return false;
            }
        });
    });
</script>
