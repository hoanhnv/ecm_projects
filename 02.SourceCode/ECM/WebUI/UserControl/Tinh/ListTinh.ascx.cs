﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.Service;
using WebUI.Models;
using Utility;

namespace WebUI.UserControl.Tinh
{
    public partial class ListTinh1 : System.Web.UI.UserControl
    {
        protected void ShowMessage(string Message, EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadListTinh();
            }
        }
        private void LoadListTinh(string tenTinh = "")
        {
            try
            {
                long curentUserId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = long.Parse(Session["User_Id"].ToString());
                }

                if (tenTinh.Length > 0) // serach
                {
                    var listTinh = TinhService.GetListTinhByName(tenTinh);
                    if (listTinh.Count > 0)
                    {
                        grvList.Visible = true;
                        grvList.DataSource = listTinh;
                        grvList.DataBind();
                    }
                    else
                    {
                        grvList.Visible = false;
                        ShowMessage("Không có bản ghi nào", EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    var listTinh = TinhService.GetListTinh();
                    if (listTinh.Count > 0)
                    {
                        grvList.Visible = true;
                        grvList.DataSource = listTinh;
                        grvList.DataBind();
                    }
                    else
                    {
                        grvList.Visible = false;
                        ShowMessage("Không có bản ghi nào", EnumSite.MessageType.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load danh sách tỉnh: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim().Length == 0)
            {
                ShowMessage("Bạn chưa nhập tên tỉnh tìm", EnumSite.MessageType.Success);
                return;
            }
            else
            {
                LoadListTinh(txtSearch.Text.Trim());
            }
        }
        protected void grvList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string comanName = e.CommandName;
                var tinhId = int.Parse(e.CommandArgument.ToString());
                switch (comanName)
                {
                    case "sua":
                        lblTitle.Text = "Cập nhật";
                        btnAdd.Text = "Cập nhật";
                        TINH tinh = TinhService.GetTinhByID(tinhId);
                        if (tinh != null)
                        {
                            hdfId.Value = tinh.TINH_ID.ToString();
                            txtMaTinh.Text = tinh.MATINH;
                            txtTenTinh.Text = tinh.TENTINH;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
                        }
                        break;
                    case "xoa":
                        try
                        {
                            var message = TinhService.DeleteTinh(tinhId);
                            if (message.Length == 0)
                            {
                                LoadListTinh();
                                ShowMessage("Xóa tỉnh thành công!", EnumSite.MessageType.Success);
                            }
                            else
                            {
                                ShowMessage("Lỗi xóa tỉnh:" + message, EnumSite.MessageType.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi xóa:" + ex.Message, EnumSite.MessageType.Error);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi xảy ra:" + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void grvList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grvList.PageIndex = e.NewPageIndex;
                grvList.DataBind();
                LoadListTinh();
            }
            catch (Exception)
            {
            }
        }
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            lblTitle.Text = "Thêm mới";
            btnAdd.Text = "Thêm mới";
            hdfId.Value = "";
            txtMaTinh.Text = "";
            txtTenTinh.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                var mesage = "";
                if (btnAdd.Text == "Thêm mới")
                {
                    TINH tinh = new TINH();
                    tinh.MATINH = txtMaTinh.Text.Trim();
                    tinh.TENTINH = txtTenTinh.Text.Trim();
                    mesage = TinhService.InsertTinh(tinh);
                    if (mesage.Length > 0)
                    {
                        ShowMessage("Lỗi thêm mới: " + mesage, EnumSite.MessageType.Error);
                        return;
                    }
                    else
                    {
                        ShowMessage("Them mới thành công", EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    var tinhId = int.Parse(hdfId.Value);
                    var matinh = txtMaTinh.Text.Trim();
                    var tentinh = txtTenTinh.Text.Trim();
                    mesage = TinhService.UpdatetTinh(tinhId, matinh, tentinh);
                    if (mesage.Length > 0)
                    {
                        ShowMessage("Lỗi cập nhật: " + mesage, EnumSite.MessageType.Error);
                        return;
                    }
                    else
                    {
                        ShowMessage("Cập nhật thành công ", EnumSite.MessageType.Success);
                    }
                }

                LoadListTinh();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi thao tác: " + ex.Message, EnumSite.MessageType.Success);
            }
        }
    }
}