﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.Service;
using WebUI.Models;
using Utility;

namespace WebUI.UserControl.Xa
{
    public partial class ListXa : System.Web.UI.UserControl
    {
        protected void ShowMessage(string Message, EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadListXa();
            }
        }
        private void LoadListXa(string tenXa = "")
        {
            try
            {
                long curentUserId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = long.Parse(Session["User_Id"].ToString());
                }

                if (tenXa.Length > 0) // serach
                {
                    var listXa = XaService.GetListXaByName(tenXa);
                    if (listXa.Count > 0)
                    {
                        grvList.Visible = true;
                        grvList.DataSource = listXa;
                        grvList.DataBind();
                    }
                    else
                    {
                        grvList.Visible = false;
                        ShowMessage("Không có bản ghi nào", EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    var listXa = XaService.GetListXa();
                    if (listXa.Count > 0)
                    {
                        grvList.Visible = true;
                        grvList.DataSource = listXa;
                        grvList.DataBind(); 
                    }
                    else
                    {
                        grvList.Visible = false;
                        ShowMessage("Không có bản ghi nào", EnumSite.MessageType.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load danh sách tỉnh: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadHuyen()
        {
            try
            {
                List<HUYEN> listHuyen = HuyenService.GetListHuyen();
                if (listHuyen.Count > 0)
                {
                    ddlHuyen.DataSource = listHuyen;
                    ddlHuyen.DataTextField = "TENHUYEN";
                    ddlHuyen.DataValueField = "HUYEN_ID";
                    ddlHuyen.DataBind();
                    ddlHuyen.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách huyện: " + ex.Message, Utility.EnumSite.MessageType.Error);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim().Length == 0)
            {
                ShowMessage("Bạn chưa nhập tên xã cần tìm", EnumSite.MessageType.Success);
                return;
            }
            else
            {
                LoadListXa(txtSearch.Text.Trim());
            }
        }
        protected void grvList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string comanName = e.CommandName;
                var xaId = int.Parse(e.CommandArgument.ToString());
                switch (comanName)
                {
                    case "sua":
                        lblTitle.Text = "Cập nhật";
                        btnAdd.Text = "Cập nhật";
                        XA huyen = XaService.GetXaByID(xaId);
                        if (huyen != null)
                        {
                            hdfId.Value = huyen.XA_ID.ToString();
                            txtMaXa.Text = huyen.MAXA;
                            txtTenXa.Text = huyen.TENXA;
                            LoadHuyen();
                            if (ddlHuyen.Items.Count > 0)
                            {
                                ddlHuyen.SelectedValue = huyen.HUYEN_ID.ToString();
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
                        }
                        break;
                    case "xoa":
                        try
                        {
                            var message = XaService.DeleteXa(xaId);
                            if (message.Length == 0)
                            {
                                LoadListXa();
                                ShowMessage("Xóa xã thành công!", EnumSite.MessageType.Success);
                            }
                            else
                            {
                                ShowMessage("Lỗi xóa tỉnh:" + message, EnumSite.MessageType.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi xóa:" + ex.Message, EnumSite.MessageType.Error);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi xảy ra:" + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void grvList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grvList.PageIndex = e.NewPageIndex;
                grvList.DataBind();
                LoadListXa();
            }
            catch (Exception)
            {
            }
        }
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            lblTitle.Text = "Thêm mới";
            btnAdd.Text = "Thêm mới";
            hdfId.Value = "";
            txtMaXa.Text = "";
            txtTenXa.Text = "";
            LoadHuyen();
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                var mesage = "";
                if (btnAdd.Text == "Thêm mới")
                {
                    XA xa = new XA();
                    xa.MAXA = txtMaXa.Text.Trim();
                    xa.TENXA = txtTenXa.Text.Trim();
                    xa.HUYEN_ID = int.Parse(ddlHuyen.SelectedValue);
                    mesage = XaService.InsertXa(xa);
                    if (mesage.Length > 0)
                    {
                        ShowMessage("Lỗi thêm mới: " + mesage, EnumSite.MessageType.Error);
                        return;
                    }
                    else
                    {
                        ShowMessage("Them mới thành công", EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    var xaId = int.Parse(hdfId.Value);
                    var maXa = txtMaXa.Text.Trim();
                    var tenXa = txtTenXa.Text.Trim();
                    var tinhId = int.Parse(ddlHuyen.SelectedValue);

                    mesage = XaService.UpdatetXa(xaId, maXa, tenXa, tinhId);
                    if (mesage.Length > 0)
                    {
                        ShowMessage("Lỗi cập nhật: " + mesage, EnumSite.MessageType.Error);
                        return;
                    }
                    else
                    {
                        ShowMessage("Cập nhật thành công ", EnumSite.MessageType.Success);
                    }
                }

                LoadListXa();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi thao tác: " + ex.Message, EnumSite.MessageType.Success);
            }
        }
    }
}