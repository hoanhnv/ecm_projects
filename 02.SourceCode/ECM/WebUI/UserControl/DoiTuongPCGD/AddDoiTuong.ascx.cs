﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility;
using WebUI.Service;
using WebUI.Models;
using System.Globalization;

namespace WebUI.UserControl.DoiTuongPCGD
{
    public partial class AddDoiTuong : System.Web.UI.UserControl
    {
        ECM_CSDLEntities context = new ECM_CSDLEntities();
        int curentUserId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                long douTuongId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = int.Parse(Session["User_Id"].ToString());
                }

                if (Request.QueryString["Id"] != null)  // eidt
                {
                    try
                    {
                        douTuongId = long.Parse(Request.QueryString["Id"]);
                        btnAdd.Text = "Cập nhật";
                        hdfID.Value = douTuongId.ToString();
                        lblTitle.Text = "Cập nhật thông tin đối tượng";

                        LoadThongTinDoiTuong(douTuongId);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi load thông tin cập nhật: " + ex.Message, EnumSite.MessageType.Error);
                    }
                }
                else // insert
                {
                    btnAdd.Text = "Thêm mới";
                    lblTitle.Text = "Thêm mới đối tượng";
                }
                LoadListHuyen();
                LoadListCoSo();
            }
        }
        public void LoadThongTinDoiTuong(long doiTuongId)
        {
            try
            {
                DOITUONG doiTuong = DoiTuongPCGDService.GetDoiTuongByID(doiTuongId);
                if (doiTuong != null)
                {
                    txtTenDoiTuong.Text = doiTuong.TEN_DOITUONG;
                    txtNgaySinh.Text = doiTuong.NGAY_SINH != null ? DateTime.Parse(doiTuong.NGAY_SINH.ToString()).ToString("dd/MM/yyyy") : "";
                    txtMaDoiTuong.Text = doiTuong.MA_DOITUONG;
                    txtDiaChi.Text = doiTuong.DIACHI_HIENTAI;
                    txtHoTenCha.Text = doiTuong.HOTEN_CHA;
                    txtHoTenMe.Text = doiTuong.HOTEN_ME;
                    ddlCoSoDaoTao.SelectedValue = doiTuong.COSODAOTAO_ID.ToString();

                    // load list huyện
                    LoadListHuyen();
                    if (ddlHuyen.Items.Count > 0)
                    {
                        ddlHuyen.SelectedValue = doiTuong.HUYEN_ID != null ? doiTuong.HUYEN_ID.ToString() : "0";
                    }
                    // load list xa by huyện Id
                    LoadListXaByHuyenID(int.Parse(ddlHuyen.SelectedValue));
                    if (ddXa.Items.Count > 0)
                    {
                        ddXa.SelectedValue = doiTuong.XA_ID != null? doiTuong.XA_ID.ToString(): "0";
                    }
                    // load list thôn by xa id
                    LoadListThonByXaId(int.Parse(ddXa.SelectedValue));
                    if (ddlThon.Items.Count > 0)
                    {
                        ddlThon.SelectedValue = doiTuong.THON_ID != null ? doiTuong.THON_ID.ToString(): "0";
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load thông tin đối tượng: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void ShowMessage(string Message, EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        private void LoadListCoSo()
        {
            try
            {
                List<COSODAOTAO> listCoSo = DM_CoSoService.GetListCoSoDaoTao();
                if (listCoSo != null)
                {
                    ddlCoSoDaoTao.DataSource = listCoSo;
                    ddlCoSoDaoTao.DataTextField = "TENCOSO";
                    ddlCoSoDaoTao.DataValueField = "ID";
                    ddlCoSoDaoTao.DataBind();
                    ddlCoSoDaoTao.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách đối tượng: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadListHuyen()
        {
            try
            {
                List<HUYEN> listHuyen = HuyenService.GetListHuyen();
                if (listHuyen != null)
                {
                    ddlHuyen.DataSource = listHuyen;
                    ddlHuyen.DataTextField = "TENHUYEN";
                    ddlHuyen.DataValueField = "HUYEN_ID";
                    ddlHuyen.DataBind();
                    ddlHuyen.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách huyện: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadListXaByHuyenID(int huyenId)
        {
            try
            {
                List<XA> listXa = XaService.GetListXaByHuyen_ID(huyenId);
                if (listXa != null)
                {
                    ddXa.DataSource = listXa;
                    ddXa.DataTextField = "TENXA";
                    ddXa.DataValueField = "XA_ID";
                    ddXa.DataBind();
                    ddXa.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách xã: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadListThonByXaId(int xaId)
        {
            try
            {
                List<THON_XOM> listHuyen = ThonService.GetListThonXomByXaID(xaId);
                if (listHuyen != null)
                {
                    ddlThon.DataSource = listHuyen;
                    ddlThon.DataTextField = "TEN_THON";
                    ddlThon.DataValueField = "THON_ID";
                    ddlThon.DataBind();
                    ddlThon.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách thôn: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DOITUONG doiTuong = new DOITUONG(); ;
                if (btnAdd.Text == "Thêm mới")
                {
                    try
                    {
                        doiTuong.TEN_DOITUONG = txtTenDoiTuong.Text.Trim();
                        doiTuong.NGAY_SINH = DateTime.ParseExact(txtNgaySinh.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture);
                        doiTuong.MA_DOITUONG = txtMaDoiTuong.Text.Trim();
                        doiTuong.DIACHI_HIENTAI = txtDiaChi.Text.Trim();
                        doiTuong.HOTEN_CHA = txtHoTenCha.Text.Trim();
                        doiTuong.HOTEN_ME = txtHoTenMe.Text.Trim();
                      
                        doiTuong.COSODAOTAO_ID = int.Parse(ddlCoSoDaoTao.SelectedValue.ToString());
                        doiTuong.HUYEN_ID = int.Parse(ddlHuyen.SelectedValue);
                        doiTuong.XA_ID = int.Parse(ddXa.SelectedValue);
                        doiTuong.THON_ID = int.Parse(ddlThon.SelectedValue);
                        var mesage = DoiTuongPCGDService.InsertDoiTuong(doiTuong);
                        if (mesage.Length > 0)
                        {
                            ShowMessage("Lỗi thêm mới:" + mesage, EnumSite.MessageType.Error);
                            return;
                        }

                        ShowMessage("Thêm mới thành công!", EnumSite.MessageType.Success);
                        Response.Redirect("DoiTuongPCGD.aspx");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi thêm mới đối tượng: " + ex.Message, EnumSite.MessageType.Error);
                    }
                }
                else // cập nhật
                {
                    try
                    {
                        var mesage = "";
                        int doiTuong_Id = int.Parse(hdfID.Value);
                        doiTuong.DOITUONG_ID = doiTuong_Id;
                        doiTuong.TEN_DOITUONG = txtTenDoiTuong.Text.Trim();
                        doiTuong.NGAY_SINH = DateTime.ParseExact(txtNgaySinh.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture);
                        doiTuong.MA_DOITUONG = txtMaDoiTuong.Text.Trim();
                        doiTuong.DIACHI_HIENTAI = txtDiaChi.Text.Trim();
                        doiTuong.HOTEN_CHA = txtHoTenCha.Text.Trim();
                        doiTuong.HOTEN_ME = txtHoTenMe.Text.Trim();
                    
                        doiTuong.COSODAOTAO_ID = int.Parse(ddlCoSoDaoTao.SelectedValue.ToString());
                        doiTuong.HUYEN_ID = int.Parse(ddlHuyen.SelectedValue);
                        doiTuong.XA_ID = int.Parse(ddXa.SelectedValue);
                        doiTuong.THON_ID = int.Parse(ddlThon.SelectedValue);

                        mesage = DoiTuongPCGDService.UpdatetDoiTuong(doiTuong);
                        if (mesage.Length > 0)
                        {
                            ShowMessage("Lỗi cập nhật thông tin đối tượng:" + mesage, EnumSite.MessageType.Error);
                            return;
                        }

                        ShowMessage("Cập nhật thành công!", EnumSite.MessageType.Success);
                        Response.Redirect("DoiTuongPCGD.aspx");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi cập nhật thông tin đối tượng: " + ex.Message, EnumSite.MessageType.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void ddlHuyen_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {
                LoadListXaByHuyenID(int.Parse(ddlHuyen.SelectedValue));
            }
            catch (Exception)
            {
            }
        }
        protected void ddXa_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadListThonByXaId(int.Parse(ddXa.SelectedValue));
            }
            catch (Exception)
            {
            }
        }
    }
}