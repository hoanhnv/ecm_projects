﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddDoiTuong.ascx.cs" Inherits="WebUI.UserControl.DoiTuongPCGD.AddDoiTuong" %>

<div class="page-header">
    <h3>
        <asp:Label Text="" ID="lblTitle" runat="server" /></h3>
</div>
<br />
<div id="alert_container" class="messagealert">
</div>
<div class="col-xs-12">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPassword3">
                Tên đối tượng:</label>
            <div class="col-sm-10">
                <asp:TextBox ID="txtTenDoiTuong" ClientIDMode="Static" runat="server" class="validate[required] form-control"
                    placeholder="nhập tên đối tượng" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputEmail3">
                Mã đối tượng:</label>
            <div class="col-sm-10">
                <asp:HiddenField runat="server" ID="hdfID" />
                <asp:TextBox ID="txtMaDoiTuong" ClientIDMode="Static" runat="server" class="validate[required] form-control"
                    placeholder="nhập mã đối tượng" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                Ngày sinh:</label>
            <div class="col-sm-10">
                <asp:TextBox ID="txtNgaySinh" ClientIDMode="Static" runat="server" class="datepicker validate[required] form-control" placeholder="nhập ngày sinh" Width="150px" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                Địa chỉ:</label>
            <div class="col-sm-10">
                <asp:TextBox ID="txtDiaChi" TextMode="MultiLine" Height="50px" runat="server" class="form-control" placeholder="nhập địa chỉ" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPassword3">
                Họ tên cha:</label>
            <div class="col-sm-10">
                <asp:TextBox ID="txtHoTenCha" runat="server" class="validate[required] form-control"
                    placeholder="nhập họ tên cha" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPassword3">
                Họ tên mẹ:</label>
            <div class="col-sm-10">
                <asp:TextBox ID="txtHoTenMe" runat="server" class="validate[required] form-control"
                    placeholder="nhập họ tên mẹ" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPassword3">
                Tuổi:</label>
            <div class="col-sm-10">
                <asp:TextBox ID="txtTuoi" ClientIDMode="Static" runat="server" class="validate[required] form-control"
                    placeholder="nhập tuổi" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                Nghề nghiệp:</label>
            <div class="col-sm-10">
                <asp:TextBox ID="txtNgheNghiep" runat="server" class="form-control" placeholder="nhập nghề nghiệp" />
            </div>
        </div>


        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPassword3">
                Cơ sở đào tạo:</label>
            <div class="col-sm-10">
                <asp:DropDownList ID="ddlCoSoDaoTao" runat="server" class="validate[required] form-control" Width="280px"></asp:DropDownList>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPassword3">
                Huyện:</label>
            <div class="col-sm-10">
                <asp:DropDownList ID="ddlHuyen" ClientIDMode="Static" AutoPostBack="true" runat="server" class="form-control" Width="280px" OnSelectedIndexChanged="ddlHuyen_SelectedIndexChanged1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPassword3">
                Xã:</label>
            <div class="col-sm-10">
                <asp:UpdatePanel runat="server" ID="updatepanel1">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddXa" ClientIDMode="Static" AutoPostBack="true" runat="server" class="form-control" Width="280px" OnSelectedIndexChanged="ddXa_SelectedIndexChanged">
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlHuyen" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPassword3">
                Thôn:</label>
            <div class="col-sm-10">
                <asp:UpdatePanel runat="server" ID="updatepanel2">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlThon" runat="server" class="form-control" Width="280px">
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddXa" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="form-group">
            <asp:Label Text="" ID="lblMessage" ClientIDMode="Static" ForeColor="Red" runat="server" />
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <asp:Button ID="btnAdd" ClientIDMode="Static" runat="server" class="btn btn-primary" OnClick="btnAdd_Click"
                    Text="Thêm mới" />
                &nbsp; <a href="javascript:window.history.back();" class="btn btn-primary">Trở lại</a>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#btnAdd').click(function () {
            $('#lblMessage').text('');
            if (checkIsNumber($('#txtTuoi').val())) {
                $('#lblMessage').text('Tuổi sai định dạng');
                return false;
            }
            if ($('#txtTuoi').val() > 100) {
                $('#lblMessage').text('Tuổi không được vượt quá 100');
                return false;
            }
        });
    });

</script>
