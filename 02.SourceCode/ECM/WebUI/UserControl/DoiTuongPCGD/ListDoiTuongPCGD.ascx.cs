﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.Service;
using WebUI.Models;

namespace WebUI.UserControl.DoiTuongPCGD
{
    public partial class ListDoiTuongPCGD : System.Web.UI.UserControl
    {
        ECM_CSDLEntities context = new ECM_CSDLEntities();
        protected void ShowMessage(string Message, Utility.EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadListDoiTuong();
            }
        }
        private void LoadListDoiTuong(string tenDoiTuong = "")
        {
            try
            {
                long curentUserId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = long.Parse(Session["User_Id"].ToString());
                }

                if (tenDoiTuong.Length > 0)
                {
                    List<DOITUONG> listDoiTuong = DoiTuongPCGDService.GetListDoiTuong().Where(o => o.TEN_DOITUONG.Contains(tenDoiTuong)).OrderBy(o => o.TEN_DOITUONG).ToList();
                    if (listDoiTuong != null)
                    {
                        grvDoiTuong.Visible = true;
                        grvDoiTuong.DataSource = listDoiTuong;
                        grvDoiTuong.DataBind();
                    }
                    else
                    {
                        grvDoiTuong.Visible = false;
                        ShowMessage("Không có đối tượng nào", Utility.EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    List<DOITUONG> listDoiTuong = DoiTuongPCGDService.GetListDoiTuong().OrderBy(o => o.TEN_DOITUONG).ToList();
                    if (listDoiTuong != null)
                    {
                        grvDoiTuong.Visible = true;
                        grvDoiTuong.DataSource = listDoiTuong;
                        grvDoiTuong.DataBind();
                    }
                    else
                    {
                        grvDoiTuong.Visible = false;
                        ShowMessage("Không có đối tượng nào", Utility.EnumSite.MessageType.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load đối tượng: " + ex.Message, Utility.EnumSite.MessageType.Error);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim().Length == 0)
            {
                ShowMessage("Bạn chưa nhập tên đối tượngcần tìm", Utility.EnumSite.MessageType.Success);
                return;
            }
            else
            {
                LoadListDoiTuong(txtSearch.Text.Trim());
            }
        }
        protected void grvDoiTuong_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string comanName = e.CommandName;
                var doiTuongId = long.Parse(e.CommandArgument.ToString());
                switch (comanName)
                {
                    case "sua":
                        Response.Redirect("ThemMoiDoiTuong.aspx?Id=" + doiTuongId, false);
                        break;
                    case "xoa":
                        try
                        {
                            var message = DoiTuongPCGDService.DeleteDoiTuong(doiTuongId);
                            if (message.Length > 0)
                            {
                                LoadListDoiTuong();
                                ShowMessage("Xóa đối tượng thành công!", Utility.EnumSite.MessageType.Success);
                            }
                            else
                            {
                                ShowMessage("Lỗi xóa đối tượng:" + message, Utility.EnumSite.MessageType.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi xóa:" + ex.Message, Utility.EnumSite.MessageType.Error);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi xảy ra:" + ex.Message, Utility.EnumSite.MessageType.Error);
            }
        }
        protected void grvDoiTuong_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grvDoiTuong.PageIndex = e.NewPageIndex;
                grvDoiTuong.DataBind();
                LoadListDoiTuong();
            }
            catch (Exception)
            {
            }
        }
    }
}