﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace WebUI.UserControl.Chart
{
    public partial class PieChart : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        Dictionary<DateTime, int> testData = new Dictionary<DateTime, int>();

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!IsPostBack)
            {
                // bind chart type names to ddl
                ddlChartType.DataSource = Enum.GetNames(typeof(SeriesChartType));
                ddlChartType.DataBind();
            }

            DataBind();
        }

        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);

            // define test data
            Random rnd = new Random(Guid.NewGuid().GetHashCode());

            for (int i = 0; i < Convert.ToInt32(rblValueCount.SelectedValue); i++)
            {
                testData.Add(DateTime.Now.AddDays(i), rnd.Next(1, 50));
            }

            pieChart.Series["PieChart"].Points.DataBind(testData, "Key", "Value", string.Empty);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // update chart rendering
            pieChart.Series["PieChart"].ChartTypeName = ddlChartType.SelectedValue;
            pieChart.ChartAreas[0].Area3DStyle.Enable3D = cbUse3D.Checked;
            pieChart.ChartAreas[0].Area3DStyle.Inclination = Convert.ToInt32(rblInclinationAngle.SelectedValue);
        }
    }
}