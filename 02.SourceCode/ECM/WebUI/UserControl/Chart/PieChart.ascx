﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PieChart.ascx.cs" Inherits="WebUI.UserControl.Chart.PieChart" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<style type="text/css">
    h2 {
        margin-left: 5px;
    }

    .clearfix:after {
        content: ".";
        display: block;
        clear: both;
        visibility: hidden;
        line-height: 0;
        height: 0;
    }

    .clearfix {
        display: inline-block;
    }

    .box {
        float: left;
        width: 300px;
        margin: 10px;
        padding: 10px;
        border: 1px solid #ccc;
    }
</style>
<div class="clearfix">

    <h2>Cấu hình biểu đồ hiển thị</h2>

    <div style="float: left; width: 340px;">
        <div class="box">
            <h3>Loại biểu đồ</h3>
            <p>
                <asp:DropDownList ID="ddlChartType" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </p>
        </div>

        <div class="box">
            <h3>Phân vùng dữ liệu hiển thị</h3>
            <p>
                <asp:RadioButtonList ID="rblValueCount" runat="server" AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="10">Him Lam</asp:ListItem>
                    <asp:ListItem Value="20">Mường Nhé</asp:ListItem>
                    <asp:ListItem Value="50">Mường Lay</asp:ListItem>
                    <asp:ListItem Value="100">Tủa Chùa</asp:ListItem>
                    <asp:ListItem Value="500">Điện Biên</asp:ListItem>
                </asp:RadioButtonList>
            </p>
        </div>
    </div>

    <div class="box">
        <h3>Thiết lập hiển thị 3D</h3>
        <p>
            <asp:CheckBox ID="cbUse3D" runat="server" AutoPostBack="True" Text="Use 3D Chart" />
        </p>
        <h4>Góc nghiêng</h4>
        <p>
            <asp:RadioButtonList ID="rblInclinationAngle" runat="server" AutoPostBack="True">
                <asp:ListItem Value="-90">-90°</asp:ListItem>
                <asp:ListItem Value="-50">-50°</asp:ListItem>
                <asp:ListItem Value="-20">-20°</asp:ListItem>
                <asp:ListItem Value="0">0°</asp:ListItem>
                <asp:ListItem Selected="True" Value="20">20°</asp:ListItem>
                <asp:ListItem Value="50">50°</asp:ListItem>
                <asp:ListItem Value="90">90°</asp:ListItem>
            </asp:RadioButtonList>
        </p>
    </div>


</div>

<asp:Chart ID="pieChart" runat="server" Height="400px" Width="800px">
    <Series>
        <asp:Series Name="PieChart">
        </asp:Series>
    </Series>
    <ChartAreas>
        <asp:ChartArea Name="ChartArea1">
            <Area3DStyle />
        </asp:ChartArea>
    </ChartAreas>
</asp:Chart>
