﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddUser.ascx.cs" Inherits="WebUI.UserControl.Users.AddUser" %>

<div class="page-header">
    <h3>
        <asp:Label Text="" ID="lblTitle" runat="server" /></h3>
</div>
<br />
<div id="alert_container" class="messagealert">
</div>
<div class="col-md-12">
    <asp:UpdatePanel runat="server" ID="updatepaneladd">
        <ContentTemplate>
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="inputEmail3">
                        Tên đăng nhập:</label>
                    <div class="col-sm-10">
                        <asp:HiddenField runat="server" ID="hdfUserID" />
                        <asp:TextBox ID="txtUser" ClientIDMode="Static" runat="server" class="validate[required] form-control"
                            placeholder="nhập tên đăng nhập" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="inputPassword3">
                        Mật khẩu:</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtPassword" ClientIDMode="Static" runat="server" class="validate[required] form-control"
                            TextMode="Password" placeholder="nhập mật khẩu" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Họ tên:</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtFullName" ClientIDMode="Static" runat="server" class="form-control" placeholder="nhập họ tên đầy đủ" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Số điện thoại:</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtPhone" ClientIDMode="Static" runat="server" class="form-control" placeholder="nhập số điện thoại" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Email:</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="nhập địa chỉ email" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Địa chỉ:</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtAddress" runat="server" class="form-control" placeholder="nhập địa chỉ"
                            TextMode="MultiLine" Height="70px" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Ngày sinh:</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtBirthDay" ClientIDMode="Static" runat="server" class="datepicker form-control" placeholder="nhập ngày sinh" Width="150px" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="inputPassword3">
                        Huyện/Xã/Thôn:</label>
                    <div class="col-sm-10">
                        <asp:DropDownList ID="ddlHuyen" ClientIDMode="Static" AutoPostBack="true" runat="server" class="form-control" CssClass="validate[required]" OnSelectedIndexChanged="ddlHuyen_SelectedIndexChanged">
                        </asp:DropDownList>
                        &nbsp;/ 
                <asp:DropDownList ID="ddlXa" ClientIDMode="Static" AutoPostBack="true" runat="server" class="form-control" CssClass="validate[required]" OnSelectedIndexChanged="ddlXa_SelectedIndexChanged">
                </asp:DropDownList>
                        &nbsp;/ 
                <asp:DropDownList ID="ddlThon" ClientIDMode="Static" runat="server" class="form-control" CssClass="validate[required]">
                </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="inputPassword3">
                        Cơ sở:</label>
                    <div class="col-sm-10">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlCoSo" runat="server" class="form-control" CssClass="validate[required]" Width="280px">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlHuyen" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="ddlXa" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="ddlThon" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="inputPassword3">
                        Trạng thái:</label>
                    <div class="col-sm-10">
                        <asp:DropDownList ID="ddlStatus" runat="server" class="form-control" Width="280px">
                            <asp:ListItem Text="Kích hoạt" Value="1" Selected />
                            <asp:ListItem Text="Không kích hoạt" Value="2" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label Text="" ID="lblMessage" ClientIDMode="Static" ForeColor="Red" runat="server" />
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:Button ID="btnAdd" ClientIDMode="Static" runat="server" class="btn btn-primary" OnClick="btnAdd_Click"
                            Text="Thêm mới" />
                        &nbsp; <a href="javascript:window.history.back();" class="btn btn-primary">Trở lại</a>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<script>
    $(document).ready(function () {
        $('#btnAdd').click(function () {
            if ($('#btnAdd').val() == 'Thêm mới') {
                if ($('#txtUser').val() == '') {
                    $('#lblMessage').text('Tên đăng nhập không được để trống');
                    return false;
                }
                if ($('#txtPassword').val() == '') {
                    $('#lblMessage').text('Mật khẩu không được để trống');
                    return false;
                }
            }

            if ($('#txtFullName').val() == '') {
                $('#lblMessage').text('Họ tên không được để trống');
                return false;
            }
            if ($('#txtPhone').val() == '') {
                $('#lblMessage').text('Số điện thoại không được để trống');
                return false;
            }
            if ($('#txtBirthDay').val() == '') {
                $('#lblMessage').text('Ngày sinh không được để trống');
                return false;
            }
        });
    });
</script>
