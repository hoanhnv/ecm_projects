﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility;
using WebUI.Service;
using WebUI.Models;

namespace WebUI.UserControl.Users
{
    public partial class AssignUserToRole : System.Web.UI.UserControl
    {
        ECM_CSDLEntities context = new ECM_CSDLEntities();
        protected void ShowMessage(string Message, Utility.EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadListUser();
            }
        }
        private void LoadListUser(string userName = "")
        {
            try
            {
                long curentUserId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = long.Parse(Session["User_Id"].ToString());
                }

                if (userName.Length > 0)
                {
                    var listUser = (from u in context.DM_NGUOIDUNG
                                    join cs in context.COSODAOTAO on u.COSO_ID equals cs.ID
                                    where u.TENDANGNHAP.Contains(userName)
                                    select new
                                    {
                                        u.DM_NGUOIDUNG_ID,
                                        u.TENDANGNHAP,
                                        u.MATKHAU,
                                        u.TRANGTHAI,
                                        u.DIENTHOAI,
                                        u.HOTEN,
                                        u.EMAIL,
                                        u.DIACHI,
                                        u.NGAYSINH,
                                        cs.TENCOSO
                                    }).ToList();
                    if (listUser.Count > 0)
                    {
                        grvUsers.Visible = true;
                        grvUsers.DataSource = listUser;
                        grvUsers.DataBind();
                    }
                    else
                    {
                        grvUsers.Visible = false;
                        ShowMessage("Không có người dùng nào", Utility.EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    var listUser = (from u in context.DM_NGUOIDUNG
                                    join cs in context.COSODAOTAO on u.COSO_ID equals cs.ID
                                    select new
                                    {
                                        u.DM_NGUOIDUNG_ID,
                                        u.TENDANGNHAP,
                                        u.MATKHAU,
                                        u.TRANGTHAI,
                                        u.DIENTHOAI,
                                        u.HOTEN,
                                        u.EMAIL,
                                        u.DIACHI,
                                        u.NGAYSINH,
                                        cs.TENCOSO
                                    }).ToList();
                    if (listUser.Count > 0)
                    {
                        grvUsers.Visible = true;
                        grvUsers.DataSource = listUser;
                        grvUsers.DataBind();
                    }
                    else
                    {
                        grvUsers.Visible = false;
                        ShowMessage("Không có người dùng nào", Utility.EnumSite.MessageType.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load danh sách người dùng: " + ex.Message, Utility.EnumSite.MessageType.Error);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim().Length == 0)
            {
                ShowMessage("Bạn chưa nhập tài khoản cần tìm", Utility.EnumSite.MessageType.Success);
                return;
            }
            else
            {
                LoadListUser(txtSearch.Text.Trim());
            }
        }
        protected void grvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string comanName = e.CommandName;
                var userId = long.Parse(e.CommandArgument.ToString());
                if (comanName == "sua")
                {
                    hdfUser_Id.Value = userId.ToString();
                    LoadListRole();
                    List<USER_ROLE> listUserRole = User_RoleService.GetListUserRoleByUserId(userId);
                    if (listUserRole.Count > 0)
                    {
                        CheckBox cb;
                        HiddenField hdfRoleId;
                        foreach (GridViewRow row in grvRole.Rows)
                        {
                            cb = (CheckBox)row.FindControl("cbCheck");
                            hdfRoleId = (HiddenField)row.FindControl("hdfID");
                            foreach (USER_ROLE roleFunction in listUserRole)
                            {
                                if (roleFunction.ROLE_ID == int.Parse(hdfRoleId.Value))
                                {
                                    cb.Checked = true;
                                }
                            }
                        }
                    }

                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "ShowPopup('popupadd');", true);
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi xảy ra:" + ex.Message, Utility.EnumSite.MessageType.Error);
            }
        }
        protected void grvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grvUsers.PageIndex = e.NewPageIndex;
                grvUsers.DataBind();
                LoadListUser();
            }
            catch (Exception)
            {
            }
        }






        private void LoadListRole()
        {
            try
            {
                List<ROLES> listRole = RoleService.GetListRole();
                if (listRole.Count > 0)
                {
                    grvRole.DataSource = listRole;
                    grvRole.DataBind();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load danh sách vai trò: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "";
                int user_Id = int.Parse(hdfUser_Id.Value);
                message = User_RoleService.DeleteUserRoleByUserId(user_Id);
                if (message != "")
                {
                    ShowMessage("Lỗi trong quá trình lưu", EnumSite.MessageType.Error); return;
                }

                List<USER_ROLE> listUserRole = new List<USER_ROLE>();
                USER_ROLE userRole;
                CheckBox cb;
                HiddenField hdfRoleId;

                foreach (GridViewRow row in grvRole.Rows)
                {
                    cb = (CheckBox)row.FindControl("cbCheck");
                    if (cb.Checked)
                    {
                        hdfRoleId = (HiddenField)row.FindControl("hdfID");
                        userRole = new USER_ROLE();
                        userRole.USER_ID = user_Id;
                        userRole.ROLE_ID = int.Parse(hdfRoleId.Value);
                        listUserRole.Add(userRole);
                    }
                }

                if (listUserRole.Count > 0)
                {
                    message = User_RoleService.InsertListUserRole(listUserRole);
                    if (message == "")
                    {
                        ShowMessage("Phân quyền vai trò cho người dùng thành công.", EnumSite.MessageType.Success); return;
                    }
                    else
                    {
                        ShowMessage("Lỗi ghi lại", EnumSite.MessageType.Error); return;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi :" + ex.Message, EnumSite.MessageType.Error); return;
            }
        }
    }
}