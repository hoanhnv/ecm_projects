﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility;
using WebUI.Service;
using WebUI.Models;
using WebUI.Models;

namespace WebUI.UserControl.RoleAndPermision
{
    public partial class AssignUserToRole : System.Web.UI.UserControl
    {
        protected void ShowMessage(string Message, EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadListRole();
            }
        }
        private void LoadListRole()
        {
            try
            {
                List<ROLES> listRole = RoleService.GetListRole();
                if (listRole.Count > 0)
                {
                    ddlRole.DataSource = listRole;
                    ddlRole.DataTextField = "ROLENAME";
                    ddlRole.DataValueField = "ROLE_ID";
                    ddlRole.DataBind();
                    ddlRole.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load danh sách vai trò: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadListFunction()
        {
            try
            {
                List<DM_CHUCNANG> listChucNang = DM_ChucNangService.GetListDanhMucChucNang();
                if (listChucNang.Count > 0)
                {
                    grvFunction.DataSource = listChucNang;
                    grvFunction.DataBind();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load danh sách chức năng: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadListFunction();
                int roleId = int.Parse(ddlRole.SelectedValue);
                List<ROLE_FUNCTION> listRoleFunction = RoleFunctionService.GetListFunctionByRoleId(roleId);
                if (listRoleFunction.Count > 0)
                {
                    CheckBox cb;
                    HiddenField hdfFunctionId;
                    foreach (GridViewRow row in grvFunction.Rows)
                    {
                        cb = (CheckBox)row.FindControl("cbCheck");
                        hdfFunctionId = (HiddenField)row.FindControl("hdfID");
                        foreach (ROLE_FUNCTION roleFunction in listRoleFunction)
                        {
                            if (roleFunction.CHUCNANG_ID == int.Parse(hdfFunctionId.Value))
                            {
                                cb.Checked = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlRole.SelectedValue == "0")
                {
                    ShowMessage("Bạn chưa chọn vai trò", EnumSite.MessageType.Error); return;
                }
                string message = "";
                int roleId = int.Parse(ddlRole.SelectedValue);
                message = RoleFunctionService.DeleteRoleFunctionByRoleId(roleId);
                if (message != "")
                {
                    ShowMessage("Lỗi trong quá trình lưu", EnumSite.MessageType.Error); return;
                }

                List<ROLE_FUNCTION> listRoleFunction = new List<ROLE_FUNCTION>();
                ROLE_FUNCTION roleFunction;
                CheckBox cb;
                HiddenField hdfFunctionId;

                foreach (GridViewRow row in grvFunction.Rows)
                {
                    cb = (CheckBox)row.FindControl("cbCheck");
                    if (cb.Checked)
                    {
                        hdfFunctionId = (HiddenField)row.FindControl("hdfID");
                        roleFunction = new ROLE_FUNCTION();
                        roleFunction.ROLE_ID = int.Parse(ddlRole.SelectedValue);
                        roleFunction.CHUCNANG_ID = int.Parse(hdfFunctionId.Value);
                        listRoleFunction.Add(roleFunction);
                    }
                }

                if (listRoleFunction.Count > 0)
                {
                    message = RoleFunctionService.InsertListRoleFunction(listRoleFunction);
                    if (message == "")
                    {
                        ShowMessage("Phân quyền chức năng cho vai trò thành công.", EnumSite.MessageType.Success); return;
                    }
                    else
                    {
                        ShowMessage("Lỗi ghi lại", EnumSite.MessageType.Error); return;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi :" + ex.Message, EnumSite.MessageType.Error); return;
            }
        }
    }
}