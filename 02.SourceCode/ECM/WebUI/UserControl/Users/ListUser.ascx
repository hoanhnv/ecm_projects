﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListUser.ascx.cs" Inherits="WebUI.UserControl.Users.ListUser" %>
<div class="page-header">
    <h3>Danh sách người dùng</h3>
</div>
<div class="messagealert" id="alert_container">
</div>
<div class="panel-body">
    <div class="form-group">
        <div class="col-sm-10">
            <div style="float: left; width: 280px">
                <asp:TextBox ID="txtSearch" Width="250px" runat="server" class="form-control" placeholder="Từ khóa" />
            </div>
            <div style="float: left; width: 30%">
                <asp:Button ID="btnSearch" Text="Tìm" CssClass="btn btn-info" runat="server" OnClick="btnSearch_Click" />
            </div>
        </div>
    </div>
</div>
<div style="float: right; margin: 0px 0px 5px 0px">
    <asp:Button Text="Thêm mới" runat="server" ID="btnAddNew" CssClass="btn btn-info" PostBackUrl="~/ThemMoiTaiKhoan.aspx" />
</div>
<asp:GridView runat="server" ID="grvUsers" ClientIDMode="Static" Width="100%" CssClass="wapperpager table table-bordered table-hover dataTable"
    AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="grvUsers_PageIndexChanging"
    OnRowCommand="grvUsers_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="Thứ tự">
            <ItemTemplate>
                <%#(Convert.ToInt32(DataBinder.Eval(Container, "RowIndex")) + 1)%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="7%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tên đăng nhập">
            <ItemTemplate>
                <%# Eval("TENDANGNHAP")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="20%" HorizontalAlign="Center" ForeColor="#23527c" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Họ tên">
            <ItemTemplate>
                <%# Eval("HOTEN")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="20%" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Số điện thoại">
            <ItemTemplate>
                <%# Eval("DIENTHOAI")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="15%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tên cơ sở">
            <ItemTemplate>
                <%# Eval("TENCOSO")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="10%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Kích hoạt">
            <ItemTemplate>
                <%--<asp:CheckBox Text="" runat="server" Checked='<%# Eval("Status") %>' Enabled="false" />--%>
                <%# Eval("TRANGTHAI") %>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="10%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="">
            <ItemTemplate>
                <asp:LinkButton ID="btnEdit" Text="Sửa" ToolTip="Sửa" CommandArgument='<%# Eval("DM_NGUOIDUNG_ID") %>'
                    CommandName="sua" runat="server"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton>
                &nbsp;
                                        <asp:LinkButton ID="btnDelete" Text="Xóa" ToolTip="Xóa" OnClientClick="return confirm('Bạn có chắc muốn xóa?')"
                                            CommandName="xoa" CommandArgument='<%# Eval("DM_NGUOIDUNG_ID") %>' runat="server"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="10%" HorizontalAlign="Center" />
        </asp:TemplateField>
    </Columns>
    <PagerSettings FirstPageText="Đầu" LastPageText="Cuối" Mode="NumericFirstLast" NextPageText="Tiếp"
        PreviousPageText="Trước" />
    <PagerStyle BackColor="White" BorderColor="#3366CC" BorderStyle="Outset" BorderWidth="1px"
        CssClass="paging" ForeColor="Blue" Height="40px" HorizontalAlign="Right" Width="200px" />
</asp:GridView>

