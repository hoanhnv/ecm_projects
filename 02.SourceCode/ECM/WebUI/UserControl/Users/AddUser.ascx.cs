﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.Models;
using WebUI.Service;
using Utility;
using Business.CommonHelper;
using WebUI.Models;
using System.Globalization;

namespace WebUI.UserControl.Users
{
    public partial class AddUser : System.Web.UI.UserControl
    {
        ECM_CSDLEntities context = new ECM_CSDLEntities();
        long curentUserId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                long userId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = long.Parse(Session["User_Id"].ToString());
                }

                if (Request.QueryString["UserId"] != null)  // eidt
                {
                    try
                    {
                        userId = long.Parse(Request.QueryString["UserId"]);
                        btnAdd.Text = "Cập nhật";
                        hdfUserID.Value = userId.ToString();
                        txtUser.Enabled = false;
                        txtPassword.Enabled = false;
                        lblTitle.Text = "Cập nhật thông tin người dùng";

                        LoadUserInfo(userId);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi load thông tin cập nhật: " + ex.Message, EnumSite.MessageType.Error);
                    }
                }
                else // insert
                {
                    btnAdd.Text = "Thêm mới";
                    lblTitle.Text = "Thêm người dùng";
                }
                LoadListHuyen();
                LoadListCoSo();

            }
        }
        public void LoadUserInfo(long userId)
        {
            try
            {
                DM_NGUOIDUNG nguoidung = UserService.GetUserByUserID(userId);
                VITRI_NGUOIDUNG viTriNguoiDung = VITRI_NGUOIDUNGService.GetViTriNguoiDungByUser_Id(userId);
                if (nguoidung != null)
                {
                    txtUser.Text = nguoidung.TENDANGNHAP;
                    txtPassword.Text = nguoidung.MATKHAU;
                    txtFullName.Text = nguoidung.HOTEN;
                    txtPhone.Text = nguoidung.DIENTHOAI;
                    txtEmail.Text = nguoidung.EMAIL;
                    txtAddress.Text = nguoidung.DIACHI;
                    txtBirthDay.Text = nguoidung.NGAYSINH != null ? DateTime.Parse(nguoidung.NGAYSINH.ToString()).ToString("dd/MM/yyyy") : "";

                    if (viTriNguoiDung != null)
                    {
                        // load list huyện
                        LoadListHuyen();
                        if (ddlHuyen.Items.Count > 0)
                        {
                            ddlHuyen.SelectedValue = viTriNguoiDung.HUYEN_ID != null ? viTriNguoiDung.HUYEN_ID.ToString() : "0";
                        }

                        // load list xa by huyện Id
                        LoadListXaByHuyenID(int.Parse(ddlHuyen.SelectedValue));
                        if (ddlXa.Items.Count > 0)
                        {
                            ddlXa.SelectedValue = viTriNguoiDung.XA_ID != null ? viTriNguoiDung.XA_ID.ToString() : "0";
                        }

                        // load list thôn by xa id
                        LoadListThonByXaId(int.Parse(ddlXa.SelectedValue));
                        if (ddlThon.Items.Count > 0)
                        {
                            ddlThon.SelectedValue = viTriNguoiDung.THON_ID != null ? viTriNguoiDung.THON_ID.ToString() : "0";
                        }
                    }

                    //load list cơ sở
                    LoadListCoSo();
                    if (ddlCoSo.Items.Count > 0)
                    {
                        ddlCoSo.SelectedValue = nguoidung.COSO_ID.ToString();
                    }
                    ddlStatus.SelectedValue = nguoidung.TRANGTHAI.ToString();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load thông tin người dùng: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void ShowMessage(string Message, EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }

        private void LoadListCoSo()
        {
            try
            {
                List<COSODAOTAO> listCoSoDaoTao = new List<COSODAOTAO>();
                //int huyenId = int.Parse(ddlHuyen.Items.Count > 0 ? ddlHuyen.SelectedValue : "0");
                //int xaId = int.Parse(ddlXa.Items.Count > 0 ? ddlXa.SelectedValue : "0");
                //int thonId = int.Parse(ddlThon.Items.Count > 0 ? ddlThon.SelectedValue : "0");
                //listCoSoDaoTao = DM_CoSoService.GetListCoSoDaoTaoByThonXaHuyen(200, huyenId, xaId, thonId);

                listCoSoDaoTao = DM_CoSoService.GetListCoSoDaoTao();
                if (listCoSoDaoTao != null)
                {
                    ddlCoSo.DataSource = listCoSoDaoTao;
                    ddlCoSo.DataTextField = "TENCOSO";
                    ddlCoSo.DataValueField = "ID";
                    ddlCoSo.DataBind();
                    ddlCoSo.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách cơ sở: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadListHuyen()
        {
            try
            {
                List<HUYEN> listHuyen = HuyenService.GetListHuyen();
                if (listHuyen != null)
                {
                    ddlHuyen.DataSource = listHuyen;
                    ddlHuyen.DataTextField = "TENHUYEN";
                    ddlHuyen.DataValueField = "HUYEN_ID";
                    ddlHuyen.DataBind();
                    ddlHuyen.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách huyện: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadListXaByHuyenID(int huyenId)
        {
            try
            {
                List<XA> listXa = XaService.GetListXaByHuyen_ID(huyenId);
                if (listXa != null)
                {
                    ddlXa.DataSource = listXa;
                    ddlXa.DataTextField = "TENXA";
                    ddlXa.DataValueField = "XA_ID";
                    ddlXa.DataBind();
                    ddlXa.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách xã: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        private void LoadListThonByXaId(int xaId)
        {
            try
            {
                List<THON_XOM> listHuyen = ThonService.GetListThonXomByXaID(xaId);
                if (listHuyen != null)
                {
                    ddlThon.DataSource = listHuyen;
                    ddlThon.DataTextField = "TEN_THON";
                    ddlThon.DataValueField = "THON_ID";
                    ddlThon.DataBind();
                    ddlThon.Items.Insert(0, new ListItem("<----------------Chọn---------------->", "0"));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi tải danh sách thôn: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DM_NGUOIDUNG nguoidung;
                if (btnAdd.Text == "Thêm mới")
                {
                    try
                    {
                        if (Utility.Utility.CheckContainsSpecialChars(txtUser.Text.Trim()))
                        {
                            ShowMessage("Tên đăng nhập không được chứa các ký tự đặc biệt!", EnumSite.MessageType.Error);
                            return;
                        }

                        nguoidung = UserService.GetUserByUserName(txtUser.Text.Trim());
                        if (nguoidung != null)
                        {
                            ShowMessage("Tên đăng nhập này đã tồn tại!", EnumSite.MessageType.Error);
                            return;
                        }

                        MaHoaMatKhau mahoa = new MaHoaMatKhau();
                        nguoidung = new DM_NGUOIDUNG();
                        nguoidung.TENDANGNHAP = txtUser.Text.Trim();
                        nguoidung.MATKHAU = mahoa.Encode_Data(txtPassword.Text.Trim());
                        nguoidung.TRANGTHAI = int.Parse(ddlStatus.SelectedValue);
                        nguoidung.NGAYTAO = DateTime.Now;
                        nguoidung.NGAYSUA = null;
                        nguoidung.NGUOITAO = Session["UserName"].ToString();
                        nguoidung.DIENTHOAI = txtPhone.Text.Trim();
                        nguoidung.COSO_ID = int.Parse(ddlCoSo.SelectedValue);
                        nguoidung.NGUOISUA = "";
                        nguoidung.HOTEN = txtFullName.Text.Trim();
                        nguoidung.EMAIL = txtEmail.Text.Trim();
                        nguoidung.DIACHI = txtAddress.Text.Trim();
                        nguoidung.NGAYSINH = DateTime.ParseExact(txtBirthDay.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture);
                        var mesage = UserService.InsertUser(nguoidung);
                        if (mesage.Length > 0)
                        {
                            ShowMessage("Lỗi thêm mới:" + mesage, EnumSite.MessageType.Error);
                            return;
                        }

                        // thêm vào bảng ViTriNguoiDung
                        var newUserId = UserService.GetLastUserId();
                        VITRI_NGUOIDUNG vitriNguoiDung = new VITRI_NGUOIDUNG();
                        vitriNguoiDung.NGUOIDUNG_ID = newUserId;
                        vitriNguoiDung.TINH_ID = 1; // fix tỉnh
                        vitriNguoiDung.HUYEN_ID = int.Parse(ddlHuyen.SelectedValue);
                        vitriNguoiDung.XA_ID = int.Parse(ddlXa.SelectedValue);
                        vitriNguoiDung.THON_ID = int.Parse(ddlThon.SelectedValue);
                        mesage = VITRI_NGUOIDUNGService.InsertUpdateViTriNguoiDung(vitriNguoiDung);
                        if (mesage.Length > 0)
                        {
                            ShowMessage("Lỗi thêm ví trí người dùng: " + mesage, EnumSite.MessageType.Error);
                            return;
                        }

                        ShowMessage("Thêm mới thành công!", EnumSite.MessageType.Success);
                        Response.Redirect("TaiKhoan.aspx");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi thêm mới người dùng: " + ex.Message, EnumSite.MessageType.Error);
                    }
                }
                else // cập nhật
                {
                    try
                    {
                        int userId = int.Parse(hdfUserID.Value);
                        var mesage = "";

                        nguoidung = new DM_NGUOIDUNG();
                        nguoidung.DM_NGUOIDUNG_ID = userId;
                        nguoidung.TRANGTHAI = int.Parse(ddlStatus.SelectedValue);
                        nguoidung.NGAYSUA = DateTime.Now;
                        nguoidung.DIENTHOAI = txtPhone.Text.Trim();
                        nguoidung.COSO_ID = int.Parse(ddlCoSo.SelectedValue);
                        nguoidung.NGUOISUA = Session["UserName"].ToString();
                        nguoidung.HOTEN = txtFullName.Text.Trim();
                        nguoidung.EMAIL = txtEmail.Text.Trim();
                        nguoidung.DIACHI = txtAddress.Text.Trim();
                        nguoidung.NGAYSINH = DateTime.ParseExact(txtBirthDay.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture);
                        mesage = UserService.UpdatetUser(nguoidung);
                        if (mesage.Length > 0)
                        {
                            ShowMessage("Lỗi cập nhật thông tin người dùng:" + mesage, EnumSite.MessageType.Success);
                            return;
                        }
                        // thêm vào bảng ViTriNguoiDung
                        VITRI_NGUOIDUNG vitriNguoiDung = new VITRI_NGUOIDUNG();
                        vitriNguoiDung.NGUOIDUNG_ID = userId;
                        vitriNguoiDung.TINH_ID = 1; // fix tỉnh
                        vitriNguoiDung.HUYEN_ID = int.Parse(ddlHuyen.SelectedValue);
                        vitriNguoiDung.XA_ID = int.Parse(ddlXa.SelectedValue);
                        vitriNguoiDung.THON_ID = int.Parse(ddlThon.SelectedValue);
                        mesage = VITRI_NGUOIDUNGService.InsertUpdateViTriNguoiDung(vitriNguoiDung);
                        if (mesage.Length > 0)
                        {
                            ShowMessage("Lỗi cập nhật ví trí người dùng: " + mesage, EnumSite.MessageType.Error);
                            return;
                        }

                        ShowMessage("Cập nhật thành công!", EnumSite.MessageType.Success);
                        Response.Redirect("TaiKhoan.aspx");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi cập nhật thông tin người dùng: " + ex.Message, EnumSite.MessageType.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, EnumSite.MessageType.Error);
            }
        }
        protected void ddlHuyen_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadListXaByHuyenID(int.Parse(ddlHuyen.SelectedValue));
            }
            catch (Exception)
            {
            }
        }
        protected void ddlXa_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadListThonByXaId(int.Parse(ddlXa.SelectedValue));
            }
            catch (Exception)
            {
            }
        }
    }
}