﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssignRoleToFunction.ascx.cs" Inherits="WebUI.UserControl.RoleAndPermision.AssignUserToRole" %>
<div class="page-header">
    <h3>Phân quyền vai trò chức năng</h3>
</div>
<br />
<div class="messagealert" id="alert_container">
</div>
<div class="panel-body">
    <div class="form-group">
        <div class="col-sm-offset-0 col-sm-12">
            <table style="width: 50%">
                <tr>
                    <td>Vai Trò:</td>
                    <td>
                        <asp:DropDownList ID="ddlRole" Width="250px" ClientIDMode="Static" AutoPostBack="true" runat="server" class="form-control" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged">
                        </asp:DropDownList></td>
                    <td>
                        <asp:Button Text="Lưu" ID="btnSave" runat="server" OnClick="btnSave_Click" /></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<asp:GridView runat="server" ID="grvFunction" ClientIDMode="Static" Width="100%" CssClass="table table-striped table-bordered table-hover dataTable no-footer DTTT_selectable"
    AutoGenerateColumns="False">
    <Columns>
        <asp:TemplateField HeaderText="Thứ tự">
            <ItemTemplate>
                <%#(Convert.ToInt32(DataBinder.Eval(Container, "RowIndex")) + 1)%>
                <asp:HiddenField runat="server" ID="hdfID" Value='<%# Eval("DM_CHUCNANG_ID") %>' ClientIDMode="Static" />
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="7%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tên chức năng">
            <ItemTemplate>
                <%# Eval("TEN_CHUCNANG")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="20%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Link">
            <ItemTemplate>
                <%# Eval("URL")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="43%" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Chọn">
            <HeaderTemplate>
                <asp:CheckBox Text="Chọn hết" ID="cbAll" runat="server" />
            </HeaderTemplate>
            <ItemTemplate>
                <asp:CheckBox Text="" ID="cbCheck" runat="server" />
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="15%" HorizontalAlign="Center" />
        </asp:TemplateField>
    </Columns>
</asp:GridView>


<script type="text/javascript">
    $(document).ready(function () {
        $('#cbAll').click(function () {
            if ($('#cbAll').is(':checked')) {
                $("#grvFunction").find("[type='checkbox']").prop('checked', true);
            } else {
                $("#grvFunction").find("[type='checkbox']").prop('checked', false);
            }
        });
    });
</script>
