﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.Service;
using Utility;
using WebUI.Models;
namespace WebUI.UserControl.Users
{
    public partial class ListUser : System.Web.UI.UserControl
    {
        ECM_CSDLEntities context = new ECM_CSDLEntities();
        protected void ShowMessage(string Message, Utility.EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadListUser();
            }
        }
        private void LoadListUser(string userName = "")
        {
            try
            {
                long curentUserId = 0;
                if (Session["User_Id"] != null)
                {
                    curentUserId = long.Parse(Session["User_Id"].ToString());
                }

                if (userName.Length > 0)
                {
                    var listUser = (from u in context.DM_NGUOIDUNG
                                    join cs in context.COSODAOTAO on u.COSO_ID equals cs.ID
                                    where u.TENDANGNHAP.Contains(userName)
                                    select new
                                    {
                                        u.DM_NGUOIDUNG_ID,
                                        u.TENDANGNHAP,
                                        u.MATKHAU,
                                        u.TRANGTHAI,
                                        u.DIENTHOAI,
                                        u.HOTEN,
                                        u.EMAIL,
                                        u.DIACHI,
                                        u.NGAYSINH,
                                        cs.TENCOSO
                                    }).ToList();
                    if (listUser.Count > 0)
                    {
                        grvUsers.Visible = true;
                        grvUsers.DataSource = listUser;
                        grvUsers.DataBind();
                    }
                    else
                    {
                        grvUsers.Visible = false;
                        ShowMessage("Không có người dùng nào", Utility.EnumSite.MessageType.Success);
                    }
                }
                else
                {
                    var listUser = (from u in context.DM_NGUOIDUNG
                                    join cs in context.COSODAOTAO on u.COSO_ID equals cs.ID
                                    select new
                                    {
                                        u.DM_NGUOIDUNG_ID,
                                        u.TENDANGNHAP,
                                        u.MATKHAU,
                                        u.TRANGTHAI,
                                        u.DIENTHOAI,
                                        u.HOTEN,
                                        u.EMAIL,
                                        u.DIACHI,
                                        u.NGAYSINH,
                                        cs.TENCOSO
                                    }).ToList();
                    if (listUser.Count > 0)
                    {
                        grvUsers.Visible = true;
                        grvUsers.DataSource = listUser;
                        grvUsers.DataBind();
                    }
                    else
                    {
                        grvUsers.Visible = false;
                        ShowMessage("Không có người dùng nào", Utility.EnumSite.MessageType.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi load danh sách người dùng: " + ex.Message, Utility.EnumSite.MessageType.Error);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim().Length == 0)
            {
                ShowMessage("Bạn chưa nhập tài khoản cần tìm", Utility.EnumSite.MessageType.Success);
                return;
            }
            else
            {
                LoadListUser(txtSearch.Text.Trim());
            }
        }
        protected void grvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string comanName = e.CommandName;
                var userId = long.Parse(e.CommandArgument.ToString());
                switch (comanName)
                {
                    case "sua":
                        Response.Redirect("ThemMoiTaiKhoan.aspx?UserId=" + userId, false);
                        break;
                    case "xoa":
                        try
                        {
                            var message = UserService.DeleteUser(userId);
                            if (message.Length == 0)
                            {
                                LoadListUser();
                                ShowMessage("Xóa người dùng thành công!", Utility.EnumSite.MessageType.Success);
                            }
                            else
                            {
                                ShowMessage("Lỗi xóa người dùng:" + message, Utility.EnumSite.MessageType.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi xóa:" + ex.Message, Utility.EnumSite.MessageType.Error);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi xảy ra:" + ex.Message, Utility.EnumSite.MessageType.Error);
            }
        }
        protected void grvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grvUsers.PageIndex = e.NewPageIndex;
                grvUsers.DataBind();
                LoadListUser();
            }
            catch (Exception)
            {
            }
        }
    }
}