﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssignUserToRole.ascx.cs" Inherits="WebUI.UserControl.Users.AssignUserToRole" %>
<div class="page-header">
    <h3>Phân quyền người dùng</h3>
</div>
<br />
<div class="messagealert" id="alert_container">
</div>
<div id="popupadd" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="panel-body">
                    <asp:GridView runat="server" ID="grvRole" ClientIDMode="Static" Width="100%" CssClass="table table-striped table-bordered table-hover dataTable no-footer DTTT_selectable"
                        AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField HeaderText="Thứ tự">
                                <ItemTemplate>
                                    <%#(Convert.ToInt32(DataBinder.Eval(Container, "RowIndex")) + 1)%>
                                    <asp:HiddenField runat="server" ID="hdfID" Value='<%# Eval("ROLE_ID") %>' ClientIDMode="Static" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle Width="7%" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tên vai trò">
                                <ItemTemplate>
                                    <%# Eval("ROLENAME")%>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle Width="20%" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Chọn">
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox Text="" ID="cbCheck" runat="server" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle Width="15%" HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">hủy</button>
                <asp:Button ID="btnAdd" ClientIDMode="Static" runat="server" class="btn btn-primary" OnClick="btnSave_Click"
                    Text="Phân quyền" />
            </div>
        </div>
    </div>
</div>
<%--  popup--%>
<div class="panel-body">
    <div class="form-group">
        <div class="col-sm-10">
            <div style="float: left; width: 280px">
                <asp:TextBox ID="txtSearch" Width="250px" runat="server" class="form-control" placeholder="nhập acc người dùng" />
            </div>
            <div style="float: left; width: 30%">
                <asp:Button ID="btnSearch" Text="Tìm" CssClass="btn btn-info" runat="server" OnClick="btnSearch_Click" />
            </div>
        </div>
    </div>
</div>
<div style="float: right; margin: 0px 0px 5px 0px">
    <asp:Button Text="Thêm mới" runat="server" ID="btnAddNew" CssClass="btn btn-info" PostBackUrl="~/ThemMoiTaiKhoan.aspx" />
</div>
<asp:GridView runat="server" ID="grvUsers" ClientIDMode="Static" Width="100%" CssClass="wapperpager table table-bordered table-hover dataTable"
    AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="grvUsers_PageIndexChanging"
    OnRowCommand="grvUsers_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="Thứ tự">
            <ItemTemplate>
                <%#(Convert.ToInt32(DataBinder.Eval(Container, "RowIndex")) + 1)%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="7%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tên đăng nhập">
            <ItemTemplate>
                <%# Eval("TENDANGNHAP")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="20%" HorizontalAlign="Center" ForeColor="#23527c" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Họ tên">
            <ItemTemplate>
                <%# Eval("HOTEN")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="20%" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Số điện thoại">
            <ItemTemplate>
                <%# Eval("DIENTHOAI")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="15%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tên cơ sở">
            <ItemTemplate>
                <%# Eval("TENCOSO")%>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="10%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Kích hoạt">
            <ItemTemplate>
                <%# Eval("TRANGTHAI") %>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="10%" HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="">
            <ItemTemplate>
                <asp:LinkButton ID="btnPhanQuyen" Text="Phân quyền" ToolTip="phân quyền" CommandArgument='<%# Eval("DM_NGUOIDUNG_ID") %>'
                    CommandName="sua" runat="server"></asp:LinkButton>
            </ItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="10%" HorizontalAlign="Center" />
        </asp:TemplateField>
    </Columns>
    <PagerSettings FirstPageText="Đầu" LastPageText="Cuối" Mode="NumericFirstLast" NextPageText="Tiếp"
        PreviousPageText="Trước" />
    <PagerStyle BackColor="White" BorderColor="#3366CC" BorderStyle="Outset" BorderWidth="1px"
        CssClass="paging" ForeColor="Blue" Height="40px" HorizontalAlign="Right" Width="200px" />
</asp:GridView>


<asp:HiddenField runat="server" ID="hdfUser_Id" ClientIDMode="Static" />
<script type="text/javascript">
    $(document).ready(function () {
        $('#btnAdd').click(function () {
            debugger
            var count = $('.table').find('input[type="checkbox"]:checked').length;
            if (count == 0) {
                alert('Bạn chưa chọn vai trò');
                return false;
            }
            if (count > 1) {
                alert('Bạn chỉ được phép chọn 1 vai trò');
                return false;
            }
        });
    });
</script>
