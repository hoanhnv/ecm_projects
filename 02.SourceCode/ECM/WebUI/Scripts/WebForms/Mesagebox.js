﻿function ShowMessage(message, messagetype) { // hiển thị thông báo
    var cssclass;
    switch (messagetype) {
        case 'Success':
            messagetype = "Thông báo";
            $('#alert_container').delay(3000).fadeOut();
            cssclass = 'alert-success'
            break;
        case 'Error':
            messagetype = "Lỗi";
            cssclass = 'alert-danger'
            break;
        case 'Warning':
            messagetype = "Cảnh báo";
            //$('#alert_container').delay(3000).fadeOut();
            cssclass = 'alert-warning'
            break;
        default:
            cssclass = 'alert-info'
    }

    $('#alert_container').append('<div id="alert_div" width:70% style="margin: 0 0.5%; -webkit-box-shadow: 3px 4px 6px #999;" class="alert fade in ' + cssclass + '"><strong>' + messagetype + '!</strong> <span><center>' + message + '</center></span></div>');

}