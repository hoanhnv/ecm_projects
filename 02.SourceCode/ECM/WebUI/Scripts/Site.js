﻿function ShowPopup(divPopUpId) {
    $("#" + divPopUpId).modal('show');
}

$(document).ready(function () {

    $(".datepicker").datepicker({
        showOn: "button",
        buttonImage: "Content/images/calendar-icon.png",
        buttonImageOnly: true,
        buttonText: "Chọn ngày",
        dateFormat: "dd/mm/yy"
    });
    $(".datepicker").datepicker($.datepicker.regional["vn"]);
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function validatephonenumber(phone) {
    var phoneRe = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4,6}$/;
    var digits = phone.replace(/\D/g, "");
    return (digits.match(phoneRe) !== null);
}
// false: là số, true: không phải số
function checkIsNumber(input)
{
    return isNaN(input);
}