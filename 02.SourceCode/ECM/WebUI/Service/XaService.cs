﻿using WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Service
{
    public class XaService
    {
        static ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<XA> GetListXa(int takeNumberRow = 0)
        {
            List<XA> listXa = new List<XA>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listXa = context.XA.Take(takeNumberRow).OrderBy(o => o.TENXA).ToList();
                }
                else
                {
                    listXa = context.XA.ToList();
                }
            }
            catch (Exception)
            {
            }

            return listXa;
        }
        public static List<XA> GetListXaByName(string name)
        {
            List<XA> listXa = new List<XA>();
            try
            {
                listXa = context.XA.Where(o => o.TENXA.Contains(name)).OrderBy(o => o.TENXA).ToList();
            }
            catch (Exception)
            {
            }

            return listXa;
        }
        public static XA GetXaByID(long huyenId)
        {
            XA huyen = new XA();
            try
            {
                huyen = context.XA.Where(o => o.XA_ID == huyenId).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return huyen;
        }
        public static List<XA> GetListXaByHuyen_ID(long huyenID)
        {
            List<XA> huyen = new List<XA>();
            try
            {
                huyen = context.XA.Where(o => o.HUYEN_ID == huyenID).OrderBy(o => o.TENXA).ToList();
            }
            catch (Exception)
            {
            }
            return huyen;
        }
        public static string InsertXa(XA xa)
        {
            string result = "";
            try
            {
                context.XA.Add(xa);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetXa(int xaId, string maXa, string tenXa, int huyenId)
        {
            string result = "";
            XA xa = context.XA.Where(o => o.XA_ID == xaId).SingleOrDefault();
            try
            {
                if (xa != null)
                {
                    xa.MAXA = maXa;
                    xa.TENXA = tenXa;
                    xa.HUYEN_ID = huyenId;

                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeleteXa(int id)
        {
            string result = "";
            try
            {
                List<THON_XOM> listThon = context.THON_XOM.Where(o => o.XA_ID == id).ToList();
                if (listThon.Count > 0)
                {
                    return result = "Không thể xóa xã do vẫn còn thôn trong xã này.";
                }
                XA huyen = context.XA.Where(o => o.XA_ID == id).SingleOrDefault();
                if (huyen != null)
                {
                    context.XA.Remove(huyen);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}