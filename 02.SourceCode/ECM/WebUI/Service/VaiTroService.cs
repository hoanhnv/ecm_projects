﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Service
{
    public class VaiTroService
    {
        static Model.ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<VAITRO> GetListVaiTro(int takeNumberRow = 0)
        {
            List<VAITRO> listVaiTro = new List<VAITRO>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listVaiTro = context.VAITRO.Take(takeNumberRow).OrderBy(o => o.TENVAITRO).ToList();
                }
                else
                {
                    listVaiTro = context.VAITRO.ToList();
                }
            }
            catch (Exception)
            {
            }

            return listVaiTro;
        }
        public static VAITRO GetVaiTroByID(long vaitroId)
        {
            VAITRO vaiTro = new VAITRO();
            try
            {
                vaiTro = context.VAITRO.Where(o => o.VAITRO_ID == vaitroId).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return vaiTro;
        }
        public static string InsertVaiTro(VAITRO vaitro)
        {
            string result = "";
            try
            {
                context.VAITRO.Add(vaitro);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetVaiTro(int vaiTroId, string tenVaiTro, bool isActive)
        {
            string result = "";
            VAITRO vaiTro = context.VAITRO.Where(o => o.VAITRO_ID == vaiTroId).SingleOrDefault();
            try
            {
                if (vaiTro != null)
                {
                    vaiTro.TENVAITRO = tenVaiTro;
                    vaiTro.ISACTIVE = isActive;
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeleteVaiTro(int id)
        {
            string result = "";
            try
            {
                VAITRO vaiTro = context.VAITRO.Where(o => o.VAITRO_ID == id).SingleOrDefault();
                if (vaiTro != null)
                {
                    context.VAITRO.Remove(vaiTro);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}