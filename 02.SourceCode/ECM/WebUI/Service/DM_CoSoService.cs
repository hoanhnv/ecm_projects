﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Service
{
    public class DM_CoSoService
    {
        static ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<COSODAOTAO> GetListCoSoDaoTao(int takeNumberRow = 0)
        {
            List<COSODAOTAO> listCoSoDaoTao = new List<COSODAOTAO>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listCoSoDaoTao = context.COSODAOTAO.Take(takeNumberRow).Where(o => o.IS_ACTIVE == true).OrderBy(o => o.TENCOSO).ToList();
                }
                else
                {
                    listCoSoDaoTao = context.COSODAOTAO.ToList();
                }
            }
            catch (Exception)
            {
            }

            return listCoSoDaoTao;
        }
        public static List<COSODAOTAO> GetListCoSoDaoTaoByThonXaHuyen(int takeNumberRow = 0, int huyenId = 0, int xaId = 0, int thonId = 0)
        {
            List<COSODAOTAO> listCoSoDaoTao = new List<COSODAOTAO>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listCoSoDaoTao = context.COSODAOTAO.Take(takeNumberRow).Where(o => o.IS_ACTIVE == true &&
                    (huyenId > 0 ? o.HUYEN_ID == huyenId : huyenId == 0) && (xaId > 0 ? o.XA_ID == xaId : xaId == 0) && (thonId > 0 ? o.THON_ID == thonId : thonId == 0)).OrderBy(o => o.TENCOSO).ToList();
                }
                else
                {
                    listCoSoDaoTao = context.COSODAOTAO.ToList();
                }
            }
            catch (Exception)
            {
            }

            return listCoSoDaoTao;
        }
        public static COSODAOTAO GetCoSoDaoTaoByID(long Id)
        {
            COSODAOTAO cosodaotao = new COSODAOTAO();
            try
            {
                cosodaotao = context.COSODAOTAO.Where(o => o.ID == Id).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return cosodaotao;
        }
        public static List<COSODAOTAO> GetCoSoDaoTaoByName(string name)
        {
            List<COSODAOTAO> listCoSoDaoTao = new List<COSODAOTAO>();
            try
            {
                listCoSoDaoTao = context.COSODAOTAO.Where(o => o.TENCOSO.Contains(name)).ToList();
            }
            catch (Exception)
            {
            }

            return listCoSoDaoTao;
        }
        public static string InsertCoSoDaoTao(COSODAOTAO coSoDaoTao)
        {
            string result = "";
            try
            {
                context.COSODAOTAO.Add(coSoDaoTao);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetCoSoDaoTao(COSODAOTAO cosodaotao)
        {
            string result = "";
            COSODAOTAO coSoDaoTaoUpdate = context.COSODAOTAO.Where(o => o.ID == cosodaotao.ID).SingleOrDefault();
            try
            {
                if (coSoDaoTaoUpdate != null)
                {
                    coSoDaoTaoUpdate.MACOSO = cosodaotao.MACOSO;
                    coSoDaoTaoUpdate.TENCOSO = cosodaotao.TENCOSO;
                
                    coSoDaoTaoUpdate.DIENTHOAI = cosodaotao.DIENTHOAI;
                    coSoDaoTaoUpdate.FAX = cosodaotao.FAX;
                    coSoDaoTaoUpdate.EMAIL = cosodaotao.EMAIL;
                    coSoDaoTaoUpdate.WEBSITE = cosodaotao.WEBSITE;
               
                    coSoDaoTaoUpdate.NGAYTAO = cosodaotao.NGAYTAO;
                    coSoDaoTaoUpdate.NGUOITAO = cosodaotao.NGUOITAO;
                    coSoDaoTaoUpdate.NGAYSUA = cosodaotao.NGAYSUA;
                    coSoDaoTaoUpdate.NGUOISUA = cosodaotao.NGUOISUA;
                    coSoDaoTaoUpdate.IS_APPROVE = cosodaotao.IS_APPROVE;
                    coSoDaoTaoUpdate.NGUOIDUYET = cosodaotao.NGUOIDUYET;
                    coSoDaoTaoUpdate.NGAYDUYET = cosodaotao.NGAYDUYET;
                    coSoDaoTaoUpdate.IS_ACTIVE = cosodaotao.IS_ACTIVE;
                    coSoDaoTaoUpdate.NGUOIXUATBAN = cosodaotao.NGUOIXUATBAN;
                    coSoDaoTaoUpdate.NGAYXUATBAN = cosodaotao.NGAYXUATBAN;
                    coSoDaoTaoUpdate.THON_ID = cosodaotao.THON_ID;
                    coSoDaoTaoUpdate.XA_ID = cosodaotao.XA_ID;
                    coSoDaoTaoUpdate.HUYEN_ID = cosodaotao.HUYEN_ID;

                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy cơ sở đào tạo muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeleteCoSoDaoTao(long id)
        {
            string result = "";
            try
            {
                COSODAOTAO cosodaotao = context.COSODAOTAO.Where(o => o.ID == id).SingleOrDefault();
                if (cosodaotao != null)
                {
                    context.COSODAOTAO.Remove(cosodaotao);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy cơ sở đào tạo muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}