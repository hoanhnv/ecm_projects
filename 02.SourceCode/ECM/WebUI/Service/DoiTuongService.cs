﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Service
{
    public class DoiTuongService
    {
        static ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<DOITUONG> GetListDoiTuong(int takeNumberRow = 0)
        {
            List<DOITUONG> listDoiTuong = new List<DOITUONG>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listDoiTuong = context.DOITUONG.Take(takeNumberRow).OrderBy(o => o.TEN_DOITUONG).ToList();
                }
                else
                {
                    listDoiTuong = context.DOITUONG.OrderBy(o => o.TEN_DOITUONG).ToList();
                }
            }
            catch (Exception)
            {
            }

            return listDoiTuong;
        }
        public static List<DOITUONG> GetListDoiTuongByThonXaHuyen(int takeNumberRow = 0, int huyenId = 0, int xaId = 0, int thonId = 0)
        {
            List<DOITUONG> listDoiTuong = new List<DOITUONG>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listDoiTuong = context.DOITUONG.Take(takeNumberRow).Where(o =>
                    (huyenId > 0 ? o.HUYEN_ID == huyenId : huyenId == 0) && (xaId > 0 ? o.XA_ID == xaId : xaId == 0)
                    && (thonId > 0 ? o.THON_ID == thonId : thonId == 0)).OrderBy(o => o.TEN_DOITUONG).ToList();
                }
                else
                {
                    listDoiTuong = context.DOITUONG.ToList();
                }
            }
            catch (Exception)
            {
            }

            return listDoiTuong;
        }
        public static List<DOITUONG> GetListDoiTuongByCoSoId(int coSo_Id)
        {
            List<DOITUONG> listDoiTuong = new List<DOITUONG>();
            try
            {
                listDoiTuong = context.DOITUONG.Where(o => o.COSODAOTAO_ID == coSo_Id).OrderBy(o => o.TEN_DOITUONG).ToList();
            }
            catch (Exception)
            {
            }

            return listDoiTuong;
        }
        public static DOITUONG GetDoiTuongByID(long Id)
        {
            DOITUONG doiTuong = new DOITUONG();
            try
            {
                doiTuong = context.DOITUONG.Where(o => o.DOITUONG_ID == Id).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return doiTuong;
        }
        public static List<DOITUONG> GetDoiTuongByName(string name)
        {
            List<DOITUONG> listDoiTuong = new List<DOITUONG>();
            try
            {
                listDoiTuong = context.DOITUONG.Where(o => o.TEN_DOITUONG.Contains(name)).ToList();
            }
            catch (Exception)
            {
            }

            return listDoiTuong;
        }
        public static string InsertDoiTuong(DOITUONG doiTuong)
        {
            string result = "";
            try
            {
                context.DOITUONG.Add(doiTuong);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetCoSoDaoTao(DOITUONG doiTuong)
        {
            string result = "";
            DOITUONG doiTuongUpdate = context.DOITUONG.Where(o => o.DOITUONG_ID == doiTuong.DOITUONG_ID).SingleOrDefault();
            try
            {
                if (doiTuongUpdate != null)
                {
                    doiTuongUpdate.TEN_DOITUONG = doiTuong.TEN_DOITUONG;
                    doiTuongUpdate.NGAY_SINH = doiTuong.NGAY_SINH;
                    doiTuongUpdate.MA_DOITUONG = doiTuong.MA_DOITUONG;
                    doiTuongUpdate.DIACHI_HIENTAI = doiTuong.DIACHI_HIENTAI;
                    doiTuongUpdate.HOTEN_CHA = doiTuong.HOTEN_CHA;
                    doiTuongUpdate.HOTEN_ME = doiTuong.HOTEN_ME;
                    doiTuongUpdate.TUOI = doiTuong.TUOI;
                    doiTuongUpdate.NGHE_NGHIEP = doiTuong.NGHE_NGHIEP;
                    doiTuongUpdate.COSODAOTAO_ID = doiTuong.COSODAOTAO_ID;
                    doiTuongUpdate.HUYEN_ID = doiTuong.HUYEN_ID;
                    doiTuongUpdate.XA_ID = doiTuong.XA_ID;
                    doiTuongUpdate.THON_ID = doiTuong.THON_ID;

                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy đối tượng muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeleteDoituong(long id)
        {
            string result = "";
            try
            {
                DOITUONG doiTuong = context.DOITUONG.Where(o => o.DOITUONG_ID == id).SingleOrDefault();
                if (doiTuong != null)
                {
                    context.DOITUONG.Remove(doiTuong);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy đối tượng muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}