﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Service
{
    public class DM_ChucNangService
    {
        static ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<DM_CHUCNANG> GetListDanhMucChucNang()
        {
            List<DM_CHUCNANG> listDanhMucChucNang = new List<DM_CHUCNANG>();
            try
            {
                listDanhMucChucNang = context.DM_CHUCNANG.OrderBy(o => o.TEN_CHUCNANG).ToList();
            }
            catch (Exception)
            {
            }

            return listDanhMucChucNang;
        }
        public static DM_CHUCNANG GetDanhMucChucNangById(long Id)
        {
            DM_CHUCNANG chucNang = new DM_CHUCNANG();
            try
            {
                chucNang = context.DM_CHUCNANG.Where(o => o.DM_CHUCNANG_ID == Id).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return chucNang;
        }
        public static List<DM_CHUCNANG> GetDanhMucChucNangByName(string name)
        {
            List<DM_CHUCNANG> listChucNang = new List<DM_CHUCNANG>();
            try
            {
                listChucNang = context.DM_CHUCNANG.Where(o => o.TEN_CHUCNANG.Contains(name)).ToList();
            }
            catch (Exception)
            {
            }

            return listChucNang;
        }
        public static string InsertChucNang(DM_CHUCNANG chucNang)
        {
            string result = "";
            try
            {
                context.DM_CHUCNANG.Add(chucNang);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetChucNang(DM_CHUCNANG chucNang)
        {
            string result = "";
            DM_CHUCNANG chucNangUpdate = context.DM_CHUCNANG.Where(o => o.DM_CHUCNANG_ID == chucNang.DM_CHUCNANG_ID).SingleOrDefault();
            try
            {
                if (chucNangUpdate != null)
                {
                    chucNangUpdate.MA_CHUCNANG = chucNang.MA_CHUCNANG;
                    chucNangUpdate.TEN_CHUCNANG = chucNang.TEN_CHUCNANG;
                    chucNangUpdate.TRANGTHAI = chucNang.TRANGTHAI;
                    chucNangUpdate.NGAYSUA = DateTime.Now; ;
                    chucNangUpdate.CHUCNANG_CHA = chucNang.CHUCNANG_CHA;
                    chucNangUpdate.URL = chucNang.URL;
                    chucNangUpdate.TT_HIENTHI = chucNang.TT_HIENTHI;
                    chucNangUpdate.NGUOISUA = chucNang.NGUOISUA;
                    chucNangUpdate.IS_HIDDEN = chucNang.IS_HIDDEN;

                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy chức năng muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeleteChucNangById(long id)
        {
            string result = "";
            try
            {
                DM_CHUCNANG chucNang = context.DM_CHUCNANG.Where(o => o.DM_CHUCNANG_ID == id).SingleOrDefault();
                if (chucNang != null)
                {
                    context.DM_CHUCNANG.Remove(chucNang);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy chức năng muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }

    }
}