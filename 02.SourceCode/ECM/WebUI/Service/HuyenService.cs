﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Service
{
    public class HuyenService
    {
        static ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<HUYEN> GetListHuyen(int takeNumberRow = 0)
        {
            List<HUYEN> listHuyen = new List<HUYEN>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listHuyen = context.HUYEN.Take(takeNumberRow).OrderBy(o => o.TENHUYEN).ToList();
                }
                else
                {
                    listHuyen = context.HUYEN.ToList();
                }
            }
            catch (Exception)
            {
            }

            return listHuyen;
        }
        public static List<HUYEN> GetListHuyenByName(string name)
        {
            List<HUYEN> listHuyen = new List<HUYEN>();
            try
            {
                listHuyen = context.HUYEN.Where(o => o.TENHUYEN.Contains(name)).OrderBy(o => o.TENHUYEN).ToList();
            }
            catch (Exception)
            {
            }

            return listHuyen;
        }
        public static HUYEN GetHuyenByID(int huyenId)
        {
            HUYEN huyen = new HUYEN();
            try
            {
                huyen = context.HUYEN.Where(o => o.HUYEN_ID == huyenId).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return huyen;
        }
        public static List<HUYEN> GetListHuyenByTinhId(long tinhId)
        {
            List<HUYEN> huyen = new List<HUYEN>();
            try
            {
                huyen = context.HUYEN.Where(o => o.TINH_ID == tinhId).OrderBy(o => o.TENHUYEN).ToList();
            }
            catch (Exception)
            {
            }
            return huyen;
        }
        public static string InsertHuyen(HUYEN HUYEN)
        {
            string result = "";
            try
            {
                context.HUYEN.Add(HUYEN);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetHuyen(int huyenId, string maHuyen, string tenHUYEN, int tinhId)
        {
            string result = "";
            HUYEN huyen = context.HUYEN.Where(o => o.HUYEN_ID == huyenId).SingleOrDefault();
            try
            {
                if (huyen != null)
                {
                    huyen.MAHUYEN = maHuyen;
                    huyen.TENHUYEN = tenHUYEN;
                    huyen.TINH_ID = tinhId;
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeletetHuyen(int id)
        {
            string result = "";
            try
            {
                List<XA> listXa = context.XA.Where(o => o.HUYEN_ID == id).ToList();
                if (listXa.Count > 0)
                {
                    return result = "Không thể xóa huyện do vẫn còn xã trong huyện này.";
                }
                HUYEN huyen = context.HUYEN.Where(o => o.HUYEN_ID == id).SingleOrDefault();
                if (huyen != null)
                {
                    context.HUYEN.Remove(huyen);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}