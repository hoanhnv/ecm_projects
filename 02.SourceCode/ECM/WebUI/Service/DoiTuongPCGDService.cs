﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Service
{
    public class DoiTuongPCGDService
    {
        static ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<DOITUONG> GetListDoiTuong(int takeNumberRow = 0)
        {
            List<DOITUONG> listDoiTuong = new List<DOITUONG>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listDoiTuong = context.DOITUONG.Take(takeNumberRow).OrderBy(o => o.TEN_DOITUONG).ToList();
                }
                else
                {
                    listDoiTuong = context.DOITUONG.ToList();
                }
            }
            catch (Exception)
            {
            }

            return listDoiTuong;
        }
        public static DOITUONG GetDoiTuongByID(long doituongId)
        {
            DOITUONG doituong = new DOITUONG();
            try
            {
                doituong = context.DOITUONG.Where(o => o.DOITUONG_ID == doituongId).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return doituong;
        }
        public static List<DOITUONG> GetDoiTuongByName(string name)
        {
            List<DOITUONG> listDoiTuong = new List<DOITUONG>();
            try
            {
                listDoiTuong = context.DOITUONG.Where(o => o.TEN_DOITUONG.Contains(name)).ToList();
            }
            catch (Exception )
            {
            }

            return listDoiTuong;
        }
        public static string InsertDoiTuong(DOITUONG doituong)
        {
            string result = "";
            try
            {
                context.DOITUONG.Add(doituong);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetDoiTuong(DOITUONG doituong)
        {
            string result = "";
            DOITUONG doituongUpdate = context.DOITUONG.Where(o => o.DOITUONG_ID == doituong.DOITUONG_ID).SingleOrDefault();
            try
            {
                if (doituongUpdate != null)
                {
                    doituongUpdate.TEN_DOITUONG = doituong.TEN_DOITUONG;
                    doituongUpdate.NGAY_SINH = doituong.NGAY_SINH;
                    doituongUpdate.MA_DOITUONG = doituong.MA_DOITUONG;
                    doituongUpdate.DIACHI_HIENTAI = doituong.DIACHI_HIENTAI;
                    doituongUpdate.THON_ID = doituong.THON_ID;
                    doituongUpdate.XA_ID = doituong.XA_ID;
                    doituongUpdate.HOTEN_CHA = doituong.HOTEN_CHA;
                    doituongUpdate.HOTEN_ME = doituong.HOTEN_ME;
          
                    doituongUpdate.COSODAOTAO_ID = doituong.COSODAOTAO_ID;

                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy đối tượng muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeleteDoiTuong(long doituongId)
        {
            string result = "";
            try
            {
                DOITUONG doituong = context.DOITUONG.Where(o => o.DOITUONG_ID == doituongId).SingleOrDefault();
                if (doituong != null)
                {
                    context.DOITUONG.Remove(doituong);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy đối tượng muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}