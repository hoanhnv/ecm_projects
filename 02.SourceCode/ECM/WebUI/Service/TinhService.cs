﻿using WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Service
{
    public class TinhService
    {
        static ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<TINH> GetListTinh(int takeNumberRow = 0)
        {
            List<TINH> listTinh = new List<TINH>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listTinh = context.TINH.Take(takeNumberRow).OrderBy(o => o.TENTINH).ToList();
                }
                else
                {
                    listTinh = context.TINH.ToList();
                }
            }
            catch (Exception)
            {
            }

            return listTinh;
        }
        public static List<TINH> GetListTinhByName(string name)
        {
            List<TINH> listTinh = new List<TINH>();
            try
            {
                listTinh = context.TINH.Where(o => o.TENTINH.Contains(name)).OrderBy(o => o.TENTINH).ToList();
            }
            catch (Exception)
            {
            }

            return listTinh;
        }
        public static TINH GetTinhByID(long tinhId)
        {
            TINH tinh = new TINH();
            try
            {
                tinh = context.TINH.Where(o => o.TINH_ID == tinhId).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return tinh;
        }
        public static string InsertTinh(TINH tinh)
        {
            string result = "";
            try
            {
                context.TINH.Add(tinh);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetTinh(int tinhId, string matinh, string tentinh)
        {
            string result = "";
            TINH tinh = context.TINH.Where(o => o.TINH_ID == tinhId).SingleOrDefault();
            try
            {
                if (tinh != null)
                {
                    tinh.MATINH = matinh;
                    tinh.TENTINH = tentinh;
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeleteTinh(int id)
        {
            string result = "";
            try
            {
                List<HUYEN> listHuyen = context.HUYEN.Where(o => o.TINH_ID == id).ToList();
                if (listHuyen.Count > 0)
                {
                    return result = "Không thể xóa tỉnh do vẫn còn huyện trong tỉnh này.";
                }
                TINH tinh = context.TINH.Where(o => o.TINH_ID == id).SingleOrDefault();
                if (tinh != null)
                {
                    context.TINH.Remove(tinh);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}