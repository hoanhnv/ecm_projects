﻿using System;
using System.Linq;
using System.Collections.Generic;
using WebUI.Models;
using Business.CommonHelper;

namespace WebUI.Service
{
    public class UserService
    {
        static ECM_CSDLEntities context = new ECM_CSDLEntities();
        static MaHoaMatKhau mahoa = new MaHoaMatKhau();
        public static DM_NGUOIDUNG Login(string userName, string password)
        {
            DM_NGUOIDUNG nguoiDung = new DM_NGUOIDUNG();
            string passwordMahoa = mahoa.Encode_Data(password).ToString();
            try
            {
                nguoiDung = context.DM_NGUOIDUNG.Where(o => o.TENDANGNHAP == userName && o.MATKHAU == passwordMahoa).SingleOrDefault();
            }
            catch (Exception ex)
            {
            }

            return nguoiDung;
        }
        public static List<DM_NGUOIDUNG> GetListUser(int takeNumberRow = 0)
        {
            List<DM_NGUOIDUNG> listUser = new List<DM_NGUOIDUNG>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listUser = context.DM_NGUOIDUNG.Take(takeNumberRow).ToList();
                }
                else
                {
                    listUser = context.DM_NGUOIDUNG.ToList();
                }
            }
            catch (Exception)
            {
            }
            return listUser;
        }
        public static DM_NGUOIDUNG GetUserByUserID(long userId)
        {
            DM_NGUOIDUNG user = new DM_NGUOIDUNG();
            try
            {
                user = context.DM_NGUOIDUNG.Where(o => o.DM_NGUOIDUNG_ID == userId).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return user;
        }
        public static int GetLastUserId()
        {
            int userId = 0;
            DM_NGUOIDUNG user = new DM_NGUOIDUNG();
            try
            {
                user = context.DM_NGUOIDUNG.OrderByDescending(o => o.DM_NGUOIDUNG_ID).Take(1).SingleOrDefault();
                if (user != null)
                {
                    userId = int.Parse(user.DM_NGUOIDUNG_ID.ToString());
                }
            }
            catch (Exception)
            {
            }
            return userId;
        }
        public static DM_NGUOIDUNG GetUserByUserName(string userName)
        {
            DM_NGUOIDUNG user = new DM_NGUOIDUNG();
            try
            {
                user = context.DM_NGUOIDUNG.Where(o => o.TENDANGNHAP == userName).SingleOrDefault();
            }
            catch (Exception)
            {
            }

            return user;
        }
        public static string InsertUser(DM_NGUOIDUNG user)
        {
            string result = "";
            try
            {
                context.DM_NGUOIDUNG.Add(user);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetUser(DM_NGUOIDUNG user)
        {
            string result = "";
            DM_NGUOIDUNG userUpdate = context.DM_NGUOIDUNG.Where(o => o.DM_NGUOIDUNG_ID == user.DM_NGUOIDUNG_ID).SingleOrDefault();
            try
            {
                if (userUpdate != null)
                {
                    userUpdate.TRANGTHAI = user.TRANGTHAI;
                    userUpdate.NGAYSUA = user.NGAYSUA;
                    userUpdate.DIENTHOAI = user.DIENTHOAI;
                    userUpdate.COSO_ID = user.COSO_ID;
                    userUpdate.NGUOISUA = user.NGUOISUA;
                    userUpdate.HOTEN = user.HOTEN;
                    userUpdate.EMAIL = user.EMAIL;
                    userUpdate.DIACHI = user.DIACHI;
                    userUpdate.NGAYSINH = user.NGAYSINH;
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy tài khoản người dùng muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {
                userUpdate = null;
            }

            return result;
        }
        public static string DeleteUser(long userId)
        {
            string result = "";
            try
            {
                DM_NGUOIDUNG user = context.DM_NGUOIDUNG.Where(o => o.DM_NGUOIDUNG_ID == userId).SingleOrDefault();
                if (user != null)
                {
                    context.DM_NGUOIDUNG.Remove(user);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy tài khoản muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string ChangePassword(int userId, string oldPassword, string newPassword)
        {
            string result = "";
            string matkhauCu = mahoa.Encode_Data(oldPassword).ToString();
            string matkhauMoi = mahoa.Encode_Data(newPassword).ToString();
            DM_NGUOIDUNG userUpdate = context.DM_NGUOIDUNG.Where(o => o.DM_NGUOIDUNG_ID == userId && o.MATKHAU == matkhauCu).SingleOrDefault();
            try
            {
                if (userUpdate != null)
                {
                    userUpdate.MATKHAU = matkhauMoi;
                    context.SaveChanges();
                }
                else
                {
                    result = "Mật khẩu cũ không đúng";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {
                userUpdate = null;
            }

            return result;
        }
    }
}