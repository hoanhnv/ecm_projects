﻿using WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Service
{
    public class User_RoleService
    {
        static ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<USER_ROLE> GetListUserRoleByUserId(long userId)
        {
            List<USER_ROLE> listUserRole = new List<USER_ROLE>();
            try
            {
                listUserRole = context.USER_ROLE.Where(o => o.USER_ID == userId).ToList();
            }
            catch (Exception)
            {
            }

            return listUserRole;
        }
        public static List<USER_ROLE> GetListUserRoleByRole(long roleId, int numberTake =0)
        {
            List<USER_ROLE> listUserRole = new List<USER_ROLE>();
            try
            {
                if (numberTake > 0)
                {
                    listUserRole = context.USER_ROLE.Where(o => o.ROLE_ID == roleId).Take(numberTake).ToList();
                }
                else
                {
                    listUserRole = context.USER_ROLE.Where(o => o.ROLE_ID == roleId).ToList();
                }
            }
            catch (Exception)
            {
            }

            return listUserRole;
        }
        public static USER_ROLE GetRoleByID(int Id)
        {
            USER_ROLE userRole = new USER_ROLE();
            try
            {
                userRole = context.USER_ROLE.Where(o => o.ID == Id).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return userRole;
        }
        public static string InsertUserRole(USER_ROLE userRole)
        {
            string result = "";
            try
            {
                context.USER_ROLE.Add(userRole);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string InsertListUserRole(List<USER_ROLE> listUserRole)
        {
            string result = "";
            try
            {
                context.USER_ROLE.AddRange(listUserRole);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetUserRple(int Id, int userId, int roleId)
        {
            string result = "";
            USER_ROLE userRole = context.USER_ROLE.Where(o => o.ID == Id).SingleOrDefault();
            try
            {
                if (userRole != null)
                {
                    userRole.USER_ID = userId;
                    userRole.ROLE_ID = roleId;
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeleteUserRole(int id)
        {
            string result = "";
            try
            {
                USER_ROLE phongban = context.USER_ROLE.Where(o => o.ID == id).SingleOrDefault();
                if (phongban != null)
                {
                    context.USER_ROLE.Remove(phongban);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeleteUserRoleByUserId(long userId)
        {
            string result = "";
            try
            {
                List<USER_ROLE> listRoleUser = context.USER_ROLE.Where(o => o.ROLE_ID == userId).ToList();
                if (listRoleUser != null)
                {
                    context.USER_ROLE.RemoveRange(listRoleUser);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}