﻿using WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Service
{
    public class VITRI_NGUOIDUNGService
    {
        static  ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<VITRI_NGUOIDUNG> GetList_ViTri_NguoiDung(int takeNumberRow = 0)
        {
            List<VITRI_NGUOIDUNG> listViTriNguoiDung = new List<VITRI_NGUOIDUNG>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listViTriNguoiDung = context.VITRI_NGUOIDUNG.Take(takeNumberRow).ToList();
                }
                else
                {
                    listViTriNguoiDung = context.VITRI_NGUOIDUNG.ToList();
                }
            }
            catch (Exception)
            {
            }

            return listViTriNguoiDung;
        }
        public static VITRI_NGUOIDUNG GetViTriNguoiDungByUser_Id(long user_Id)
        {
            VITRI_NGUOIDUNG vitringuoidung = new VITRI_NGUOIDUNG();
            try
            {
                vitringuoidung = context.VITRI_NGUOIDUNG.Where(o => o.NGUOIDUNG_ID == user_Id).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return vitringuoidung;
        }
        public static string InsertUpdateViTriNguoiDung(VITRI_NGUOIDUNG viTriNguoiDung)
        {
            string result = "";
            VITRI_NGUOIDUNG vitriNguoiDungCheck = context.VITRI_NGUOIDUNG.Where(o => o.NGUOIDUNG_ID == viTriNguoiDung.NGUOIDUNG_ID).SingleOrDefault();
            try
            {
                if (vitriNguoiDungCheck != null) //tồn tại rồi => cập nhật
                {
                    vitriNguoiDungCheck.TINH_ID = viTriNguoiDung.TINH_ID;
                    vitriNguoiDungCheck.HUYEN_ID = viTriNguoiDung.HUYEN_ID;
                    vitriNguoiDungCheck.XA_ID = viTriNguoiDung.XA_ID;
                    vitriNguoiDungCheck.THON_ID = viTriNguoiDung.THON_ID;
                }
                else // thêm mới
                {
                    context.VITRI_NGUOIDUNG.Add(viTriNguoiDung);
                }

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeletetViTriNguoiDung(long userId)
        {
            string result = "";
            try
            {
                VITRI_NGUOIDUNG viTriNguoiDung = context.VITRI_NGUOIDUNG.Where(o => o.NGUOIDUNG_ID == userId).SingleOrDefault();
                if (viTriNguoiDung != null)
                {
                    context.VITRI_NGUOIDUNG.Remove(viTriNguoiDung);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi cần xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}