﻿using WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Service
{
    public class ThonService
    {
        static  ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<THON_XOM> GetListThonXom(int takeNumberRow = 0)
        {
            List<THON_XOM> listThonXom = new List<THON_XOM>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listThonXom = context.THON_XOM.Take(takeNumberRow).OrderBy(o => o.TEN_THON).ToList();
                }
                else
                {
                    listThonXom = context.THON_XOM.ToList();
                }
            }
            catch (Exception)
            {
            }

            return listThonXom;
        }
        public static List<THON_XOM> GetListThonXomByName(string name)
        {
            List<THON_XOM> listThonXom = new List<THON_XOM>();
            try
            {
                listThonXom = context.THON_XOM.Where(o => o.TEN_THON.Contains(name)).OrderBy(o => o.TEN_THON).ToList();
            }
            catch (Exception)
            {
            }

            return listThonXom;
        }
        public static THON_XOM GetThonXomByID(long xomId)
        {
            THON_XOM thon = new THON_XOM();
            try
            {
                thon = context.THON_XOM.Where(o => o.THON_ID == xomId).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return thon;
        }
        public static List<THON_XOM> GetListThonXomByXaID(long xaID)
        {
            List<THON_XOM> thon = new List<THON_XOM>();
            try
            {
                thon = context.THON_XOM.Where(o => o.XA_ID == xaID).OrderBy(o => o.TEN_THON).ToList();
            }
            catch (Exception)
            {
            }
            return thon;
        }
        public static string InsertThonXom(THON_XOM THON_XOM)
        {
            string result = "";
            try
            {
                context.THON_XOM.Add(THON_XOM);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetThonXom(int xomId, string mathon, string tenTHON_XOM, int huyenId, int xaId)
        {
            string result = "";
            THON_XOM thon = context.THON_XOM.Where(o => o.THON_ID == xomId).SingleOrDefault();
            try
            {
                if (thon != null)
                {
                    thon.MA_THON = mathon;
                    thon.TEN_THON = tenTHON_XOM;
                    thon.HUYEN_ID = huyenId;
                    thon.XA_ID = xaId;
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeleteThonXom(int id)
        {
            string result = "";
            try
            {
                THON_XOM thon = context.THON_XOM.Where(o => o.THON_ID == id).SingleOrDefault();
                if (thon != null)
                {
                    context.THON_XOM.Remove(thon);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}