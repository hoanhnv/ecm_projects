﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Service
{
    public class RoleFunctionService
    {
        static ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<ROLE_FUNCTION> GetListRoleFunction()
        {
            List<ROLE_FUNCTION> listRoleFunction = new List<ROLE_FUNCTION>();
            try
            {
                listRoleFunction = context.ROLE_FUNCTION.ToList();
            }
            catch (Exception)
            {
            }

            return listRoleFunction;
        }
        public static List<ROLE_FUNCTION> GetListFunctionByRoleId(int roleId)
        {
            List<ROLE_FUNCTION> listRoleFunction = new List<ROLE_FUNCTION>();
            try
            {
                listRoleFunction = context.ROLE_FUNCTION.Where(o => o.ROLE_ID == roleId).ToList();
            }
            catch (Exception)
            {
            }
            return listRoleFunction;
        }
        public static ROLE_FUNCTION GetRoleFunctionByID(long id)
        {
            ROLE_FUNCTION vaiTro = new ROLE_FUNCTION();
            try
            {
                vaiTro = context.ROLE_FUNCTION.Where(o => o.ROLE_ID == id).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return vaiTro;
        }
        public static string InsertRole(ROLE_FUNCTION roleFuntion)
        {
            string result = "";
            try
            {
                context.ROLE_FUNCTION.Add(roleFuntion);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string InsertListRoleFunction(List<ROLE_FUNCTION> listRoleFuntion)
        {
            string result = "";
            try
            {
                context.ROLE_FUNCTION.AddRange(listRoleFuntion);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetRoleFunction(int id, int roleId, int functionId)
        {
            string result = "";
            ROLE_FUNCTION roleFunction = context.ROLE_FUNCTION.Where(o => o.ID == id).SingleOrDefault();
            try
            {
                if (roleFunction != null)
                {
                    roleFunction.ROLE_ID = roleId;
                    roleFunction.CHUCNANG_ID = functionId;
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeleteRoleFunctionByRoleId(int roleId)
        {
            string result = "";
            try
            {
                List<ROLE_FUNCTION> listRoleFunction = context.ROLE_FUNCTION.Where(o => o.ROLE_ID == roleId).ToList();
                if (listRoleFunction != null)
                {
                    context.ROLE_FUNCTION.RemoveRange(listRoleFunction);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string GetMenuByRole(int roleId)
        {
            string strMenu = "<ul  class='nav nav-list'>";
            try
            {
                var listMenu = (from rf in context.ROLE_FUNCTION
                                join cn in context.DM_CHUCNANG on rf.CHUCNANG_ID equals cn.DM_CHUCNANG_ID
                                where rf.ROLE_ID == roleId && cn.CHUCNANG_CHA == 0
                                select new { cn.DM_CHUCNANG_ID, cn.TEN_CHUCNANG, cn.URL, cn.TT_HIENTHI }).OrderBy(o=>o.TT_HIENTHI);
                foreach (var item in listMenu)
                {
                    var listChildMenu = (from rf in context.ROLE_FUNCTION
                                        join cn in context.DM_CHUCNANG on rf.CHUCNANG_ID equals cn.DM_CHUCNANG_ID
                                        where rf.ROLE_ID == roleId && (cn.CHUCNANG_CHA == item.DM_CHUCNANG_ID)
                                        select new { cn.DM_CHUCNANG_ID, cn.TEN_CHUCNANG, cn.URL, cn.CHUCNANG_CHA, cn.TT_HIENTHI }).OrderBy(o => o.TT_HIENTHI);
                    if (listChildMenu.Count() > 0)
                    {
                        strMenu += @"<li class=''><a class='dropdown-toggle' href='" + item.URL + "'><i class='menu-icon fa fa-desktop'></i><span class='menu-text'>" + item.TEN_CHUCNANG + "</span><b class='arrow fa fa-angle-down'></b></a><b class='arrow'></b><ul class='submenu'>";
                        foreach (var itemChild in listChildMenu)
                        {
                            strMenu += @"<li class=''><a href='" + itemChild.URL + "'><i class='menu-icon fa fa-caret-right'></i>" + itemChild.TEN_CHUCNANG + "</a><b class='arrow'></b></li>";
                        }
                        strMenu += @"</ul></li>";
                    }
                    else
                    {
                        strMenu += @"<li class=''><a href='" + item.URL + "'><i class='menu-icon fa fa-desktop'></i><span class='menu-text'>" + item.TEN_CHUCNANG + "</span></a></li>";
                    }
                }

                strMenu += "</ul>";
            }
            catch (Exception)
            {
            }

            return strMenu;
        }
    }
}