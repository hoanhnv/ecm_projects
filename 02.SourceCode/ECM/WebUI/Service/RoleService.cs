﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.Service
{
    public class RoleService
    {
        static ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<ROLES> GetListRole()
        {
            List<ROLES> listRole = new List<ROLES>();
            try
            {
                if (context == null)
                {
                    context = new ECM_CSDLEntities();
                }
                listRole = context.ROLES.ToList();
            }
            catch (Exception ex )
            {
                string e = ex.Message;

            }
            return listRole;
        }
        public static ROLES GetRoleByID(long roleId)
        {

            ROLES vaiTro = new ROLES();
            try
            {
                vaiTro = context.ROLES.Where(o => o.ROLE_ID == roleId).SingleOrDefault();
            }
            catch (Exception ex)
            {
            }
            return vaiTro;
        }
        public static string InsertRole(ROLES role)
        {
            string result = "";
            try
            {
                context.ROLES.Add(role);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetRole(int vaiTroId, string roleName)
        {
            string result = "";
            ROLES vaiTro = context.ROLES.Where(o => o.ROLE_ID == vaiTroId).SingleOrDefault();
            try
            {
                if (vaiTro != null)
                {
                    vaiTro.ROLENAME = roleName;
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeleteRole(int id)
        {
            string result = "";
            try
            {
                ROLES vaiTro = context.ROLES.Where(o => o.ROLE_ID == id).SingleOrDefault();
                if (vaiTro != null)
                {
                    context.ROLES.Remove(vaiTro);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}