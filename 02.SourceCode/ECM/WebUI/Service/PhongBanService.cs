﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Service
{
    public class PhongBanService
    {
        static Model.ECM_CSDLEntities context = new ECM_CSDLEntities();
        public static List<PHONGBAN> GetListPhongBan(int takeNumberRow = 0)
        {
            List<PHONGBAN> listPhongBan = new List<PHONGBAN>();
            try
            {
                if (takeNumberRow > 0)
                {
                    listPhongBan = context.PHONGBAN.Take(takeNumberRow).OrderBy(o => o.TENPHONGBAN).ToList();
                }
                else
                {
                    listPhongBan = context.PHONGBAN.ToList();
                }
            }
            catch (Exception)
            {
            }

            return listPhongBan;
        }
        public static PHONGBAN GetPhongBanByID(int phongBanId)
        {
            PHONGBAN phongban = new PHONGBAN();
            try
            {
                phongban = context.PHONGBAN.Where(o => o.PHONGBAN_ID == phongBanId).SingleOrDefault();
            }
            catch (Exception)
            {
            }
            return phongban;
        }
        public static string InsertPhongBan(PHONGBAN phongban)
        {
            string result = "";
            try
            {
                context.PHONGBAN.Add(phongban);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string UpdatetPhongBan(int phongBanId, string tenPHONGBAN, bool isActive)
        {
            string result = "";
            PHONGBAN phongban = context.PHONGBAN.Where(o => o.PHONGBAN_ID == phongBanId).SingleOrDefault();
            try
            {
                if (phongban != null)
                {
                    phongban.TENPHONGBAN = tenPHONGBAN;
                    phongban.ISACTIVE = isActive;
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn cập nhật";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string DeletePhongBan(int id)
        {
            string result = "";
            try
            {
                PHONGBAN phongban = context.PHONGBAN.Where(o => o.PHONGBAN_ID == id).SingleOrDefault();
                if (phongban != null)
                {
                    context.PHONGBAN.Remove(phongban);
                    context.SaveChanges();
                }
                else
                {
                    result = "Không tìm thấy bản ghi muốn xóa";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}