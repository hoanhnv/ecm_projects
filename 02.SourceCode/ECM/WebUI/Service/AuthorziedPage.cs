﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Service
{
    public class AuthorziedPage : System.Web.UI.UserControl
    {
        public AuthorziedPage()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public const string EmailContact = "";
        public long GetUserID
        {
            get
            {
                if (Session["UserId"] == null || Session["UserId"].ToString() == "0")
                {
                    return 0;
                }

                return Convert.ToInt64(Session["UserId"].ToString());
            }
        }
        public string GetUserName
        {
            get
            {
                if (Session["UserName"] == null)
                {
                    return "";
                }

                return Session["UserName"].ToString();
            }
        }
        public string GetPhone
        {
            get
            {
                if (Session["Phone"] == null)
                {
                    return "";
                }

                return Session["Phone"].ToString();
            }
        }
        public string GetEmail
        {
            get
            {
                if (Session["Email"] == null)
                {
                    return "";
                }

                return Session["Email"].ToString();
            }
        }
        public int GetRoleId
        {
            get
            {
                if (Session["RoleId"] == null)
                {
                    return 0;
                }

                return int.Parse(Session["RoleId"].ToString());
            }
        }

    }
}