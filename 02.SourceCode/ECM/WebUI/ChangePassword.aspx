﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="WebUI.Account.ChangePassword" %>
<asp:Content ID="Content2" ContentPlaceHolderID="BreadCrumb" runat="server">
    Đổi mật khẩu
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-md-12">
        <div class="panel-heading">
            <div class="panel-title">
                <h3>
                    Đổi mật khẩu</h3>
            </div>
        </div>
        <br />
        <div class="messagealert" id="alert_container">
        </div>
        <div class="panel-body" style="width: 60% !important; margin: auto !important">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">
                    Mật khẩu cũ:</label>
                <div class="col-sm-4">
                    <asp:TextBox runat="server" class="validate[required] form-control" ID="txtOldPassword"
                        placeholder="nhập tiêu đề" TextMode="Password" />
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">
                    Mật khẩu mới:</label>
                <div class="col-sm-4">
                    <asp:TextBox runat="server" class="validate[required] form-control" ID="txtNewPassword"
                        TextMode="Password" placeholder="nhập tiêu đề" />
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">
                    nhập lại:</label>
                <div class="col-sm-4">
                    <asp:TextBox runat="server" class="validate[required] form-control" ID="txtRepeatePassword"
                        TextMode="Password" placeholder="nhập tiêu đề" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <asp:Button Text="Cập nhật" runat="server" class="btn btn-primary" ID="btnUpdate"
                        OnClick="btnUpdate_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
