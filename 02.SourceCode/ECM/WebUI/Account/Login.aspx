﻿<%@ Page Title="Log in" Language="C#" MasterPageFile="~/Account/Login.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebUI.Account.Login" Async="true" %>

<%--<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>--%>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="LoginContentPlaceHolder">
    <div class="col-md-8">
        <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
            <p class="text-danger">
                <asp:Literal runat="server" ID="FailureText" />
            </p>
        </asp:PlaceHolder>
        <div id="alert_container" class="messagealert"></div>
        <div class="KhoangCachLogin">
            <span>Tên đăng nhập:</span>
        </div>
        <div class="KhoangCachLogin">
            <asp:TextBox runat="server" ID="UserName" Style="width: 371px; height: 36px" placeholder="Tên truy cập" />
            <br />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName"
                Style="color: Red;" ErrorMessage="Bạn chưa nhập tên tài khoản." />
        </div>
        <div class="KhoangCachLogin">
            <asp:Label runat="server" AssociatedControlID="Password">Mật khẩu đăng nhập</asp:Label>
        </div>
        <div class="KhoangCachLogin">
            <asp:TextBox runat="server" ID="Password" TextMode="Password" Style="width: 371px; height: 36px" placeholder="Mật khẩu truy cập" />
            <br />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" Style="color: Red;" ErrorMessage="Bạn chưa nhập mật khẩu." />
        </div>
    </div>
    <div class="KhoangCachLogin">
        <asp:Button runat="server" OnClick="LogIn" Text="Đăng nhập" CssClass="button1" />
    </div>
    <asp:HyperLink ID="RegisterHyperLink" runat="server"></asp:HyperLink>
</asp:Content>
