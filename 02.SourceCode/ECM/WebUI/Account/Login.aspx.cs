﻿using System;
using System.Web.UI;
using WebUI.Service;
using Utility;
using WebUI.Models;
using System.Collections.Generic;

namespace WebUI.Account
{
    public partial class Login : Page
    {
        protected void ShowMessage(string Message, EnumSite.MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void LogIn(object sender, EventArgs e)
        {
            string userName = UserName.Text.Trim();
            string password = Password.Text.Trim();
            try
            {
                DM_NGUOIDUNG nguoiDung = UserService.Login(userName, password);
                List<USER_ROLE> userRole = User_RoleService.GetListUserRoleByUserId(nguoiDung.DM_NGUOIDUNG_ID);
                if (nguoiDung != null)
                {
                    Session["UserId"] = nguoiDung.DM_NGUOIDUNG_ID;
                    Session["UserName"] = nguoiDung.TENDANGNHAP;
                    Session["RoleId"] = userRole.Count > 0 ? userRole[0].ROLE_ID.ToString() : "";
                    Response.Redirect("../Default.aspx");
                }
                else
                {
                    ShowMessage("Tên đăng nhập hoặc mật khẩu không đúng", EnumSite.MessageType.Error);
                }
            }
            catch (Exception)
            {
                ShowMessage("Lỗi đăng nhập", EnumSite.MessageType.Error);
            }
        }
    }
}