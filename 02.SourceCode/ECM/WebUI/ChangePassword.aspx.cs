﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.Service;

namespace WebUI.Account
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        public enum MessageType { Success, Error, Info, Warning };
        protected void ShowMessage(string Message, MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx", false);
                    return;
                }
                var oldPass = txtOldPassword.Text.Trim();
                var newPass = txtNewPassword.Text.Trim();
                var newPassRepeate = txtRepeatePassword.Text.Trim();

                if (txtNewPassword.Text.Trim() != txtRepeatePassword.Text.Trim())
                {
                    ShowMessage("Mật khẩu mới và nhập lại phải giống nhau", MessageType.Error);
                    return;
                }
                if (txtNewPassword.Text.Length < 6)
                {
                    ShowMessage("Mật khẩu phải có ít nhất 6 ký tự", MessageType.Error);
                    return;
                }
                string message = UserService.ChangePassword(int.Parse(Session["UserId"].ToString()), oldPass, newPass);
                if (message.Length > 0)
                {
                    ShowMessage(message, MessageType.Error);
                    return;
                }
                ShowMessage("Đổi mật khẩu thành công!", MessageType.Success);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi đổi mật khẩu: " + ex.Message, MessageType.Error);
            }
        }
    }
}