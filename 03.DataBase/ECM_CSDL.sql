CREATE DATABASE ECM_CSDL
go
USE [ECM_CSDL]
GO
/****** Object:  Table [dbo].[XA]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[XA](
	[XA_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MAXA] [nvarchar](7) NULL,
	[HUYEN_ID] [bigint] NULL,
	[TENXA] [nvarchar](255) NULL,
 CONSTRAINT [PK_XA] PRIMARY KEY CLUSTERED 
(
	[XA_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[XA] ON
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (1, N'', 1, N'Him Lam')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (2, N'', 1, N'Thanh Trường')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (3, N'', 1, N'Thanh Bình')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (4, N'', 1, N'Tân Thanh')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (5, N'', 1, N'Mường Thanh')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (6, N'', 1, N'Nam Thanh')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (7, N'', 1, N'Noong Bua')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (8, N'', 1, N'Thanh Minh')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (9, N'', 1, N'Tà Lèng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (10, N'', 2, N'Chung Chải')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (11, N'', 2, N'Mường Nhé')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (12, N'', 2, N'Mường Toong')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (13, N'', 2, N'Quảng Lâm')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (14, N'', 2, N'Nậm Kè')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (15, N'', 2, N'Sín Thầu')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (16, N'', 2, N'Nậm Vì')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (17, N'', 2, N'Pá Mỳ')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (18, N'', 2, N'Sen Thượng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (19, N'', 2, N'Leng Su Sìn')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (20, N'', 3, N' Sông Đà')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (21, N'', 3, N'Na Lay')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (22, N'', 3, N'Lay Nưa')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (23, N'', 4, N'Tủa Chùa')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (24, N'', 4, N'Huổi Só')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (25, N'', 4, N'Lao Xả Phình')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (26, N'', 4, N'Mường Báng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (27, N'', 4, N'Mường Đun')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (28, N'', 4, N'Sáng Nhè')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (29, N'', 4, N'Sín Chải (Xín Chải)')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (30, N'', 4, N'Sính Phình')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (31, N'', 4, N'Tả Phìn')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (32, N'', 4, N'Tả Sìn Thàng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (33, N'', 4, N'Trung Thu')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (34, N'', 4, N'Tủa Thàng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (35, N'', 5, N'Tuần Giáo')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (36, N'', 5, N'Chiềng Sinh')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (37, N'', 5, N'Mường Mùn')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (38, N'', 5, N'Mường Thín')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (39, N'', 5, N'Nà Sáy')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (40, N'', 5, N'Phình Sáng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (41, N'', 5, N'Pú Nhung')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (42, N'', 5, N'Quài Cang')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (43, N'', 5, N'Quài Nưa')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (44, N'', 5, N'Quài Tở')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (45, N'', 5, N'Ta Ma')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (46, N'', 5, N'Tênh Phông')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (47, N'', 5, N'Tỏa Tình')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (48, N'', 5, N'Mùn Chung')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (49, N'', 5, N'Nà Tòng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (50, N'', 5, N'Pú Xi')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (51, N'', 5, N'Rạng Đông')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (52, N'', 5, N'Chiềng Đông')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (53, N'', 5, N'Mường Khong')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (54, N'', 6, N'Mường Lói')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (55, N'', 6, N'Mường Nhà')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (56, N'', 6, N'Mường Phăng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (57, N'', 6, N'Mường Pồn')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (58, N'', 6, N'Na Ư')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (59, N'', 6, N'Nà Nhạn')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (60, N'', 6, N'Nà Tấu')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (61, N'', 6, N'Noong Hẹt')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (62, N'', 6, N'Noong Luống')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (63, N'', 6, N'Núa Ngam')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (64, N'', 6, N'Pa Thơm')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (65, N'', 6, N'Sam Mứn')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (66, N'', 6, N'Thanh An')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (67, N'', 6, N'Thanh Chăn')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (68, N'', 6, N'Thanh Hưng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (69, N'', 6, N'Thanh Luông')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (70, N'', 6, N'Thanh Nưa')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (71, N'', 6, N'Thanh Xương')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (72, N'', 6, N'Thanh Yên')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (73, N'', 7, N'Điện Biên Đông')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (74, N'', 7, N'Chiềng Sơ')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (75, N'', 7, N'Háng Lìa')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (76, N'', 7, N'Keo Lôm')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (77, N'', 7, N'Luân Giới')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (78, N'', 7, N'Mường Luân')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (79, N'', 7, N'Na Son')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (80, N'', 7, N'Nong U')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (81, N'', 7, N'Phì Nhừ')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (82, N'', 7, N'Phình Giàng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (83, N'', 7, N'Pú Hồng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (84, N'', 7, N'Pú Nhi (Pù Nhi)')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (85, N'', 7, N'Tìa Dình')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (86, N'', 7, N'Xa Dung')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (87, N'', 8, N'Mường Chà (Mường Lay)')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (88, N'', 8, N'Huổi Lèng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (89, N'', 8, N'Hừa Ngài')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (90, N'', 8, N'Ma Thì Hồ')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (91, N'', 8, N'Mường Mươn')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (92, N'', 8, N'Mường Tùng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (93, N'', 8, N'Na Sang')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (94, N'', 8, N'Pa Ham')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (95, N'', 8, N'Sa Lông')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (96, N'', 8, N'Sá Tổng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (97, N'', 8, N'Huổi Mí')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (98, N'', 8, N'Nậm Nèn')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (99, N'', 9, N'Mường Ảng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (100, N'', 9, N'Ẳng Cang')
GO
print 'Processed 100 total records'
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (101, N'', 9, N'Ẳng Nưa')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (102, N'', 9, N'Ẳng Tở')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (103, N'', 9, N'Búng Lao')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (104, N'', 9, N'Mường Đăng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (105, N'', 9, N'Mường Lạn')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (106, N'', 9, N'Nặm lịch')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (107, N'', 9, N'Ngối Cáy')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (108, N'', 9, N'Xuân Lao')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (109, N'', 10, N'Chà Cang')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (110, N'', 10, N'Chà Nưa')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (111, N'', 10, N'Chà Tở')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (112, N'', 10, N'Na Cô Sa')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (113, N'', 10, N'Nà Bủng')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (114, N'', 10, N'Nà Hỳ')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (115, N'', 10, N'Nà Khoa')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (116, N'', 10, N'Nậm Chua')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (117, N'', 10, N'Nậm Khăn')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (118, N'', 10, N'Nậm Nhừ')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (119, N'', 10, N'Nậm Tin')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (120, N'', 10, N'Pa Tần')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (121, N'', 10, N'Phìn Hồ')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (122, N'', 10, N'Si Pa Phìn')
INSERT [dbo].[XA] ([XA_ID], [MAXA], [HUYEN_ID], [TENXA]) VALUES (123, N'', 10, N'Vàng Đán')
SET IDENTITY_INSERT [dbo].[XA] OFF
/****** Object:  Table [dbo].[VITRI_NGUOIDUNG]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VITRI_NGUOIDUNG](
	[NGUOIDUNG_ID] [bigint] NOT NULL,
	[TINH_ID] [int] NULL,
	[HUYEN_ID] [int] NULL,
	[XA_ID] [int] NULL,
	[THON_ID] [int] NULL,
 CONSTRAINT [PK_VITRI_NGUOIDUNG] PRIMARY KEY CLUSTERED 
(
	[NGUOIDUNG_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[USER_ROLE]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[USER_ROLE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[USER_ID] [int] NULL,
	[ROLE_ID] [int] NULL,
 CONSTRAINT [PK_USER_ROLE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TINH]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TINH](
	[TINH_ID] [int] IDENTITY(1,1) NOT NULL,
	[MATINH] [nvarchar](3) NULL,
	[TENTINH] [nvarchar](50) NULL,
 CONSTRAINT [PK_TINH] PRIMARY KEY CLUSTERED 
(
	[TINH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[THON_XOM]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[THON_XOM](
	[THON_ID] [int] IDENTITY(1,1) NOT NULL,
	[MA_THON] [nchar](10) NULL,
	[XA_ID] [int] NULL,
	[TEN_THON] [nvarchar](250) NULL,
	[HUYEN_ID] [int] NULL,
 CONSTRAINT [PK_THON_XOM] PRIMARY KEY CLUSTERED 
(
	[THON_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[THON_XOM] ON
INSERT [dbo].[THON_XOM] ([THON_ID], [MA_THON], [XA_ID], [TEN_THON], [HUYEN_ID]) VALUES (1, N'thôn 1    ', 1, N'Thôn điện biên 1', 1)
INSERT [dbo].[THON_XOM] ([THON_ID], [MA_THON], [XA_ID], [TEN_THON], [HUYEN_ID]) VALUES (2, N'thôn 1    ', 2, N'Thôn điện biên 2', 1)
INSERT [dbo].[THON_XOM] ([THON_ID], [MA_THON], [XA_ID], [TEN_THON], [HUYEN_ID]) VALUES (3, N'thôn 1    ', 3, N'Thôn điện biên 3', 1)
INSERT [dbo].[THON_XOM] ([THON_ID], [MA_THON], [XA_ID], [TEN_THON], [HUYEN_ID]) VALUES (4, N'thôn 1    ', 1, N'Thôn điện biên 4', 1)
INSERT [dbo].[THON_XOM] ([THON_ID], [MA_THON], [XA_ID], [TEN_THON], [HUYEN_ID]) VALUES (5, N'thôn 1    ', 1, N'Thôn điện biên 5', 1)
SET IDENTITY_INSERT [dbo].[THON_XOM] OFF
/****** Object:  Table [dbo].[ROLES]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ROLES](
	[ROLE_ID] [int] IDENTITY(1,1) NOT NULL,
	[ROLENAME] [nvarchar](50) NULL,
 CONSTRAINT [PK_ROLES] PRIMARY KEY CLUSTERED 
(
	[ROLE_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ROLES] ON
INSERT [dbo].[ROLES] ([ROLE_ID], [ROLENAME]) VALUES (1, N'Admin')
INSERT [dbo].[ROLES] ([ROLE_ID], [ROLENAME]) VALUES (2, N'Cấp Tỉnh')
INSERT [dbo].[ROLES] ([ROLE_ID], [ROLENAME]) VALUES (3, N'Cấp Huyện')
INSERT [dbo].[ROLES] ([ROLE_ID], [ROLENAME]) VALUES (4, N'Cấp Xã')
INSERT [dbo].[ROLES] ([ROLE_ID], [ROLENAME]) VALUES (5, N'Cấp Xóm')
INSERT [dbo].[ROLES] ([ROLE_ID], [ROLENAME]) VALUES (6, N'User')
SET IDENTITY_INSERT [dbo].[ROLES] OFF
/****** Object:  Table [dbo].[ROLE_FUNCTION]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ROLE_FUNCTION](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ROLE_ID] [int] NULL,
	[CHUCNANG_ID] [int] NULL,
 CONSTRAINT [PK_ROLE_FUNCTION] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HUYEN]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HUYEN](
	[HUYEN_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MAHUYEN] [nvarchar](5) NULL,
	[TINH_ID] [int] NULL,
	[TENHUYEN] [nvarchar](100) NULL,
 CONSTRAINT [PK_HUYEN] PRIMARY KEY CLUSTERED 
(
	[HUYEN_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[HUYEN] ON
INSERT [dbo].[HUYEN] ([HUYEN_ID], [MAHUYEN], [TINH_ID], [TENHUYEN]) VALUES (1, NULL, 1, N'Điện Biên Phủ')
INSERT [dbo].[HUYEN] ([HUYEN_ID], [MAHUYEN], [TINH_ID], [TENHUYEN]) VALUES (2, NULL, 1, N'Mường Nhé')
INSERT [dbo].[HUYEN] ([HUYEN_ID], [MAHUYEN], [TINH_ID], [TENHUYEN]) VALUES (3, NULL, 1, N'Mường Lay')
INSERT [dbo].[HUYEN] ([HUYEN_ID], [MAHUYEN], [TINH_ID], [TENHUYEN]) VALUES (4, NULL, 1, N'Tủa Chùa')
INSERT [dbo].[HUYEN] ([HUYEN_ID], [MAHUYEN], [TINH_ID], [TENHUYEN]) VALUES (5, NULL, 1, N'Tuần Giáo')
INSERT [dbo].[HUYEN] ([HUYEN_ID], [MAHUYEN], [TINH_ID], [TENHUYEN]) VALUES (6, NULL, 1, N'Điện Biên')
INSERT [dbo].[HUYEN] ([HUYEN_ID], [MAHUYEN], [TINH_ID], [TENHUYEN]) VALUES (7, NULL, 1, N'Điện Biên Đông')
INSERT [dbo].[HUYEN] ([HUYEN_ID], [MAHUYEN], [TINH_ID], [TENHUYEN]) VALUES (8, NULL, 1, N'Mường Chà')
INSERT [dbo].[HUYEN] ([HUYEN_ID], [MAHUYEN], [TINH_ID], [TENHUYEN]) VALUES (9, NULL, 1, N'Mường Ảng')
INSERT [dbo].[HUYEN] ([HUYEN_ID], [MAHUYEN], [TINH_ID], [TENHUYEN]) VALUES (10, NULL, 1, N'Nậm Pồ')
SET IDENTITY_INSERT [dbo].[HUYEN] OFF
/****** Object:  Table [dbo].[FILES]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FILES](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FILE_PATH] [nvarchar](250) NULL,
	[DISPLAY_NAME] [nvarchar](250) NULL,
	[FILE_NAME] [nvarchar](150) NULL,
	[FILE_TYPE] [int] NULL,
	[FILE_DOWNLOAD] [int] NULL,
	[IS_ACTIVE] [bit] NULL,
	[IS_APPROVE] [bit] NULL,
	[NGUOI_TAO] [int] NULL,
	[NGUOI_SUA] [int] NULL,
	[NGUOIDUYET] [int] NULL,
	[NGAY_DUYET] [datetime] NULL,
	[NGAY_SUA] [datetime] NULL,
	[NGAY_TAO] [datetime] NULL,
	[NGAY_XUATBAN] [datetime] NULL,
	[NGUOI_XUATBAN] [int] NULL,
 CONSTRAINT [PK_FILES] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DOITUONG]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DOITUONG](
	[DOITUONG_ID] [int] IDENTITY(1,1) NOT NULL,
	[TEN_DOITUONG] [nvarchar](150) NULL,
	[NGAY_SINH] [datetime] NULL,
	[MA_DOITUONG] [nvarchar](150) NULL,
	[DIACHI_HIENTAI] [nvarchar](250) NULL,
	[HOTEN_CHA] [nvarchar](150) NULL,
	[HOTEN_ME] [nvarchar](150) NULL,
	[TUOI] [int] NULL,
	[NGHE_NGHIEP] [nvarchar](150) NULL,
	[COSODAOTAO_ID] [int] NULL,
	[HUYEN_ID] [int] NULL,
	[XA_ID] [int] NULL,
	[THON_ID] [int] NULL,
 CONSTRAINT [PK_DOITUONG] PRIMARY KEY CLUSTERED 
(
	[DOITUONG_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM_NGUOIDUNG]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_NGUOIDUNG](
	[DM_NGUOIDUNG_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TENDANGNHAP] [nvarchar](50) NULL,
	[MATKHAU] [nvarchar](100) NULL,
	[HOTEN] [nvarchar](250) NULL,
	[DIENTHOAI] [nvarchar](20) NULL,
	[EMAIL] [nvarchar](250) NULL,
	[DIACHI] [nvarchar](500) NULL,
	[NGAYSINH] [datetime] NULL,
	[TRANGTHAI] [int] NULL,
	[COSO_ID] [int] NULL,
	[NGUOITAO] [nvarchar](50) NULL,
	[NGAYTAO] [datetime] NULL,
	[NGUOISUA] [nvarchar](50) NULL,
	[NGAYSUA] [datetime] NULL,
 CONSTRAINT [PK_DM_NGUOIDUNG] PRIMARY KEY CLUSTERED 
(
	[DM_NGUOIDUNG_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM_CHUCNANG]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_CHUCNANG](
	[DM_CHUCNANG_ID] [int] IDENTITY(1,1) NOT NULL,
	[MA_CHUCNANG] [nvarchar](50) NULL,
	[TEN_CHUCNANG] [nvarchar](100) NULL,
	[TRANGTHAI] [int] NULL,
	[NGAYTAO] [datetime] NULL,
	[NGAYSUA] [datetime] NULL,
	[CHUCNANG_CHA] [int] NULL,
	[URL] [nvarchar](300) NULL,
	[TT_HIENTHI] [int] NULL,
	[NGUOITAO] [nvarchar](50) NULL,
	[NGUOISUA] [nvarchar](50) NULL,
	[IS_HIDDEN] [int] NULL,
 CONSTRAINT [PK_DM_CHUCNANG] PRIMARY KEY CLUSTERED 
(
	[DM_CHUCNANG_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[COSODAOTAO]    Script Date: 07/06/2016 13:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COSODAOTAO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MACOSO] [nvarchar](250) NULL,
	[TENCOSO] [nvarchar](250) NULL,
	[TENTIENGANH] [nvarchar](250) NULL,
	[TENVIETTAT] [nvarchar](250) NULL,
	[DIACHI] [nvarchar](250) NULL,
	[DIENTHOAI] [nvarchar](50) NULL,
	[FAX] [nvarchar](50) NULL,
	[EMAIL] [nvarchar](250) NULL,
	[WEBSITE] [nvarchar](250) NULL,
	[SOQUYETDINHTHANHLAP] [nvarchar](50) NULL,
	[SOGIAYDANGKYKINHDOANH] [nvarchar](50) NULL,
	[NGAYCAPDKKD] [datetime] NULL,
	[NGAYCAPQDTHANHLAP] [datetime] NULL,
	[LOAIGIAYTO] [int] NULL,
	[DONVICAP] [nvarchar](250) NULL,
	[NOIDUNGCAP] [nvarchar](500) NULL,
	[DAIDIENPHAPNHAN] [nvarchar](250) NULL,
	[CACLINHVUCHOATDONGCHINH] [nvarchar](250) NULL,
	[CACTHONGTINKHAC] [ntext] NULL,
	[NGAYTAO] [datetime] NULL,
	[NGUOITAO] [int] NULL,
	[NGAYSUA] [datetime] NULL,
	[NGUOISUA] [int] NULL,
	[IS_APPROVE] [bit] NULL,
	[NGUOIDUYET] [int] NULL,
	[NGAYDUYET] [datetime] NULL,
	[IS_ACTIVE] [bit] NULL,
	[NGUOIXUATBAN] [int] NULL,
	[NGAYXUATBAN] [datetime] NULL,
	[THON_ID] [int] NULL,
	[XA_ID] [int] NULL,
	[HUYEN_ID] [int] NULL,
 CONSTRAINT [PK_COSODAOTAO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Default [DF__DM_CHUCNA__NGUOI__7E6CC920]    Script Date: 07/06/2016 13:59:15 ******/
ALTER TABLE [dbo].[DM_CHUCNANG] ADD  CONSTRAINT [DF__DM_CHUCNA__NGUOI__7E6CC920]  DEFAULT ('admin') FOR [NGUOITAO]
GO
