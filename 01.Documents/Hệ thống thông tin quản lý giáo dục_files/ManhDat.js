﻿var strUniGlobal = "áàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬÉÈẺẼẸÊẾỀỂỄỆÍÌỈĨỊÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴĐ";
var strAbcGlobal = " abcdefghijkmlnopqrstuvxyzwABCDEFGHIJKMLNOPQRSTUVXYZW";
var strSpecialGlobal = "0123456789,./<>?:;[]{}|!@#$%^&*()-_=+";
function SetPanelWidth() { }
function CheckDisplay() { }

function setCookie(c_name, value, expiredays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = c_name + "=" + escape(value) +
    ((expiredays == null) ? "" : ";expires=" + exdate.toUTCString());
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}
$(document).ready(function () {
    $(".ColValidlength textarea").keyup(function () {
        checklenght();
    });
});

function checklenght() {
    var checkValid = true;
    $('.ColValidlength textarea').each(function (index) {
        var maxlength = $(this).attr("maxlength");
        if ($(this).val().length > maxlength) {
            var a = $(this).parent();
            var sp = $(this).parent().find('span');
            if (sp.length == 0) {
                a.append("<span style='color:red'>Bạn đã nhập quá " + maxlength + " ký tự</span>");
            }
            checkValid = false;
        } else {
            var sp = $(this).parent().find('span');
            if (sp.length > 0) {
                $(this).parent().find('span').remove();
            }
        }
    });
    if (!checkValid) $("#cphMain_my_Grid_btSaveGrid").prop("disabled", true);
    else $("#cphMain_my_Grid_btSaveGrid").prop("disabled", false);
}